package com.example.salesnrich

import android.Manifest
import android.provider.Settings
import android.telephony.TelephonyManager
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import java.util.*
import java.util.UUID;


class MainActivity: FlutterActivity() {
    private val CHANNEL = "platformCS";
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler{
            call, result ->
            if(call.method=="Key"){
                val str =uuid();
                if(str!=null){
                    result.success(str);
                }
                else{
                    result.error("UnAvailable","cant able to get",null)
                }
            }else{
                result.notImplemented();
            }
        }
    }
    private fun uuid(): String{
        val androidId: String = Settings.Secure.getString(contentResolver,
                Settings.Secure.ANDROID_ID)
        val uuid : String = UUID.nameUUIDFromBytes(androidId.toByteArray()).toString()
        return uuid;
    }
}
