// ignore: duplicate_ignore
// ignore: file_names
// ignore_for_file: unnecessary_brace_in_string_interps, non_constant_identifier_names, file_names
class ServiceGenerator {
// ==========   API_BASE_URL ==================

  static String API_BASE_URL =
      'http://www.salesnrich.com/api'; //  salesnrich server
    // static String API_BASE_URL = 'http://192.168.1.87:80/api';   //prashoob
//  static String API_BASE_URL = 'http://192.168.0.88/api'; //  reshmi

  static String URL = 'www.salesnrich.com'; //  salesnrich server
    // static String URL = '192.168.1.87:80';   //  prashoob
//  static String URL = '192.168.0.88'; //reshmi
//
//

// ==========   WEB_VIEW_LOGIN_URL ==================

 static String WEB_VIEW_LOGIN_URL =
     "http://salesnrich.com/login"; // salesnrich server
  // static String WEB_VIEW_LOGIN_URL = "http://192.168.1.87:80/login"; // reshmi
//
//

// ==========   VALIDATION ==================

  static String VALIDATION_URL =
      "http://salesnrich.com/api/account/user-validate";
  static String VALIDATION_URL_ALL =
      "http://salesnrich.com/api/validate/user-onpremise";

//
//
//
//

// ==========   API's ==================

  static String LOGIN = '${API_BASE_URL}/authenticate';
  static String POST_DEVICE_KEY = '${API_BASE_URL}/register-device';
  static String ACCOUNT_TYPE = '${API_BASE_URL}/account-types';
  static String DOCUMENT = '${API_BASE_URL}/documents';
  static String MOBILE_MENU_ITEMS = '${API_BASE_URL}/mobile-menu-items';
  static String ACCOUNT_USER_DETIALS = '${API_BASE_URL}/account';
  static String EXECUTIVE_ACTIVITY = '${API_BASE_URL}/activities';
  static String ACTIVITY_GROUP = '${API_BASE_URL}/activity-groups';
  static String ACTIVITY_USER_TARGET = '${API_BASE_URL}/activity-user-targets';
  static String ACTIVITY_GROUP_TARGET =
      '${API_BASE_URL}/activity-group-user-targets';
  static String ACCOUNT_PROFILE = '${API_BASE_URL}/location-account-profiles';
  static String TERRITORY = '${API_BASE_URL}/territories';
  static String MOBILE_SETTINGS_CONFIGURATION =
      '${API_BASE_URL}/mobile-settings';
  static String EMPLOYEE_TERRITORIES = '${API_BASE_URL}/employee-locations';
  static String TERRITORY_ACCOUNTG_ASSOCIATION =
      '${API_BASE_URL}/location-account-profile-association';
  static String ACTIVITY_STAGE = '${API_BASE_URL}/activity-stage';
  static String PRODUCT_PROFILE = '${API_BASE_URL}/product-profiles';
  static String PRODUCT_CATEGORY = '${API_BASE_URL}/product-categories';
  static String DOCUMENT_PRODUCT_CATEGORY =
      '${API_BASE_URL}/document-product-categories';
  static String PRODUCT_GROUP = '${API_BASE_URL}/product-group-products';
  static String DOCUMENT_PRODUCT_GROUP =
      '${API_BASE_URL}/document-product-groups';
  static String DOCUMENT_INVENTORY_VOUCHER_COLUMNS =
      '${API_BASE_URL}/document-inventory-voucher-columns';
  static String DOCUMENT_ACCOUNTING_VOUCHER_COLUMNS =
      '$API_BASE_URL/document-accounting-voucher-columns';
  static String DOCUMENT_ACCOUNT_TYPE =
      '${API_BASE_URL}/document-account-types';
  static String LIVE_PRODUCT_PRICE = '${API_BASE_URL}/product/get-price';
  static String DYNAMIC_FORMULA = '${API_BASE_URL}/static-form-js-code';
  static String PRODUCT_PRICE_LIST = '${API_BASE_URL}/price-levels';
  static String STOCK_LOCATIONS = '${API_BASE_URL}/stock-locations';
  static String DOCUMENT_PRICE_LEVEL = '${API_BASE_URL}/document-price-levels';
    static String TASKEXECUTIONPOST = '${API_BASE_URL}/executive-task-execution/saveOrupdate';

}
