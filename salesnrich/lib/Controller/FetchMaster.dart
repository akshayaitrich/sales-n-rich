import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:salesnrich/Controller/APIs.dart';
import 'package:salesnrich/Controller/preferenceOperation.dart';
import 'package:salesnrich/Controller/staticFields.dart';
import 'package:salesnrich/Model/Tables.dart';
import 'package:salesnrich/Model/sharedPreference.dart';
import 'package:salesnrich/Model/user.dart';
import 'package:salesnrich/View/home_Activity.dart';
import 'package:salesnrich/View/splash_Login_Download.dart';
import 'package:salesnrich/WebServer.dart';
import 'package:sizer/sizer_ext.dart';
import 'package:uuid/uuid.dart';

import 'keygenerator.dart';

class Fetchmaster {
  var entities = [
    'POST_DEVICE_KEY',
    'ACCOUNT_TYPE',
    'ACCOUNT_PROFILE',
    'EXECUTIVE_ACTIVITY',
    'ACTIVITY_GROUP',
    'DOCUMENT',
    'FORM_ELEMENT_TYPE',
    'FORM_ELEMENTS',
    'SUB_FORM_ELEMENTS',
    'FORM',
    'FORMS_FORM_ELEMENTS',
    'DOCUMENT_FORMS',
    'DOCUMENT_INVENTORY_VOUCHER_COLUMNS',
    'DOCUMENT_ACCOUNTING_VOUCHER_COLUMNS',
    'USER_ACTIVITY_DOCUMENT_DETAILS',
    'PRODUCT_GROUP',
    'PRODUCT_CATEGORY',
    'PRODUCT_PROFILE',
    'DOCUMENT_PRODUCT_CATEGORY',
    'DOCUMENT_PRODUCT_GROUP',
    'PRODUCT_PRICE_LIST',
    'ACTIVITY_GROUP_TARGET',
    'ACTIVITY_USER_TARGET',
    'DYNAMIC_FORMULA',
    'KNOWLEDGE_BASE',
    'DOCUMENT_ACCOUNT_TYPE',
    'RECEIVABLE_PAYABLE',
    'ACTIVITY_HOUR_PRODUCT_GROUP',
    'PRICE_TREND_CONFIGURATION',
    'PRICE_TREND_PRODUCT_COMPETITIORS',
    'COMPETITORS_PROFILE',
    'DOCUMENT_PRICE_LEVELS',
    'USER_SALES_TARGET',
    'USER_RECEIPT_TARGET',
    'STOCK_LOCATIONS',
    'ADVANCED_SEARCH_DOCUMENTS',
    'INVENTORY_SEARCH',
    'ACCOUNT_USER_DETIALS',
    'TERRITORY',
    'ROUTE_PLANS',
    'DAY_PLANS',
    'FAVORITE',
    'DAY_PLAN_EXECUTIVE_CONFIG',
    'PRODUCT_BATCH_NUMBERS',
    'MOBILE_SETTING_CONFIG',
    'TERRITORY_ACCOUNTG_ASSOCIATION',
    'EMPLOYEE_TERRITORIES',
    'MOBILE_MENU_ITEMS',
    'DOCUMENT_DESTGINATION_STOCK_LOCATIONS',
    'DOCUMENT_SOURCE_STOCK_LOCATIONS',
    'INCOME_EXPENSE_HEAD',
    'LOCATION_HIRARCHY',
    'PICKING_PACKING',
    'ATTENDANCE_SUB_GROUP',
    'ACCOUNT_PURCHASE_HISTORY',
    'FEEDBACK_DOCUMENT_DETGAILS',
    'RECEIVABLE_PAYABLE_COLUMN',
    'ACTIVITY_STAGE',
    'REFERENCE_DOCUMENT',
    'POST_DATED_VOUCHERS',
    'VEHICLE_MASTER',
    'DOCUMENT_VOUCHER_NUMBER',
  ];

  Fetchmaster() {
    start();
  }

  int downloads = 0;
  update({String item_Started_to_download}) {
    downloads++;
    DownloadPageState.total_Downloads = '$downloads/53';
    DownloadPageState.current_Download_Percentage = (downloads / 53) * 100;
    DownloadPageState.download_Proggressbar_length = (downloads * 3).toDouble();
    DownloadPageState.item_Started_to_download = item_Started_to_download;
    DownloadPageState.sub_items_downloaded_inThat = '';
    print(DownloadPageState.current_Download_Percentage);
    if (DownloadPageState.current_Download_Percentage >
        StaticFields.downLoadPercentage) {
      DownloadPageState.button = FlatButton(
          onPressed: (() {
            FirstPageState.wi = HomeActivity();
            (FirstPageState.contextt as StatefulElement).state.setState(() {});
          }),
          color: Colors.green,
          child: Text(
            'Continue',
            style: TextStyle(fontSize: 14.0.sp),
          ));
      (DownloadPageState.contextt as StatefulElement).state.setState(() {});
    } else {
      (DownloadPageState.contextt as StatefulElement).state.setState(() {});
      print(DownloadPageState.current_Download_Percentage);
    }
  }

  categorise(String value) async {
    switch (value) {
      case 'POST_DEVICE_KEY':
        print('inside POST_DEVICE_KEY');
        var response = getDeviceKey();
        update(item_Started_to_download: 'Device key posted');
        break;
      case 'ACCOUNT_TYPE':
        print('inside ACCOUNT_TYPE');
        var response = await WebServer()
            .gett(url: ServiceGenerator.ACCOUNT_TYPE) as http.Response;
        if (response.statusCode == 200) {
          AccountType().connectDatabase(response);
        }
        update(item_Started_to_download: 'Account type downloaded');
        break;
      case 'MOBILE_MENU_ITEMS':
        print('inside MOBILE_MENU_ITEMS');

        var response = await WebServer()
            .gett(url: ServiceGenerator.MOBILE_MENU_ITEMS) as Response;
        if (response.statusCode == 200) {
          MobileMenuItems().connectDatabase(response);
        }
        update(item_Started_to_download: 'Mobile menu items downloaded');
        break;
      case 'ACCOUNT_USER_DETIALS':
        print('inside ACCOUNT_USER_DETIALS');
        var response = await WebServer()
            .gett(url: ServiceGenerator.ACCOUNT_USER_DETIALS) as Response;
        if (response.statusCode == 200) {
          User().connectDatabase(response);
        }
        update(item_Started_to_download: 'account user details downloaded');
        break;

      case 'EXECUTIVE_ACTIVITY':
        print('inside EXECUTIVE_ACTIVITY');
        var response = await WebServer()
            .gett(url: ServiceGenerator.EXECUTIVE_ACTIVITY) as http.Response;
        if (response.statusCode == 200) {
          ExecutiveActivity().connectDatabase(response);
          ActivityAccountType().connectDatabase(response);
          ActivityDocument().connectDatabase(response);
        }
        update(item_Started_to_download: 'executive activities downloaded');
        break;
      case 'ACTIVITY_GROUP':
        print('inside ACTIVITY_GROUP');
        var response = await WebServer()
            .gett(url: ServiceGenerator.ACTIVITY_GROUP) as http.Response;
        if (response.statusCode == 200) {
          ActivityGroup().connectDatabase(response);
        }
        update(item_Started_to_download: 'activity groups downloaded');
        break;
      case 'ACTIVITY_USER_TARGET':
        print('inside ACTIVITY_USER_TARGET');
        var response = await WebServer()
            .gett(url: ServiceGenerator.ACTIVITY_USER_TARGET) as http.Response;
        if (response.statusCode == 200) {
          ActivityUserTarget().connectDatabase(response);
        }
        update(item_Started_to_download: 'activity user target downloaded');
        break;
      case 'ACTIVITY_GROUP_TARGET':
        print('inside ACTIVITY_GROUP_TARGET');
        var response = await WebServer()
            .gett(url: ServiceGenerator.ACTIVITY_GROUP_TARGET) as http.Response;
        if (response.statusCode == 200) {
          ActivityGroupTarget().connectDatabase(response);
        }

        update(item_Started_to_download: 'activity group target downloaded');
        break;
      case 'ACCOUNT_PROFILE':
        print('inside ACCOUNT_PROFILE');
        var mp = {'page': '${0}', 'size': '${3000}'};
        var response = await WebServer().get_with_query(
            url: 'location-account-profiles', data: mp) as http.Response;

        if (response.statusCode == 200) {
          Preference().setApiType('fetch');
          AccountProfile().connectDatabase(response);
        }
        update(item_Started_to_download: 'account profiles downloaded');
        break;
      case 'MOBILE_SETTING_CONFIG':
        print('inside MOBILE_SETTING_CONFIG');
        var response = await WebServer()
                .gett(url: ServiceGenerator.MOBILE_SETTINGS_CONFIGURATION)
            as http.Response;
        if (response.statusCode == 200) {
          print("this is the mobile config data ${response.body}");
          MobileSettingsConfiguration().connectDatabase(response);
        }
        update(item_Started_to_download: 'mobile configuration downloaded');
        break;
      case 'TERRITORY':
        print('inside TERRITORY');
        var response = await WebServer().gett(url: ServiceGenerator.TERRITORY)
            as http.Response;
        if (response.statusCode == 200) {
          (DownloadPageState.contextt as StatefulElement).state.setState(() {});
          Territory().connectDatabase(response, false);
        }
        update(item_Started_to_download: 'territory downloaded');
        break;
      case 'EMPLOYEE_TERRITORIES':
        print('inside EMPLOYEE_TERRITORIES');
        var response = await WebServer()
            .gett(url: ServiceGenerator.EMPLOYEE_TERRITORIES) as http.Response;
        if (response.statusCode == 200) {
          (DownloadPageState.contextt as StatefulElement).state.setState(() {});
          Territory().connectDatabase(response, true);
        }
        update(item_Started_to_download: 'employee territory downloaded');
        break;
      case 'TERRITORY_ACCOUNTG_ASSOCIATION':
        print('inside TERRITORY_ACCOUNTG_ASSOCIATION');
        var mp = {'page': '${0}', 'size': '${3000}'};
        var response = await WebServer().get_with_query(
            url: 'location-account-profile-association',
            data: mp) as http.Response;
        if (response.statusCode == 200) {
          TerrtoryAccountAssocation().connectDatabase(response);
        }
        update(
            item_Started_to_download:
                'account profiles association downloaded');
        break;
      case 'ACTIVITY_STAGE':
        print('inside ACTIVITY_STAGE');
        var response = await WebServer()
            .gett(url: ServiceGenerator.ACTIVITY_STAGE) as http.Response;
        if (response.statusCode == 200) {
          ActivityStage().connectDatabase(response);
        }
        update(item_Started_to_download: 'activity stage downloaded');
        break;
      case 'DOCUMENT':
        print('inside DOCUMENT');
        var response = await WebServer().gett(url: ServiceGenerator.DOCUMENT)
            as http.Response;
        if (response.statusCode == 200) {
          Document().connectDatabase(response);
        }
        update(item_Started_to_download: 'document downloaded');
        break;
      case 'PRODUCT_PROFILE':
        print('inside PRODUCT_PROFILE');
        var mp = {'page': '${0}', 'size': '${3000}'};
        var response = await WebServer()
            .get_with_query(url: 'product-profiles', data: mp) as http.Response;
        if (response.statusCode == 200) {
          SharedPrefernce().connectDatabase({'api_type': 'fetch'});
          ProductProfile().connectDatabase(response);
        }
        update(item_Started_to_download: 'product profiles downloaded');
        break;
      case 'PRODUCT_CATEGORY':
        print('inside PRODUCT_CATEGORY');
        var response = await WebServer()
            .gett(url: ServiceGenerator.PRODUCT_CATEGORY) as http.Response;
        if (response.statusCode == 200) {
          SharedPrefernce().connectDatabase({'api_type': 'fetch'});
          ProductCategory().connectDatabase(response);
        }
        update(item_Started_to_download: 'product category downloaded');
        break;
      case 'DOCUMENT_PRODUCT_CATEGORY':
        print('inside DOCUMENT_PRODUCT_CATEGORY');
        var response = await WebServer().gett(
            url: ServiceGenerator.DOCUMENT_PRODUCT_CATEGORY) as http.Response;
        if (response.statusCode == 200) {
          DocumentProductCategory().connectDatabase(response);
        }
        update(
            item_Started_to_download: 'document product category downloaded');
        break;
      case 'PRODUCT_GROUP':
        print('inside PRODUCT_GROUP');
        var response = await WebServer()
            .gett(url: ServiceGenerator.PRODUCT_GROUP) as http.Response;
        if (response.statusCode == 200) {
          SharedPrefernce().connectDatabase({'api_type': 'fetch'});
          ProductGroup().connectDatabase(response);
        }
        update(item_Started_to_download: 'product group downloaded');
        break;
      case 'DOCUMENT_PRODUCT_GROUP':
        print('inside DOCUMENT_PRODUCT_GROUP');
        var response =
            await WebServer().gett(url: ServiceGenerator.DOCUMENT_PRODUCT_GROUP)
                as http.Response;
        if (response.statusCode == 200) {
          DocumentProductGroup().connectDatabase(response);
        }
        update(item_Started_to_download: 'document product group downloaded');
        break;
      case 'DOCUMENT_INVENTORY_VOUCHER_COLUMNS':
        print('inside DOCUMENT_INVENTORY_VOUCHER_COLUMNS');
        var response = await WebServer()
                .gett(url: ServiceGenerator.DOCUMENT_INVENTORY_VOUCHER_COLUMNS)
            as http.Response;
        if (response.statusCode == 200) {
          VoucherColoum().connectDatabase(response);
        }
        update(
            item_Started_to_download:
                'document inventory voucher column downloaded');
        break;
      case 'DOCUMENT_ACCOUNTING_VOUCHER_COLUMNS':
        break;
      case 'DOCUMENT_ACCOUNT_TYPE':
        print('inside DOCUMENT_ACCOUNT_TYPE');
        var response = await WebServer()
            .gett(url: ServiceGenerator.DOCUMENT_ACCOUNT_TYPE) as http.Response;
        if (response.statusCode == 200) {
          DocumentAccountType().connectDatabase(response);
        }
        update(item_Started_to_download: 'document account type downloaded');
        break;
      case 'DYNAMIC_FORMULA':
        print('inside DYNAMIC_FORMULA');
        var response = await WebServer()
            .gett(url: ServiceGenerator.DYNAMIC_FORMULA) as http.Response;
        if (response.statusCode == 200) {
          DynamicFormula().connectDatabase(response);
        }
        update(item_Started_to_download: 'dynamic formula downloaded');
        break;
      case 'PRODUCT_PRICE_LIST':
        print('inside PRODUCT_PRICE_LIST');
        var response = await WebServer()
            .gett(url: ServiceGenerator.PRODUCT_PRICE_LIST) as http.Response;
        if (response.statusCode == 200) {
          SharedPrefernce().connectDatabase({'api_type': 'fetch'});
          ProductPriceList().connectDatabase(response);
          PriceLevel().connectDatabase(response);
        }
        update(item_Started_to_download: 'product pricelist downloaded');
        break;
      case 'STOCK_LOCATIONS':
        print('inside STOCK_LOCATIONS');
        var response = await WebServer()
            .gett(url: ServiceGenerator.STOCK_LOCATIONS) as http.Response;
        if (response.statusCode == 200) {
          StockLocations().connectDatabase(response);
        }
        update(item_Started_to_download: 'stock locations downloaded');
        break;
      case 'DOCUMENT_PRICE_LEVELS':
        print('inside DOCUMENT_PRICE_LEVELS');
        var response = await WebServer()
            .gett(url: ServiceGenerator.DOCUMENT_PRICE_LEVEL) as http.Response;
        if (response.statusCode == 200) {
          DocumentPriceLevel().connectDatabase(response);
        }
        update(item_Started_to_download: 'document price level downloaded');
        break;
    }
  }

  start() async {
    UserModel user = await UserDatabase().getUser();
    var respnse = await postDeviceKey();
    if (respnse) {
      entities.forEach((element) {
        categorise(element);
      });
    } else {
      showDialog(
          context: DownloadPageState.contextt,
          builder: ((BuildContext context) {
            return AlertDialog(
                title: const Text("Error"),
                content: const Text(
                    "Unable to login. Yo have no access to this resourse"),
                actions: [
                  TextButton(
                      onPressed: (() {
                        StaticFields.user.username = user.username;

                        StaticFields.user.userPassword = user.userPassword;
                        Navigator.pop(context);
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginPage()));
                      }),
                      child: const Text("OK",
                          style: TextStyle(color: Colors.orange)))
                ]);
          }));
    }
  }

  Future<bool> postDeviceKey() async {
    var uuid = Uuid();
    UserModel user = await UserDatabase().getUser();
    var deviceKey = uuid.v5(Uuid.NAMESPACE_URL, 'com.aitrich.android.orderpro');
    var appVersion = await AppVersionUtils().getAppVersion();

    print("$deviceKey $appVersion");

    String token = StaticFields.token;
    var response = await http.post(
        Uri.http(ServiceGenerator.URL, "/api/register-device",
            {"deviceKey": deviceKey, "buildVersion": appVersion}),
        headers: {
          'Authorization': 'Bearer $token',
          'Content-Type': 'application/json'
        });
    print(response.body);
    if (response.statusCode == 200) {
      print(user.username);
      StaticFields.user = user;
      print(response.body);
      return true;
    } else {
      return false;
    }
  }
}
