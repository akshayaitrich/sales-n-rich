final String PREF_DEVICE_UUID = "device_uuid";
final String PREF_APP_VERSION = "app_version";
final String PREF_APP_VERSION_NAME = "app_version_name";
final String PREF_APP_VERSION_DATE = "app_version_DATE";
final String PREF_KEY_COMPANY_NAME = "company_name";
final String PREF_KEY_AUTHORIZATION_TOKEN = "auth_token";
final String PREF_KEY_MASTER_DOWNLOAD_CODE = "mater_download_code";
final String PREF_KEY_MASTER_DOWNLOAD_STATUS = "mater_download_status";
final String PREF_KEY_FCM_TOKEN = "fcm_token";
final String PREF_KEY_NOTIFICATION_ID = "notification_id";
final String PREF_KEY_IMAGE_UPLOAD_SERVICE_STATUS =
    "image_upload_service_status";
final String PREF_KEY_VISIT_SEND_SERVICE_STATUS = "visit_send_service_status";
final String PREF_KEY_LIVE_LOCATION_SERVICE_STATUS =
    "visit_live_location_service_status";
final String PREF_KEY_AUTO_UPDATE_SERVICE_STATUS = "auto_upload_service_status";
final String PREF_KEY_IS_ATTANDACE = "is_attandace";
final String PREF_KEY_ATTANDANCE = "attandance";
final String PREF_KEY_DOCUMENT_UNIQUE_NUMBER = "document_unique_number";
final String PREF_KEY_CURRENT_DATE = "current_date";
final String PREF_KEY_COUNT_EXPIRY_DIALOG = "expiry_count";

final String PREF_ATTENDANCE_DAY = "attendance_day";

final String PREF_ATTENDANCE_SUB_GROUP_ID = "attendance_sub_group_id";
final String PREF_ATTENDANCE_SUB_GROUP_NAME = "attendance_sub_group_name";
final String PREF_ATTENDANCE_SUB_GROUP_STATUS = "attendance_sub_group_status";

final String PREF_KEY_TODAY_ONE_TIME_DAY_PLAN_UPDATE_STATUS =
    "one_time_day_plan_update_status";

final String PREF_KEY_TODAY_ONE_TIME_MASTER_DATA_UPDATE_STATUS =
    "one_time_master_data_update_status";
final String PREF_KEY_USERNAME = "user_name";
final String PREF_KEY_PASSWORD = "password";
final String PREF_KEY_FIRST_TIME_USERNAME = "first_time_user_name";
final String PREF_KEY_LOGIN_TIME_UPDATION_ALERTER =
    "login_time_updation_alerter";
final String PREF_KEY_FIRST_TIME_PASSWORD = "first_time_password";
final String PREF_KEY_ACCOUNTING_VOUCHER_MODE = "accounting_voucher_mode";
final String PREF_KEY_TEXT_SIZE = "text_size";
final String PREF_KEY_INVENTORY_FILTERING_STATUS = "inventory_filtering_status";
final String PREF_KEY_INVENTORY_PREV_NEXT_STATUS = "inventory_prev_next_status";
final String PREF_KEY_DEFAULT_PRODUCT_LOADING_FROM =
    "default_product_loading_from";
final String PREF_DAYPLAN_CONFIG_POS = "dayplan_config_position";
final String PREF_PLAN_DATE = "plane_date";
final String PREF_CURRENT_PLAN_POS = "plan_pos";
final String PREF_PRODUCT_GROUP = "product_group";
final String PREF_PRODUCT_CATAGORY = "product_catagory";
final String PREF_SET_VALUE = "set_value";
final String PREF_SCAN_BUTTON_STATUS = "scan_button_status";
final String PREF_KEY_LAST_SYNC_DATE = "last_sync_date";
final String PREF_KEY_API_TYPE = "api_type";
final String PREF_KEY_LAST_VISITED_URL = "last_visited_url";
final String PREF_KEY_LAST_SELECTED_TERRITORY = "last_selected_territory";

final String PREF_KEY_BASE_URL = "base_url";
final String PREF_KEY_WEB_VIEW_URL = "web_view_url";
final String PREF_KEY_SELECTED_VEHICLE_MASTER_PID = "selectedVehicleMasterPid";
final String PREF_VEHICLE_SELECTED_DAY = "vehicle_selected_day";

//inventory search purpose start
final String PREF_KEY_SAVED_CURRENT_DAY = "saved_current_day";
final String PREF_KEY_SAVED_PROCESSING_STATUS = "saved_processing_status";
final String PREF_KEY_SAVED_PERIOD = "saved_period";
final String PREF_KEY_SAVED_PERIOD_START_DATE = "saved_period_start_date";
final String PREF_KEY_SAVED_PERIOD_END_DATE = "actual_period_end_date";

//snapshot DashBoard
final String PREF_KEY_SENT_DAY = "Day";
final String PREF_KEY_SENT_MONTH = "Month";
final String PREF_SUPPLIER_SELECTION_STATUS = "supplier_selection_status";
final String PREF_SUPPLIER_ACCOUNT_PID = "supplier_account_pid";
final String PREF_STOCK_LOCATION_NAME = "stock_location_name";

//inventory search purpose end

// intent extra key
final String INTENT_EXTRA_NOTIFICATION = "notification_extra";
final String PREF_KEY_NOTFICATION_BADGE_COUNT = "notification_badge_count";
