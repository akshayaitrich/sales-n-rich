import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:package_info/package_info.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:salesnrich/Controller/APIs.dart';
import 'package:salesnrich/WebServer.dart';

Future<bool> askPermission() async {
  var status = await Permission.phone.status;
  if (status.isDenied || status.isUndetermined) {
    await Permission.phone.request();
  }
  var location = await Permission.location.status;
  if (location.isDenied || location.isUndetermined) {
    await Permission.location.request();
  }
  var storage = await Permission.storage.status;
  if (storage.isDenied || storage.isUndetermined) {
    await Permission.storage.request();
  }
  var camera = await Permission.camera.status;
  if (camera.isDenied || camera.isUndetermined) {
    await Permission.camera.request();
  }
  if (status.isGranted &&
      location.isGranted &&
      storage.isGranted &&
      camera.isGranted) {
    return true;
  } else {
    return false;
  }
}

dynamic getDeviceKey() async {
  askPermission();
  if (Platform.isAndroid) {
    var platform = MethodChannel('platformCS');
    try {
      var version = await PackageInfo.fromPlatform();
      var uuid = await platform.invokeMethod('Key');
      //uuid got in 2/26/2021 = 'b6e8ec7c-1975-3d24-b5c2-6e67710aa740'
      var data = {'deviceKey': uuid, 'buildVersion': version.version};
      WebServer().post(url: ServiceGenerator.POST_DEVICE_KEY, data: data);
      return [version.version, uuid];
    } on PlatformException catch (e) {
      print('$e');
    }
  }
}

class AppVersionUtils {
  Future<String> getAppName() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return packageInfo.appName;
  }

  Future<String> getAppVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return packageInfo.version;
  }
}

class NetworkUtil {
  // The constant TYPE_WIFI.
  static int TYPE_WIFI = 1;

  // The constant TYPE_MOBILE.
  static int TYPE_MOBILE = 2;

  //The constant TYPE_NOT_CONNECTED.
  static int TYPE_NOT_CONNECTED = 0;

  Future<int> getConnectivityStatus() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.none) {
      return TYPE_NOT_CONNECTED;
    } else if (connectivityResult == ConnectivityResult.mobile) {
      return TYPE_MOBILE;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return TYPE_WIFI;
    }
    return TYPE_NOT_CONNECTED;
  }

  Future<int> isAirplaneModeOn() async {
    
  }
}

class ToastHelper {
  show(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        fontSize: 16.0);
  }
}
