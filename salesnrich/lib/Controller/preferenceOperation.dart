import 'package:salesnrich/Controller/constans.dart';
import 'package:salesnrich/Model/database.dart';
import 'package:salesnrich/Model/sharedPreference.dart';

class Preference {
  setApiType(String value) {
    SharedPrefernce().connectDatabase({PREF_KEY_API_TYPE: value});
  }

  Future<String> getApiType() async {
    var li =
        await OurDatabase(table: SharedPrefernce().table).getTables() as List;
    var mp = li.first as Map<String, dynamic>;
    return mp[PREF_KEY_API_TYPE] as String ?? "";
  }

  saveDefaultProductLoadingFrom(String value) {
    SharedPrefernce()
        .connectDatabase({PREF_KEY_DEFAULT_PRODUCT_LOADING_FROM: value});
  }

  Future<String> getDefaultProductLoadingFrom() async {
    var li =
        await OurDatabase(table: SharedPrefernce().table).getTables() as List;
    var mp = li.first as Map<String, dynamic>;
    return mp[PREF_KEY_DEFAULT_PRODUCT_LOADING_FROM] as String ?? "";
  }

  saveInventoryFilteringStatus(bool value) {
    SharedPrefernce()
        .connectDatabase({PREF_KEY_INVENTORY_FILTERING_STATUS: value});
  }

  Future<bool> getInventoryFilteringStatus() async {
    var li =
        await OurDatabase(table: SharedPrefernce().table).getTables() as List;
    var mp = li.first as Map<String, dynamic>;

    if (mp[PREF_KEY_INVENTORY_FILTERING_STATUS] == 1) {
      return true;
    } else
      return false;
  }

//PREF_KEY_INVENTORY_PREV_NEXT_STATUS
  saveInventoryPrevNextStatus(bool value) {
    SharedPrefernce()
        .connectDatabase({PREF_KEY_INVENTORY_PREV_NEXT_STATUS: value});
  }

  Future<bool> getInventoryPrevNextStatus() async {
    var li =
        await OurDatabase(table: SharedPrefernce().table).getTables() as List;
    var mp = li.first as Map<String, dynamic>;

    if (mp[PREF_KEY_INVENTORY_PREV_NEXT_STATUS] == 1) {
      return true;
    } else
      return false;
  }

  saveLastSelectedTerritoryName(String value) {
    SharedPrefernce()
        .connectDatabase({PREF_KEY_LAST_SELECTED_TERRITORY: value});
  }

  Future<String> getLastSelectedTerritoryName() async {
    var li =
        await OurDatabase(table: SharedPrefernce().table).getTables() as List;
    var mp = li.first as Map<String, dynamic>;
    return mp[PREF_KEY_LAST_SELECTED_TERRITORY] as String ?? "";
  }

//PREF_KEY_DEFAULT_PRODUCT_LOADING_FROM
  saveDefaulproductloadingfrom(bool value) {
    SharedPrefernce()
        .connectDatabase({PREF_KEY_DEFAULT_PRODUCT_LOADING_FROM: value});
  }

  Future<bool> getDefaulproductloadingfrom() async {
    var li =
        await OurDatabase(table: SharedPrefernce().table).getTables() as List;
    var mp = li.first as Map<String, dynamic>;
    if (mp[PREF_KEY_DEFAULT_PRODUCT_LOADING_FROM] == 1) {
      return true;
    } else
      return false;
  }
}
