import 'package:flutter/material.dart';
import 'package:salesnrich/Model/Tables.dart';
import 'package:salesnrich/Model/user.dart';
import 'package:sizer/sizer.dart';

class StaticFields {
  static int downLoadPercentage = 49;
  static String token;
  static bool userexists = false;
  static UserModel user;
  static bool isUserTerritory;

  static Widget getAppbar({String tittle}) {
    return AppBar(
        title: Row(children: [
      Image(
          height: 10.0.h,
          width: 10.0.w,
          image: AssetImage('assets/salesnrichWhite.png')),
      Text(tittle)
    ]));
  }
}

class Eventfindlocation {  
  String fromWhich;
  AccountProfile accountProfile;
  Eventfindlocation(String fromWhich, AccountProfile accountProfile) {
    this.fromWhich = fromWhich;
    this.accountProfile = accountProfile;
  }
}
