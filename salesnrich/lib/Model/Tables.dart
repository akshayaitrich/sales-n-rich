import 'dart:convert';
import 'package:http/http.dart';
import 'package:salesnrich/Controller/enums.dart';
import 'package:salesnrich/Controller/preferenceOperation.dart';
import 'package:salesnrich/Model/database.dart';

//tables for MOBILE_MENU_ITEMS
class MobileMenuItems {
  final String table = 'MobileMenuItems';
  final String query =
      'CREATE TABLE MobileMenuItems(id INT, name STRING, label STRING)';

  var id;
  String name;
  String label;
  MobileMenuItems({this.id, this.name, this.label});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {'id': mp['id'], 'name': mp['name'], 'label': mp['label']};
  }

  MobileMenuItems fromMap(mp) {
    MobileMenuItems items = new MobileMenuItems();
    items.id = mp['id'];
    items.name = mp['name'];
    items.label = mp['label'];
    return items;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(mp));
      });
    }
  }
}

//table for ACCOUNT_USER_DETAIL
class User {
  final String table = 'UserDetail';
  final String query =
      'CREATE TABLE UserDetail(userName STRING, password STRING, isActive BOOLEAN, pid STRING, companyId STRING,' +
          ' companyPid STRING, companyName STRING, login STRING, firstName STRING, lastName STRING, email STRING,' +
          ' mobile STRING, activated BOOLEAN, langKey STRING, deviceKey STRING, gstNumber STRING, companyPhoneNo STRING, employeeName STRING)';

  String userName;
  String password;
  String isActive;
  String pid;
  String companyId;
  String companyPid;
  String companyName;
  String login;
  String firstName;
  String lastName;
  String email;
  String mobile;
  bool activated;
  String langKey;
  String deviceKey;
  String gstNumber;
  String companyPhoneNo;
  String employeeName;

  User({
    this.userName,
    this.password,
    this.isActive,
    this.pid,
    this.companyId,
    this.companyPid,
    this.companyName,
    this.login,
    this.firstName,
    this.lastName,
    this.email,
    this.mobile,
    this.activated,
    this.langKey,
    this.deviceKey,
    this.gstNumber,
    this.companyPhoneNo,
    this.employeeName,
  });

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'userName': mp['userName'],
      'password': mp['password'],
      'isActive': mp['isActive'],
      'pid': mp['pid'],
      'companyId': mp['companyId'],
      'companyPid': mp['companyPid'],
      'companyName': mp['companyName'],
      'login': mp['login'],
      'firstName': mp['firstName'],
      'lastName': mp['lastName'],
      'email': mp['email'],
      'mobile': mp['mobile'],
      'activated': mp['activated'],
      'langKey': mp['langKey'],
      'deviceKey': mp['deviceKey'],
      'gstNumber': mp['gstNumber'],
      'companyPhoneNo': mp['companyPhoneNo'],
      'employeeName': mp['employeeName'],
    };
  }

  User fromMap(mp) {
    User user = new User();
    user.userName = mp['userName'];
    user.password = mp['password'];
    user.isActive = mp['isActive'];
    user.pid = mp['pid'];
    user.companyId = mp['companyId'];
    user.companyPid = mp['companyPid'];
    user.companyName = mp['companyName'];
    user.login = mp['login'];
    user.firstName = mp['firstName'];
    user.lastName = mp['lastName'];
    user.email = mp['email'];
    user.mobile = mp['mobile'];
    user.activated = mp['activated'] == 1 ? true : false;
    user.langKey = mp['langKey'];
    user.deviceKey = mp['deviceKey'];
    user.gstNumber = mp['gstNumber'];
    user.companyPhoneNo = mp['companyPhoneNo'];
    user.employeeName = mp['employeeName'];
    return user;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var mp = json.decode(response.body) as Map<String, dynamic>;
      mp.forEach((key, value) {
        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(mp));
      });
    }
  }
}

//tables for EXECUTIVE_ACTIVITY
class ExecutiveActivity {
  final String table = 'ExecutiveActivity';
  final String query =
      'CREATE TABLE ExecutiveActivity(id INT, pid STRING, name STRING, alias STRING, description STRING, hasDefaultAccount BOOLEAN,' +
          ' target DOUBLE, achieved DOUBLE, today DOUBLE, planThrouchOnly BOOLEAN, completePlans BOOLEAN, customerLocationSpendTimeRequiredStatus BOOLEAN,' +
          ' interimSave BOOLEAN, contactManagement STRING, hasSecondarySales BOOLEAN)';

  var id;
  String pid;
  String name;
  String alias;
  String description;
  bool hasDefaultAccount;
  double target;
  double achieved;
  double today;
  bool planThrouchOnly;
  bool completePlans;
  bool customerLocationSpendTimeRequiredStatus;
  bool interimSave;
  String contactManagement;
  bool hasSecondarySales;

  ExecutiveActivity({
    this.id,
    this.pid,
    this.name,
    this.alias,
    this.description,
    this.hasDefaultAccount,
    this.target,
    this.achieved,
    this.today,
    this.planThrouchOnly,
    this.completePlans,
    this.customerLocationSpendTimeRequiredStatus,
    this.interimSave,
    this.contactManagement,
    this.hasSecondarySales,
  });

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'name': mp['name'],
      'alias': mp['alias'],
      'description': mp['description'],
      'hasDefaultAccount': mp['hasDefaultAccount'],
      'target': mp['target'],
      'achieved': mp['achieved'],
      'today': mp['today'],
      'planThrouchOnly': mp['planThrouchOnly'],
      'completePlans': mp['completePlans'],
      'customerLocationSpendTimeRequiredStatus':
          mp['customerLocationSpendTimeRequiredStatus'],
      'interimSave': mp['interimSave'],
      'contactManagement': mp['contactManagement'],
      'hasSecondarySales': mp['hasSecondarySales']
    };
  }

  ExecutiveActivity fromMap(mp) {
    ExecutiveActivity activity = new ExecutiveActivity();
    activity.id = mp['id'];
    activity.pid = mp['pid'];
    activity.name = mp['name'];
    activity.alias = mp['alias'];
    activity.description = mp['description'];
    activity.hasDefaultAccount = mp['hasDefaultAccount'] == 1 ? true : false;

    activity.target = mp['target'];
    activity.achieved = mp['achieved'];
    activity.today = mp['today'];
    activity.planThrouchOnly = mp['planThrouchOnly'] == 1 ? true : false;
    activity.completePlans = mp['completePlans'] == 1 ? true : false;
    activity.customerLocationSpendTimeRequiredStatus =
        mp['customerLocationSpendTimeRequiredStatus'] == 1 ? true : false;
    activity.interimSave = mp['interimSave'] == 1 ? true : false;
    activity.contactManagement = mp['contactManagement'];
    activity.hasSecondarySales = mp['hasSecondarySales'] == 1 ? true : false;
    return activity;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var activity = [];
      var activityAccountTypeList = [];
      var activityDocumentList = [];
      var li = json.decode(response.body) as List;
      if (await OurDatabase(table: ExecutiveActivity().table)
          .databaseExists()) {
        OurDatabase(table: ExecutiveActivity().table).deleteAll();
      }
      li.forEach((element) {
        var mp = element as Map<String, dynamic>;
        mp.addAll({'today': 0.0});
        mp.addAll({'achieved': 0.0});
        mp.addAll({'target': 0.0});
        print(mp);
        //TODO
        // for (ExecutiveActivityUpdate executiveActivityUpdate : executiveActivityUpdatesList) {
        //          if (entity.getPid().equals(executiveActivityUpdate.getPid())) {
        //              activity.setToday(executiveActivityUpdate.getToday());
        //         }
        //     }
        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(mp));
      });
    }
  }
}

//tables for EXECUTIVE_ACTIVITY
class ActivityAccountType {
  final String table = 'ActivityAccountType';
  final String query =
      'CREATE TABLE ActivityAccountType(id INT, pid STRING, activityPid STRING, name STRING, alias STRING, description STRING)';

  var id;
  String pid;
  String activityPid;
  String name;
  String alias;
  String description;

  ActivityAccountType({
    this.id,
    this.pid,
    this.activityPid,
    this.name,
    this.alias,
    this.description,
  });

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'activityPid': mp['activityPid'],
      'name': mp['name'],
      'alias': mp['alias'],
      'description': mp['description']
    };
  }

  ActivityAccountType fromMap(mp) {
    ActivityAccountType accountType = new ActivityAccountType();
    accountType.id = mp['id'];
    accountType.pid = mp['pid'];
    accountType.activityPid = mp['activityPid'];
    accountType.name = mp['name'];
    accountType.alias = mp['alias'];
    accountType.description = mp['description'];
    return accountType;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body);
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        var accountTypeList = mp['activityAccountTypes'] as List;
        accountTypeList.forEach((element) {
          var map = element as Map<String, dynamic>;
          map.addAll({'activityPid': mp['pid']});
          OurDatabase(table: this.table, query: this.query)
              .insertTable(toMap(map));
        });
      });
    }
  }
}

//tables for ACTIVITY_DOCUMENT
class ActivityDocument {
  final String table = 'ActivityDocument';
  final String query =
      'CREATE TABLE ActivityDocument(id INT, pid STRING, activityPid STRING, name STRING, alias STRING, description STRING, documentType STRING,' +
          ' isEdit BOOLEAN, numberOfItems DOUBLE, totalAmount DOUBLE, activityAccount STRING, save BOOLEAN, sourceStockLocationPid STRING,' +
          ' sourceStockLocationName STRING, destinationStockLocationPid STRING, destinationStockLocationName STRING, batchEnabled BOOLEAN, promptStockLocation BOOLEAN,' +
          ' activityDocRequired BOOLEAN, activityDocSortOrder INT, singleVoucherMode BOOLEAN,  photoMandatory BOOLEAN,  isTakeImageFromGallery BOOLEAN,' +
          ' imageOption BOOLEAN, mode STRING, stockFlow STRING, qrCodeEnabled BOOLEAN)';

  var id;
  String pid;
  String activityPid;
  String name;
  String alias;
  String description;
  String documentType;
  bool isEdit;
  double numberOfItems;
  double totalAmount;
  String activityAccount;
  bool save;
  String sourceStockLocationPid;
  String sourceStockLocationName;
  String destinationStockLocationPid;
  String destinationStockLocationName;
  bool batchEnabled;
  bool promptStockLocation;
  bool activityDocRequired;
  var activityDocSortOrder;
  bool singleVoucherMode;
  bool photoMandatory;
  bool isTakeImageFromGallery;
  bool imageOption;
  String mode;
  String stockFlow;
  bool qrCodeEnabled = false;

  ActivityDocument(
      {this.id,
      this.pid,
      this.activityPid,
      this.name,
      this.alias,
      this.description,
      this.documentType,
      this.isEdit,
      this.numberOfItems,
      this.totalAmount,
      this.activityAccount,
      this.save,
      this.sourceStockLocationPid,
      this.sourceStockLocationName,
      this.destinationStockLocationPid,
      this.destinationStockLocationName,
      this.batchEnabled,
      this.promptStockLocation,
      this.activityDocRequired,
      this.activityDocSortOrder,
      this.singleVoucherMode,
      this.photoMandatory,
      this.isTakeImageFromGallery,
      this.imageOption,
      this.mode,
      this.stockFlow,
      this.qrCodeEnabled});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'activityPid': mp['activityPid'],
      'name': mp['name'],
      'alias': mp['alias'],
      'description': mp['description'],
      'documentType': mp['documentType'],
      'isEdit': mp['isEdit'],
      'numberOfItems': mp['numberOfItems'],
      'totalAmount': mp['totalAmount'],
      'activityAccount': mp['activityAccount'],
      'save': mp['save'],
      'sourceStockLocationPid': mp['sourceStockLocationPid'],
      'sourceStockLocationName': mp['sourceStockLocationName'],
      'destinationStockLocationPid': mp['destinationStockLocationPid'],
      'destinationStockLocationName': mp['destinationStockLocationName'],
      'batchEnabled': mp['batchEnabled'],
      'promptStockLocation': mp['promptStockLocation'],
      'activityDocRequired': mp['activityDocRequired'],
      'activityDocSortOrder': mp['activityDocSortOrder'],
      'singleVoucherMode': mp['singleVoucherMode'],
      'photoMandatory': mp['photoMandatory'],
      'isTakeImageFromGallery': mp['isTakeImageFromGallery'],
      'imageOption': mp['imageOption'],
      'mode': mp['mode'],
      'stockFlow': mp['stockFlow'],
      'qrCodeEnabled': mp['qrCodeEnabled']
    };
  }

  ActivityDocument fromMap(mp) {
    ActivityDocument actdoc = new ActivityDocument();
    actdoc.id = mp['id'];
    actdoc.pid = mp['pid'];
    actdoc.activityPid = mp['activityPid'];
    actdoc.name = mp['name'];
    actdoc.alias = mp['alias'];
    actdoc.description = mp['description'];
    actdoc.documentType = mp['documentType'];
    actdoc.isEdit = mp['isEdit'] == 1 ? true : false;
    actdoc.activityPid = mp['activityPid'];
    actdoc.numberOfItems = mp['numberOfItems'];
    actdoc.totalAmount = mp['totalAmount'];
    actdoc.activityAccount = mp['activityAccount'];
    actdoc.save = mp['save'] == 1 ? true : false;
    actdoc.sourceStockLocationPid = mp['sourceStockLocationPid'];
    actdoc.sourceStockLocationName = mp['sourceStockLocationName'];
    actdoc.destinationStockLocationPid = mp['destinationStockLocationPid'];
    actdoc.destinationStockLocationName = mp['destinationStockLocationName'];
    actdoc.batchEnabled = mp['batchEnabled'] == 1 ? true : false;
    actdoc.promptStockLocation = mp['promptStockLocation'] == 1 ? true : false;
    actdoc.activityDocRequired = mp['activityDocRequired'] == 1 ? true : false;
    actdoc.activityDocSortOrder = mp['activityDocSortOrder'];
    actdoc.singleVoucherMode = mp['singleVoucherMode'] == 1 ? true : false;
    actdoc.photoMandatory = mp['photoMandatory'] == 1 ? true : false;
    actdoc.isTakeImageFromGallery =
        mp['isTakeImageFromGallery'] == 1 ? true : false;
    actdoc.imageOption = mp['imageOption'] == 1 ? true : false;
    actdoc.mode = mp['mode'];
    actdoc.stockFlow = mp['stockFlow'];
    actdoc.qrCodeEnabled = mp['qrCodeEnabled'] == 1 ? true : false;
    return actdoc;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body);
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        var documentList = mp['documents'] as List;
        documentList.forEach((element) {
          var map = element as Map<String, dynamic>;
          map.addAll({'activityPid': mp['pid']});
          map.addAll({'isEdit': false});
          OurDatabase(table: this.table, query: this.query)
              .insertTable(toMap(map));
        });
      });
    }
  }
}

//table ACTIVITY_GROUP
class ActivityGroup {
  final String table = 'ActivityGroup';
  final String query =
      'CREATE TABLE ActivityGroup(id INT, pid STRING, name STRING, alias STRING, description STRING, target DOUBLE, achieved DOUBLE, today DOUBLE)';

  var id;
  String pid;
  String name;
  String alias;
  String description;
  double target;
  double achieved;
  double today;

  ActivityGroup(
      {this.id,
      this.pid,
      this.name,
      this.alias,
      this.description,
      this.target,
      this.achieved,
      this.today});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'name': mp['name'],
      'alias': mp['alias'],
      'description': mp['description'],
      'target': mp['target'],
      'achieved': mp['achieved'],
      'today': mp['today']
    };
  }

  ActivityGroup fromMap(mp) {
    ActivityGroup actgroup = ActivityGroup();
    actgroup.id = mp['id'];
    actgroup.pid = mp['pid'];
    actgroup.name = mp['name'];
    actgroup.alias = mp['alias'];
    actgroup.description = mp['description'];
    actgroup.target = mp['target'];
    actgroup.achieved = mp['achieved'];
    actgroup.today = mp['today'];
    return actgroup;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        mp.addAll({'target': 0.0});
        mp.addAll({'achieved': 0.0});
        mp.addAll({'today': 0.0});
        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(mp));
      });
    }
  }
}

//tble ACTIVITY_USER_TARGET
class ActivityUserTarget {
  final String table = 'ActivityUserTarget';
  final String query =
      'CREATE TABLE ActivityUserTarget(id INT, pid STRING, startDate STRING, endDate STRING, activityPid STRING, activityName STRING, userPid STRING, userName STRING, targetNumber STRING, achivedNumber STRING)';

  var id;
  String pid;
  String startDate;
  String endDate;
  String activityPid;
  String activityName;
  String userPid;
  String userName;
  String targetNumber;
  String achivedNumber;

  ActivityUserTarget(
      {this.id,
      this.pid,
      this.startDate,
      this.endDate,
      this.activityPid,
      this.activityName,
      this.userPid,
      this.userName,
      this.targetNumber,
      this.achivedNumber});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'startDate': mp['startDate'],
      'endDate': mp['endDate'],
      'activityPid': mp['activityPid'],
      'activityName': mp['activityName'],
      'userPid': mp['userPid'],
      'userName': mp['userName'],
      'targetNumber': mp['targetNumber'],
      'achivedNumber': mp['achivedNumber']
    };
  }

  ActivityUserTarget fromMap(mp) {
    ActivityUserTarget acttarget = ActivityUserTarget();
    acttarget.id = mp['id'];
    acttarget.pid = mp['pid'];
    acttarget.startDate = mp['startDate'];
    acttarget.endDate = mp['endDate'];
    acttarget.activityPid = mp['activityPid'];
    acttarget.activityName = mp['activityName'];
    acttarget.userPid = mp['userPid'];
    acttarget.userName = mp['userName'];
    acttarget.targetNumber = mp['targetNumber'];
    acttarget.achivedNumber = mp['achivedNumber'];
    return acttarget;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(mp));
      });
    }
  }
}

//table ACTIVITY_GROUP_TARGET
class ActivityGroupTarget {
  final String table = 'ActivityGroupTarget';
  final String query =
      'CREATE TABLE ActivityGroupTarget(id INT, pid STRING, startDate STRING, endDate STRING, activityPid STRING, activityName STRING, userPid STRING, userName STRING, targetNumber STRING, achivedNumber STRING)';

  var id;
  String pid;
  String startDate;
  String endDate;
  String activityPid;
  String activityName;
  String userPid;
  String userName;
  String targetNumber;
  String achivedNumber;

  ActivityGroupTarget(
      {this.id,
      this.pid,
      this.startDate,
      this.endDate,
      this.activityPid,
      this.activityName,
      this.userPid,
      this.userName,
      this.targetNumber,
      this.achivedNumber});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'startDate': mp['startDate'],
      'endDate': mp['endDate'],
      'activityPid': mp['activityPid'],
      'activityName': mp['activityName'],
      'userPid': mp['userPid'],
      'userName': mp['userName'],
      'targetNumber': mp['targetNumber'],
      'achivedNumber': mp['achivedNumber']
    };
  }

  ActivityGroupTarget fromMap(mp) {
    ActivityGroupTarget actgrouptarget = ActivityGroupTarget();
    actgrouptarget.id = mp['id'];
    actgrouptarget.pid = mp['pid'];
    actgrouptarget.startDate = mp['startDate'];
    actgrouptarget.endDate = mp['endDate'];
    actgrouptarget.activityPid = mp['activityPid'];
    actgrouptarget.activityName = mp['activityName'];
    actgrouptarget.userPid = mp['userPid'];
    actgrouptarget.userName = mp['userName'];
    actgrouptarget.targetNumber = mp['targetNumber'];
    actgrouptarget.achivedNumber = mp['achivedNumber'];
    return actgrouptarget;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(mp));
      });
    }
  }
}

//table ACCOUNT_PROFILE
class AccountProfile {
  final String table = 'AccountProfile';
  final String query =
      'CREATE TABLE AccountProfile(id INT, pid STRING, name STRING, alias STRING, description STRING, accountTypePid STRING, accountTypeName STRING, address STRING, city STRING, location STRING, pin STRING, latitude DOUBLE, longitude DOUBLE,phone1 STRING, phone2 STRING, email1 STRING, email2 STRING, accountStatus STRING, defaultPriceLevelPid STRING, defaultPriceLevelName STRING, territoryPid STRING, territoryName STRING, tinNo STRING, closingBalance DOUBLE, defaultDiscountPercentage DOUBLE,leadToCashStage STRING, contactPerson STRING, gstRegistrationType STRING, customerId STRING, dataSourceType STRING, todayClosingBalance DOUBLE, associationChecked BOOLEAN )';
  var id;
  String pid;
  String name;
  String alias;
  String description;
  String accountTypePid;
  String accountTypeName;
  String address;
  String city;
  String location;
  String pin;
  double latitude;
  double longitude;
  String phone1;
  String phone2;
  String email1;
  String email2;
  String accountStatus;
  String defaultPriceLevelPid;
  String defaultPriceLevelName;
  String territoryPid;
  String territoryName;
  String tinNo;
  double closingBalance;
  double defaultDiscountPercentage;
  String leadToCashStage;
  String contactPerson;
  String gstRegistrationType;
  String customerId;
  String dataSourceType;
  double todayClosingBalance;
  bool associationChecked;

  AccountProfile(
      {this.id,
      this.pid,
      this.name,
      this.alias,
      this.description,
      this.accountTypePid,
      this.accountTypeName,
      this.address,
      this.city,
      this.location,
      this.pin,
      this.latitude,
      this.longitude,
      this.phone1,
      this.phone2,
      this.email1,
      this.email2,
      this.accountStatus,
      this.defaultPriceLevelPid,
      this.defaultPriceLevelName,
      this.territoryPid,
      this.territoryName,
      this.tinNo,
      this.closingBalance,
      this.defaultDiscountPercentage,
      this.leadToCashStage,
      this.contactPerson,
      this.gstRegistrationType,
      this.customerId,
      this.dataSourceType,
      this.todayClosingBalance,
      this.associationChecked});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'name': mp['name'],
      'alias': mp['alias'],
      'description': mp['description'],
      'accountTypePid': mp['accountTypePid'],
      'accountTypeName': mp['accountTypeName'],
      'address': mp['address'],
      'city': mp['city'],
      'location': mp['location'],
      'pin': mp['pin'],
      'latitude': mp['latitude'],
      'longitude': mp['longitude'],
      'phone1': mp['phone1'],
      'phone2': mp['phone2'],
      'email1': mp['email1'],
      'email2': mp['email2'],
      'accountStatus': mp['accountStatus'],
      'defaultPriceLevelPid': mp['defaultPriceLevelPid'],
      'defaultPriceLevelName': mp['defaultPriceLevelName'],
      'territoryPid': mp['territoryPid'],
      'territoryName': mp['territoryName'],
      'tinNo': mp['tinNo'],
      'closingBalance': mp['closingBalance'],
      'defaultDiscountPercentage': mp['defaultDiscountPercentage'],
      'leadToCashStage': mp['leadToCashStage'],
      'contactPerson': mp['contactPerson'],
      'gstRegistrationType': mp['gstRegistrationType'],
      'customerId': mp['customerId'],
      'dataSourceType': mp['dataSourceType'],
      'todayClosingBalance': mp['todayClosingBalance'],
      'associationChecked': mp['associationChecked'],
    };
  }

  AccountProfile fromMap(Map mp, {bool donotSort = false}) {
    AccountProfile accprofile = AccountProfile();
    accprofile.id = mp['id'];
    accprofile.pid = mp['pid'].toString();
    accprofile.name = mp['name'];
    accprofile.alias = mp['alias'].toString();
    accprofile.description = mp['description'].toString();
    accprofile.accountTypePid = mp['accountTypePid'];
    accprofile.accountTypeName = mp['accountTypeName'].toString();
    accprofile.address = mp['address'].toString();
    accprofile.city = mp['city'];
    accprofile.location = mp['location'];
    accprofile.pin = mp['pin'].toString();
    accprofile.latitude = mp['latitude'];
    accprofile.longitude = mp['longitude'];
    accprofile.phone1 = mp['phone1'].toString();
    accprofile.phone2 = mp['phone2'].toString();
    accprofile.email1 = mp['email1'];
    accprofile.email2 = mp['email2'];
    accprofile.accountStatus = mp['accountStatus'];
    accprofile.defaultPriceLevelPid = mp['defaultPriceLevelPid'];
    accprofile.defaultPriceLevelName = mp['defaultPriceLevelName'];
    accprofile.territoryPid = mp['territoryPid'];
    accprofile.territoryName = mp['territoryName'];
    accprofile.tinNo = mp['tinNo'].toString();
    accprofile.closingBalance = mp['closingBalance'];
    accprofile.defaultDiscountPercentage = mp['defaultDiscountPercentage'];
    accprofile.leadToCashStage = mp['leadToCashStage'].toString();
    accprofile.contactPerson = mp['contactPerson'].toString();
    accprofile.gstRegistrationType = mp['gstRegistrationType'].toString();
    accprofile.customerId = mp['customerId'].toString();
    accprofile.dataSourceType = mp['dataSourceType'].toString();
    accprofile.todayClosingBalance = mp['todayClosingBalance'];
    accprofile.associationChecked =
        mp['associationChecked'] == 1 ? true : false;
    return accprofile;
  }

  connectDatabase(Response response) async {
    List<dynamic> newProfiles = [];
    if (!await OurDatabase(table: this.table).databaseExists()) {
      String apiType = await Preference().getApiType();
      var accountProfileList = json.decode(response.body) as List;
      if (apiType == "fetch") {
        if (await OurDatabase(table: AccountProfile().table).databaseExists()) {
          OurDatabase(table: AccountProfile().table).deleteAll();
        }
        accountProfileList.forEach((element) {
          var mp = element as Map<String, dynamic>;
          OurDatabase(table: this.table, query: this.query)
              .insertTable(toMap(mp));
        });
      } else {
        var accountProfileListdb =
            await OurDatabase(table: AccountProfile().table).getTables();

        if (accountProfileListdb.length > 0) {
          for (int i = 0; i < accountProfileList.length; i++) {
            bool hasFound = false;
            var accountprofile = accountProfileList[i] as Map<String, dynamic>;
            int j = 0;
            for (j = 0; j < accountProfileListdb.length; j++) {
              var accountProfileDb =
                  accountProfileListdb[j] as Map<String, dynamic>;
              if (accountprofile['pid'] == accountProfileDb['pid']) {
                hasFound = true;
                break;
              }
            }

            if (!hasFound) {
              newProfiles.add(accountprofile);
            } else {
              // await OurDatabase(table: AccountProfile().table).
              accountProfileListdb.remove(j);
            }
          }
        } else {
          accountProfileList.forEach((element) {
            var mp = element as Map<String, dynamic>;
            OurDatabase(table: this.table, query: this.query)
                .insertTable(toMap(mp));
          });
        }
      }
    }
  }
}

//table TERRITORY
class Territory {
  final String table = 'Territory';
  final String query =
      'CREATE TABLE Territory(id INT, pid STRING, name STRING, alias STRING, description STRING, isUserTerritory BOOLEAN)';

  var id;
  String pid;
  String name;
  String alias;
  String description;
  bool isUserTerritory;

  Territory(
      {this.id,
      this.pid,
      this.name,
      this.alias,
      this.description,
      this.isUserTerritory});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'name': mp['name'],
      'alias': mp['alias'],
      'description': mp['description'],
      'isUserTerritory': mp['isUserTerritory']
    };
  }

  Territory fromMap(mp) {
    Territory territories = Territory();
    territories.id = mp['id'];
    territories.pid = mp['pid'];
    territories.name = mp['name'];
    territories.alias = mp['alias'];
    territories.description = mp['description'];
    territories.isUserTerritory = mp['isUserTerritory'] == 1 ? true : false;
    return territories;
  }

  connectDatabase(Response response, bool isUserTerritory) async {
    //if (!await OurDatabase(table: this.table).databaseExists()) {
    var lists = json.decode(response.body) as List;

    lists.forEach((element) {
      var mp = element as Map<String, dynamic>;
      mp.addAll({'isUserTerritory': isUserTerritory});
      mp.addAll({'id': null});
      OurDatabase(table: this.table, query: this.query).insertTable(toMap(mp));
    });
    //}
  }
}

//table MOBILE_SETTINGS_CONFIGURATION
class MobileSettingsConfiguration {
  final String table = 'MobileSettingsConfiguration';
  final String query =
      'CREATE TABLE MobileSettingsConfiguration(id INT, taskExecutionOffLineSaveStatus BOOLEAN, taskExecutionOffLineSaveDuration INT, promptAttendanceMarking BOOLEAN, promptDayPlanUpdate BOOLEAN,promptMasterDataUpdate BOOLEAN, attendanceMarkingRequiredStatus BOOLEAN, dayPlanDownloadRequiredStatus BOOLEAN, masterDataUpdateRequiredStatus BOOLEAN,' +
          'buildDueDetail BOOLEAN, showAccountProfileDetails BOOLEAN, showAllActivityCount BOOLEAN, createTerritory BOOLEAN, draftScreenDeleteStatus BOOLEAN, realTimeProductPriceEnabled BOOLEAN, hasPostDatedVoucher BOOLEAN, promptVehicleMaster BOOLEAN, voucherNumberGenerationType STRING, addNewCustomer BOOLEAN,' +
          'preventNegativeStock BOOLEAN, inventoryVoucherUIType STRING, kfcEnabled BOOLEAN, cartType STRING, gpsMandatory BOOLEAN, enableSecondarySales BOOLEAN, enableAttendanceImage BOOLEAN, smartSearch BOOLEAN, salesOrderDownloadPdf BOOLEAN, findLocation BOOLEAN, hasGeoTag BOOLEAN, enableDynamicUnit BOOLEAN,' +
          'enableDiscountRoundOffColumn BOOLEAN, stockLocationProducts BOOLEAN, salesOrderAllocation BOOLEAN, rateWithoutCalculation BOOLEAN, showBestPerformerUpload BOOLEAN, belowPriceLevel BOOLEAN)';

  var id;
  bool taskExecutionOffLineSaveStatus;
  var taskExecutionOffLineSaveDuration;
  bool promptAttendanceMarking;
  bool promptDayPlanUpdate;
  bool promptMasterDataUpdate;
  bool attendanceMarkingRequiredStatus;
  bool dayPlanDownloadRequiredStatus;
  bool masterDataUpdateRequiredStatus;
  bool buildDueDetail;
  bool showAccountProfileDetails;
  bool showAllActivityCount;
  bool createTerritory;
  bool draftScreenDeleteStatus;
  bool realTimeProductPriceEnabled;
  bool hasPostDatedVoucher;
  bool promptVehicleMaster;
  String voucherNumberGenerationType;
  bool addNewCustomer;
  bool preventNegativeStock;
  String inventoryVoucherUIType;
  bool kfcEnabled;
  String cartType;
  bool gpsMandatory;
  bool enableSecondarySales;
  bool enableAttendanceImage;
  bool smartSearch;
  bool salesOrderDownloadPdf;
  bool findLocation;
  bool hasGeoTag;
  bool enableDynamicUnit;
  bool enableDiscountRoundOffColumn;
  bool stockLocationProducts;
  bool salesOrderAllocation;
  bool rateWithoutCalculation;
  bool showBestPerformerUpload;
  bool belowPriceLevel;

  MobileSettingsConfiguration(
      {this.id,
      this.taskExecutionOffLineSaveStatus,
      this.taskExecutionOffLineSaveDuration,
      this.promptAttendanceMarking,
      this.promptDayPlanUpdate,
      this.promptMasterDataUpdate,
      this.attendanceMarkingRequiredStatus,
      this.dayPlanDownloadRequiredStatus,
      this.masterDataUpdateRequiredStatus,
      this.buildDueDetail,
      this.showAccountProfileDetails,
      this.showAllActivityCount,
      this.createTerritory,
      this.draftScreenDeleteStatus,
      this.realTimeProductPriceEnabled,
      this.hasPostDatedVoucher,
      this.promptVehicleMaster,
      this.voucherNumberGenerationType,
      this.addNewCustomer,
      this.preventNegativeStock,
      this.inventoryVoucherUIType,
      this.kfcEnabled,
      this.cartType,
      this.gpsMandatory,
      this.enableSecondarySales,
      this.enableAttendanceImage,
      this.smartSearch,
      this.salesOrderDownloadPdf,
      this.findLocation,
      this.hasGeoTag,
      this.enableDynamicUnit,
      this.enableDiscountRoundOffColumn,
      this.stockLocationProducts,
      this.salesOrderAllocation,
      this.rateWithoutCalculation,
      this.showBestPerformerUpload,
      this.belowPriceLevel});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'taskExecutionOffLineSaveStatus': mp['taskExecutionOffLineSaveStatus'],
      'taskExecutionOffLineSaveDuration':
          mp['taskExecutionOffLineSaveDuration'],
      'promptAttendanceMarking': mp['promptAttendanceMarking'],
      'promptDayPlanUpdate': mp['promptDayPlanUpdate'],
      'promptMasterDataUpdate': mp['promptMasterDataUpdate'],
      'attendanceMarkingRequiredStatus': mp['attendanceMarkingRequiredStatus'],
      'dayPlanDownloadRequiredStatus': mp['dayPlanDownloadRequiredStatus'],
      'masterDataUpdateRequiredStatus': mp['masterDataUpdateRequiredStatus'],
      'buildDueDetail': mp['buildDueDetail'],
      'showAccountProfileDetails': mp['showAccountProfileDetails'],
      'showAllActivityCount': mp['showAllActivityCount'],
      'createTerritory': mp['createTerritory'],
      'draftScreenDeleteStatus': mp['draftScreenDeleteStatus'],
      'realTimeProductPriceEnabled': mp['realTimeProductPriceEnabled'],
      'hasPostDatedVoucher': mp['hasPostDatedVoucher'],
      'promptVehicleMaster': mp['promptVehicleMaster'],
      'voucherNumberGenerationType': mp['voucherNumberGenerationType'],
      'addNewCustomer': mp['addNewCustomer'],
      'preventNegativeStock': mp['preventNegativeStock'],
      'inventoryVoucherUIType': mp['inventoryVoucherUIType'],
      'kfcEnabled': mp['kfcEnabled'],
      'cartType': mp['cartType'],
      'gpsMandatory': mp['gpsMandatory'],
      'enableSecondarySales': mp['enableSecondarySales'],
      'enableAttendanceImage': mp['enableAttendanceImage'],
      'smartSearch': mp['smartSearch'],
      'salesOrderDownloadPdf': mp['salesOrderDownloadPdf'],
      'findLocation': mp['findLocation'],
      'hasGeoTag': mp['hasGeoTag'],
      'enableDynamicUnit': mp['enableDynamicUnit'],
      'enableDiscountRoundOffColumn': mp['enableDiscountRoundOffColumn'],
      'salesOrderAllocation': mp['salesOrderAllocation'],
      'rateWithoutCalculation': mp['rateWithoutCalculation'],
      'showBestPerformerUpload': mp['showBestPerformerUpload'],
      'belowPriceLevel': mp['belowPriceLevel']
    };
  }

  MobileSettingsConfiguration fromMap(mp) {
    MobileSettingsConfiguration mobaetconfig = MobileSettingsConfiguration();
    mobaetconfig.id = mp['id'];
    mobaetconfig.taskExecutionOffLineSaveStatus =
        mp['taskExecutionOffLineSaveStatus'] == 1 ? true : false;
    mobaetconfig.taskExecutionOffLineSaveDuration =
        mp['taskExecutionOffLineSaveDuration'];
    mobaetconfig.promptAttendanceMarking =
        mp['promptAttendanceMarking'] == 1 ? true : false;
    mobaetconfig.promptDayPlanUpdate =
        mp['promptDayPlanUpdate'] == 1 ? true : false;
    mobaetconfig.promptMasterDataUpdate =
        mp['promptMasterDataUpdate'] == 1 ? true : false;
    mobaetconfig.attendanceMarkingRequiredStatus =
        mp['attendanceMarkingRequiredStatus'] == 1 ? true : false;
    mobaetconfig.dayPlanDownloadRequiredStatus =
        mp['dayPlanDownloadRequiredStatus'] == 1 ? true : false;
    mobaetconfig.masterDataUpdateRequiredStatus =
        mp['masterDataUpdateRequiredStatus'] == 1 ? true : false;
    mobaetconfig.buildDueDetail = mp['buildDueDetail'] == 1 ? true : false;
    mobaetconfig.showAccountProfileDetails =
        mp['showAccountProfileDetails'] == 1 ? true : false;
    mobaetconfig.showAllActivityCount =
        mp['showAllActivityCount'] == 1 ? true : false;
    mobaetconfig.createTerritory = mp['createTerritory'] == 1 ? true : false;
    mobaetconfig.draftScreenDeleteStatus =
        mp['draftScreenDeleteStatus'] == 1 ? true : false;
    mobaetconfig.realTimeProductPriceEnabled =
        mp['realTimeProductPriceEnabled'] == 1 ? true : false;
    mobaetconfig.hasPostDatedVoucher =
        mp['hasPostDatedVoucher'] == 1 ? true : false;
    mobaetconfig.promptVehicleMaster =
        mp['promptVehicleMaster'] == 1 ? true : false;
    mobaetconfig.voucherNumberGenerationType =
        mp['voucherNumberGenerationType'];
    mobaetconfig.addNewCustomer = mp['addNewCustomer'] == 1 ? true : false;
    mobaetconfig.preventNegativeStock =
        mp['preventNegativeStock'] == 1 ? true : false;
    mobaetconfig.inventoryVoucherUIType = mp['inventoryVoucherUIType'];
    mobaetconfig.kfcEnabled = mp['kfcEnabled'] == 1 ? true : false;
    mobaetconfig.cartType = mp['cartType'];
    mobaetconfig.gpsMandatory = mp['gpsMandatory'] == 1 ? true : false;
    mobaetconfig.enableSecondarySales =
        mp['enableSecondarySales'] == 1 ? true : false;
    mobaetconfig.enableAttendanceImage =
        mp['enableAttendanceImage'] == 1 ? true : false;
    mobaetconfig.smartSearch = mp['smartSearch'] == 1 ? true : false;
    mobaetconfig.salesOrderDownloadPdf =
        mp['salesOrderDownloadPdf'] == 1 ? true : false;
    mobaetconfig.findLocation = mp['findLocation'] == 1 ? true : false;
    mobaetconfig.hasGeoTag = mp['hasGeoTag'] == 1 ? true : false;
    mobaetconfig.enableDynamicUnit =
        mp['enableDynamicUnit'] == 1 ? true : false;
    mobaetconfig.enableDiscountRoundOffColumn =
        mp['enableDiscountRoundOffColumn'] == 1 ? true : false;
    mobaetconfig.salesOrderAllocation =
        mp['salesOrderAllocation'] == 1 ? true : false;
    mobaetconfig.rateWithoutCalculation =
        mp['rateWithoutCalculation'] == 1 ? true : false;
    mobaetconfig.showBestPerformerUpload =
        mp['showBestPerformerUpload'] == 1 ? true : false;
    mobaetconfig.belowPriceLevel = mp['belowPriceLevel'] == 1 ? true : false;
    return mobaetconfig;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var mp = json.decode(response.body) as Map<String, dynamic>;
      OurDatabase(table: this.table, query: this.query).insertTable(toMap(mp));
      if (mp['guidedSellingConfig'] != null) {
        //print(mp['guidedSellingConfig']);
        if (await OurDatabase(table: GuidedSellingConfiguration().table)
            .databaseExists()) {
          OurDatabase(table: GuidedSellingConfiguration().table).deleteAll();
        }
        var guidedSellingConfig = mp['guidedSellingConfig'];
        GuidedSellingConfiguration()
            .connectDatabase(guidedSellingConfig as List);
        if (guidedSellingConfig['guidedSellingFilterItems'] != null) {
          if (guidedSellingConfig['guidedSellingFilterItems']) {
            Preference().saveInventoryFilteringStatus(
                guidedSellingConfig['guidedSellingFilterItems']);

            Preference().saveDefaultProductLoadingFrom('Product Group');
          } else {
            Preference().saveDefaultProductLoadingFrom('Product Group');
          }
        } else {
          Preference().saveDefaultProductLoadingFrom('Product Group');
        }
      } else {
        Preference().saveDefaultProductLoadingFrom('Product Group');
      }
    }
  }
}

//table ACCOUNT_TYPE
class AccountType {
  final String table = 'AccountType';
  final String query =
      'CREATE TABLE AccountType(id INT, pid STRING, name STRING, alias STRING, description STRING, receiverSupplierType STRING)';

  var id;
  String pid;
  String name;
  String alias;
  String description;
  String receiverSupplierType;

  AccountType(
      {this.id,
      this.pid,
      this.name,
      this.alias,
      this.description,
      this.receiverSupplierType});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'name': mp['name'],
      'alias': mp['alias'],
      'description': mp['description'],
      'receiverSupplierType': mp['receiverSupplierType']
    };
  }

  AccountType fromMap(mp) {
    AccountType acctype = AccountType();
    acctype.id = mp['id'];
    acctype.pid = mp['pid'];
    acctype.name = mp['name'];
    acctype.alias = mp['alias'];
    acctype.description = mp['description'];
    acctype.receiverSupplierType = mp['receiverSupplierType'];
    return acctype;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(mp));
      });
    }
  }
}

//table LOCATION_HIERARCHEY
class LocationHierarchey {
  final String table = 'LocationHierarchey';
  final String query =
      'CREATE TABLE LocationHierarchey(id INT, locationPid STRING, locationName STRING, parentPid STRING, parentName STRING )';

  var id;
  String locationPid;
  String locationName;
  String parentPid;
  String parentName;

  LocationHierarchey(
      {this.id,
      this.locationPid,
      this.locationName,
      this.parentPid,
      this.parentName});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'locationPid': mp['locationPid'],
      'locationName': mp['locationName'],
      'parentPid': mp['parentPid'],
      'parentName': mp['parentName']
    };
  }

  LocationHierarchey fromMap(mp) {
    LocationHierarchey locheirarchey = LocationHierarchey();
    locheirarchey.id = mp['id'];
    locheirarchey.locationPid = mp['locationPid'];
    locheirarchey.locationName = mp['locationName'];
    locheirarchey.parentPid = mp['parentPid'];
    locheirarchey.parentName = mp['parentName'];
    return locheirarchey;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(mp));
      });
    }
  }
}

//table TERRTORY_ACCOUNT_ASSOCATION
class TerrtoryAccountAssocation {
  final String table = 'TerrtoryAccountAssocation';
  final String query =
      'CREATE TABLE TerrtoryAccountAssocation (id INT, accountProfilePid STRING, accountProfileName STRING, locationPid STRING, locationName STRING )';

  var id;
  String accountProfilePid;
  String accountProfileName;
  String locationPid;
  String locationName;

  TerrtoryAccountAssocation(
      {this.id,
      this.accountProfilePid,
      this.accountProfileName,
      this.locationPid,
      this.locationName});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'accountProfilePid': mp['accountProfilePid'],
      'accountProfileName': mp['accountProfileName'],
      'locationPid': mp['locationPid'],
      'locationName': mp['locationName']
    };
  }

  TerrtoryAccountAssocation fromMap(mp) {
    TerrtoryAccountAssocation terrtoryAccAss = TerrtoryAccountAssocation();
    terrtoryAccAss.id = mp['id'];
    terrtoryAccAss.accountProfilePid = mp['accountProfilePid'];
    terrtoryAccAss.accountProfileName = mp['accountProfileName'];
    terrtoryAccAss.locationPid = mp['locationPid'];
    terrtoryAccAss.locationName = mp['locationName'];
    return terrtoryAccAss;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(mp));
      });
    }
  }
}

//table ACTIVITY_STAGE
class ActivityStage {
  final String table = 'ActivityStage';
  final String query =
      'CREATE TABLE ActivityStage (id INT, activityPid STRING, activityName STRING, stagePid STRING, stageName STRING )';

  var id;
  String activityPid;
  String activityName;
  String stagePid;
  String stageName;

  ActivityStage(
      {this.id,
      this.activityPid,
      this.activityName,
      this.stagePid,
      this.stageName});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'activityPid': mp['activityPid'],
      'activityName': mp['activityName'],
      'stagePid': mp['stagePid'],
      'stageName': mp['stageName']
    };
  }

  ActivityStage fromMap(mp) {
    ActivityStage actstage = ActivityStage();
    actstage.id = mp['id'];
    actstage.activityPid = mp['activityPid'];
    actstage.activityName = mp['activityName'];
    actstage.stagePid = mp['stagePid'];
    actstage.stageName = mp['stageName'];
    return actstage;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(mp));
      });
    }
  }
}

//tables for DOCUMENT
class Document {
  final String table = 'Document';
  final String query =
      'CREATE TABLE Document(id INT, pid STRING, name STRING, documentPrefix STRING,  alias STRING, description STRING, documentType STRING,' +
          'activityAccount STRING, isEdit BOOLEAN, numberOfItems DOUBLE, totalAmount DOUBLE,  save BOOLEAN, sourceStockLocationPid STRING,' +
          ' sourceStockLocationName STRING, destinationStockLocationPid STRING, destinationStockLocationName STRING, batchEnabled BOOLEAN,' +
          'voucherNumberGenerationType STRING, mode STRING, addNewCustomer BOOLEAN, termsAndConditions STRING, termsAndConditionsColumn BOOLEAN)';

  var id;
  String pid;
  String name;
  String documentPrefix;
  String alias;
  String description;
  String documentType;
  String activityAccount;
  bool isEdit;
  double numberOfItems;
  double totalAmount;
  bool save;
  String mode;
  String sourceStockLocationPid;
  String sourceStockLocationName;
  String destinationStockLocationPid;
  String destinationStockLocationName;
  bool batchEnabled;
  String voucherNumberGenerationType;
  bool addNewCustomer;
  String termsAndConditions;
  bool termsAndConditionsColumn;

  Document(
      {this.id,
      this.pid,
      this.name,
      this.documentPrefix,
      this.alias,
      this.description,
      this.documentType,
      this.activityAccount,
      this.isEdit,
      this.numberOfItems,
      this.totalAmount,
      this.save,
      this.sourceStockLocationPid,
      this.sourceStockLocationName,
      this.destinationStockLocationPid,
      this.destinationStockLocationName,
      this.batchEnabled,
      this.mode,
      this.voucherNumberGenerationType,
      this.addNewCustomer,
      this.termsAndConditions,
      this.termsAndConditionsColumn});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'name': mp['name'],
      'documentPrefix': mp['documentPrefix'],
      'alias': mp['alias'],
      'description': mp['description'],
      'documentType': mp['documentType'],
      'activityAccount': mp['activityAccount'],
      'isEdit': mp['isEdit'],
      'numberOfItems': mp['numberOfItems'],
      'totalAmount': mp['totalAmount'],
      'save': mp['save'],
      'mode': mp['mode'],
      'sourceStockLocationPid': mp['sourceStockLocationPid'],
      'sourceStockLocationName': mp['sourceStockLocationName'],
      'destinationStockLocationPid': mp['destinationStockLocationPid'],
      'destinationStockLocationName': mp['destinationStockLocationName'],
      'batchEnabled': mp['batchEnabled'],
      'voucherNumberGenerationType': mp['voucherNumberGenerationType'],
      'addNewCustomer': mp['addNewCustomer'],
      'termsAndConditions': mp['termsAndConditions'],
      'termsAndConditionsColumn': mp['termsAndConditionsColumn'],
    };
  }

  Document fromMap(mp) {
    Document doc = Document();
    doc.id = mp['id'];
    doc.pid = mp['pid'];
    doc.name = mp['name'];
    doc.documentPrefix = mp['documentPrefix'];
    doc.alias = mp['alias'];
    doc.description = mp['description'];
    doc.documentType = mp['documentType'];
    doc.activityAccount = mp['activityAccount'];
    doc.isEdit = mp['isEdit'] == 1 ? true : false;
    doc.numberOfItems = mp['numberOfItems'];
    doc.totalAmount = mp['totalAmount'];
    doc.save = mp['save'] == 1 ? true : false;
    doc.mode = mp['mode'];
    doc.sourceStockLocationPid = mp['sourceStockLocationPid'];
    doc.sourceStockLocationName = mp['sourceStockLocationName'];
    doc.destinationStockLocationPid = mp['destinationStockLocationPid'];
    doc.destinationStockLocationName = mp['destinationStockLocationName'];
    doc.batchEnabled = mp['batchEnabled'] == 1 ? true : false;
    doc.voucherNumberGenerationType = mp['voucherNumberGenerationType'];
    doc.addNewCustomer = mp['addNewCustomer'] == 1 ? true : false;
    doc.termsAndConditions = mp['termsAndConditions'];
    doc.termsAndConditionsColumn =
        mp['termsAndConditionsColumn'] == 1 ? true : false;
    return doc;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(mp));
      });
    }
  }
}

//tables for task execution
class TaskExecution {
  final String table = 'TaskExecution';
  final String query =
      'CREATE TABLE TaskExecution( id INT, accountProfileId INT, accountProfilePid STRING, accountProfileName STRING, accountProfileAddress STRING, accountProfilePhNo STRING, executiveActivityId INT,executiveActivityName STRING, executiveActivityPid STRING, activityGroupId INT, activityGroupPid STRING, activityGroupName STRING, accountTypePid STRING, accountTypeName STRING, outstandingAmount DOUBLE, sendDate STRING, sendTime STRING, successSendDate STRING, successSendTime STRING, sendVisitDate STRING, tabUniqueId STRING, serverUniqueId STRING, pidFromServer STRING, latitude DOUBLE, longitude DOUBLE, mcc STRING, mnc STRING, cellId STRING, lac STRING, startTimeLatitude DOUBLE, startTimeLongitude DOUBLE, startTimeMcc STRING, startTimeMnc STRING, startTimeCellId STRING, startTimeLac STRING, isStartTimeLocationStatus BOOLEAN, isStartTimeTowerLocationStatus BOOLEAN, isStartTimeFlightModeStatus BOOLEAN, iStartTimeGpsOff BOOLEAN, isStartTimeMobileDataOff BOOLEAN, startTimeLocationFindStatus INT, startTimeDate STRING, startTimeTime STRING, endTimeDate STRING, endTimeTime STRING, sentStatus INT, isSelected BOOLEAN, isLocationStatus BOOLEAN, isTowerLocationStatus BOOLEAN, isFlightModeStatus BOOLEAN, remarks STRING, userTaskId STRING, accountProfileToServerReason STRING, isDocumentDetails STRING, isDynamicUpdateSend STRING, isInventoryUpdateSend BOOLEAN, isAccountingUpdateSend BOOLEAN, updatedDocumentPid STRING, isGpsOff BOOLEAN, isMobileDataOff BOOLEAN, executiveTaskPlanPid STRING, clientPrimaryKeyId STRING, interimSave BOOLEAN, simState  STRING, mockLocationStatus BOOLEAN, withCustomer BOOLEAN)';

  var id;
  var accountProfileId;
  String accountProfilePid;
  String accountProfileName;
  String accountProfileAddress;
  String accountProfilePhNo;
  var executiveActivityId;
  String executiveActivityPid;
  String executiveActivityName;
  var activityGroupId;
  String activityGroupPid;
  String activityGroupName;
  String accountTypePid;
  String accountTypeName;
  double outstandingAmount;
  String sendDate;
  String sendTime;
  String successSendDate;
  String successSendTime;
  String sendVisitDate;
  String tabUniqueId;
  String serverUniqueId;
  String pidFromServer;
  double latitude;
  double longitude;
  String mcc;
  String mnc;
  String cellId;
  String lac;
  double startTimeLatitude;
  double startTimeLongitude;
  String startTimeMcc;
  String startTimeMnc;
  String startTimeCellId;
  String startTimeLac;
  bool isStartTimeLocationStatus;
  bool isStartTimeTowerLocationStatus;
  bool isStartTimeFlightModeStatus;
  bool iStartTimeGpsOff;
  bool isStartTimeMobileDataOff;
  int startTimeLocationFindStatus;
  String startTimeDate;
  String startTimeTime;
  String endTimeDate;
  String endTimeTime;
  int sentStatus;
  bool isSelected;
  bool isLocationStatus;
  bool isTowerLocationStatus;
  bool isFlightModeStatus;
  String remarks;
  var userTaskId;
  String accountProfileToServerReason;
  bool isDocumentDetails;
  bool isDynamicUpdateSend;
  bool isInventoryUpdateSend;
  bool isAccountingUpdateSend;
  String updatedDocumentPid;
  bool isGpsOff;
  bool isMobileDataOff;
  String executiveTaskPlanPid;
  String clientPrimaryKeyId;
  bool interimSave;
  String simState;
  bool mockLocationStatus;
  bool withCustomer;

  TaskExecution({
    this.id,
    this.accountProfileId,
    this.accountProfilePid,
    this.accountProfileName,
    this.accountProfileAddress,
    this.accountProfilePhNo,
    this.executiveActivityId,
    this.executiveActivityPid,
    this.executiveActivityName,
    this.activityGroupId,
    this.activityGroupPid,
    this.activityGroupName,
    this.accountTypePid,
    this.accountTypeName,
    this.outstandingAmount,
    this.sendDate,
    this.sendTime,
    this.successSendDate,
    this.successSendTime,
    this.sendVisitDate,
    this.tabUniqueId,
    this.serverUniqueId,
    this.pidFromServer,
    this.latitude,
    this.longitude,
    this.mcc,
    this.mnc,
    this.cellId,
    this.lac,
    this.startTimeLatitude,
    this.startTimeLongitude,
    this.startTimeMcc,
    this.startTimeMnc,
    this.startTimeCellId,
    this.startTimeLac,
    this.isStartTimeLocationStatus,
    this.isStartTimeTowerLocationStatus,
    this.isStartTimeFlightModeStatus,
    this.iStartTimeGpsOff,
    this.isStartTimeMobileDataOff,
    this.startTimeLocationFindStatus,
    this.startTimeDate,
    this.startTimeTime,
    this.endTimeDate,
    this.endTimeTime,
    this.sentStatus,
    this.isSelected,
    this.isLocationStatus,
    this.isTowerLocationStatus,
    this.isFlightModeStatus,
    this.remarks,
    this.userTaskId,
    this.accountProfileToServerReason,
    this.isDocumentDetails,
    this.isDynamicUpdateSend,
    this.isInventoryUpdateSend,
    this.isAccountingUpdateSend,
    this.updatedDocumentPid,
    this.isGpsOff,
    this.isMobileDataOff,
    this.executiveTaskPlanPid,
    this.clientPrimaryKeyId,
    this.interimSave,
    this.simState,
    this.mockLocationStatus,
    this.withCustomer,
  });

  Map<String, dynamic> toMap(var mp) {
    return {
      'id': mp['id'],
      'accountProfileId': mp['accountProfileId'],
      'accountProfilePid': mp['accountProfilePid'],
      'accountProfileName': mp['accountProfileName'],
      'accountProfileAddress': mp['accountProfileAddress'],
      'accountProfilePhNo': mp['accountProfilePhNo'],
      'executiveActivityId': mp['executiveActivityId'],
      'executiveActivityPid': mp['executiveActivityPid'],
      'executiveActivityName': mp['executiveActivityName'],
      'activityGroupId': mp['activityGroupId'],
      'activityGroupPid': mp['activityGroupPid'],
      'activityGroupName': mp['activityGroupName'],
      'accountTypePid': mp['accountTypePid'],
      'accountTypeName': mp['accountTypeName'],
      'outstandingAmount': mp['outstandingAmount'],
      'sendDate': mp['sendDate'],
      'sendTime': mp['sendTime'],
      'successSendDate': mp['successSendDate'],
      'successSendTime': mp['successSendTime'],
      'sendVisitDate': mp['sendVisitDate'],
      'tabUniqueId': mp['tabUniqueId'],
      'serverUniqueId': mp['serverUniqueId'],
      'pidFromServer': mp['pidFromServer'],
      'latitude': mp['latitude'],
      'longitude': mp['longitude'],
      'mcc': mp['mcc'],
      'mnc': mp['mnc'],
      'cellId': mp['cellId'],
      'lac': mp['lac'],
      'startTimeLatitude': mp['startTimeLatitude'],
      'startTimeLongitude': mp['startTimeLongitude'],
      'startTimeMcc': mp['startTimeMcc'],
      'startTimeMnc': mp['startTimeMnc'],
      'startTimeCellId': mp['startTimeCellId'],
      'startTimeLac': mp['startTimeLac'],
      'isStartTimeLocationStatus': mp['isStartTimeLocationStatus'],
      'isStartTimeTowerLocationStatus': mp['isStartTimeTowerLocationStatus'],
      'isStartTimeFlightModeStatus': mp['isStartTimeFlightModeStatus'],
      'iStartTimeGpsOff': mp['iStartTimeGpsOff'],
      'isStartTimeMobileDataOff': mp['isStartTimeMobileDataOff'],
      'startTimeLocationFindStatus': mp['startTimeLocationFindStatus'],
      'startTimeDate': mp['startTimeDate'],
      'startTimeTime': mp['startTimeTime'],
      'endTimeDate': mp['endTimeDate'],
      'endTimeTime': mp['endTimeTime'],
      'sentStatus': mp['sentStatus'],
      'isSelected': mp['isSelected'],
      'isLocationStatus': mp['isLocationStatus'],
      'isTowerLocationStatus': mp['isTowerLocationStatus'],
      'isFlightModeStatus': mp['isFlightModeStatus'],
      'remarks': mp['remarks'],
      'userTaskId': mp['userTaskId'],
      'accountProfileToServerReason': mp['accountProfileToServerReason'],
      'isDocumentDetails': mp['isDocumentDetails'],
      'isDynamicUpdateSend': mp['isDynamicUpdateSend'],
      'isInventoryUpdateSend': mp['isInventoryUpdateSend'],
      'isAccountingUpdateSend': mp['isAccountingUpdateSend'],
      'updatedDocumentPid': mp['updatedDocumentPid'],
      'isGpsOff': mp['isGpsOff'],
      'isMobileDataOff': mp['isMobileDataOff'],
      'executiveTaskPlanPid': mp['executiveTaskPlanPid'],
      'clientPrimaryKeyId': mp['clientPrimaryKeyId'],
      'interimSave': mp['interimSave'],
      'simState': mp['simState'],
      'mockLocationStatus': mp['mockLocationStatus'],
      'withCustomer': mp['withCustomer'],
    };
  }

  // ignore: non_constant_identifier_names
  Map<String, dynamic> objectToMap(TaskExecution task) {
    return {
      'id': task.id,
      'accountProfileId': task.accountProfileId,
      'accountProfilePid': task.accountProfilePid,
      'accountProfileName': task.accountProfileName,
      'accountProfileAddress': task.accountProfileAddress,
      'accountProfilePhNo': task.accountProfilePhNo,
      'executiveActivityId': task.executiveActivityId,
      'executiveActivityPid': task.executiveActivityPid,
      'executiveActivityName': task.executiveActivityName,
      'activityGroupId': task.activityGroupId,
      'activityGroupPid': task.activityGroupPid,
      'activityGroupName': task.activityGroupName,
      'accountTypePid': task.accountTypePid,
      'accountTypeName': task.accountTypeName,
      'outstandingAmount': task.outstandingAmount,
      'sendDate': task.sendDate,
      'sendTime': task.sendTime,
      'successSendDate': task.successSendDate,
      'successSendTime': task.successSendTime,
      'sendVisitDate': task.sendVisitDate,
      'tabUniqueId': task.tabUniqueId,
      'serverUniqueId': task.serverUniqueId,
      'pidFromServer': task.pidFromServer,
      'latitude': task.latitude,
      'longitude': task.longitude,
      'mcc': task.mcc,
      'mnc': task.mnc,
      'cellId': task.cellId,
      'lac': task.lac,
      'startTimeLatitude': task.startTimeLatitude,
      'startTimeLongitude': task.startTimeLongitude,
      'startTimeMcc': task.startTimeMcc,
      'startTimeMnc': task.startTimeMnc,
      'startTimeCellId': task.startTimeCellId,
      'startTimeLac': task.startTimeLac,
      'isStartTimeLocationStatus': task.isStartTimeLocationStatus,
      'isStartTimeTowerLocationStatus': task.isStartTimeTowerLocationStatus,
      'isStartTimeFlightModeStatus': task.isStartTimeFlightModeStatus,
      'iStartTimeGpsOff': task.iStartTimeGpsOff,
      'isStartTimeMobileDataOff': task.isStartTimeMobileDataOff,
      'startTimeLocationFindStatus': task.startTimeLocationFindStatus,
      'startTimeDate': task.startTimeDate,
      'startTimeTime': task.startTimeTime,
      'endTimeDate': task.endTimeDate,
      'endTimeTime': task.endTimeTime,
      'sentStatus': task.sentStatus,
      'isSelected': task.isSelected,
      'isLocationStatus': task.isLocationStatus,
      'isTowerLocationStatus': task.isTowerLocationStatus,
      'isFlightModeStatus': task.isFlightModeStatus,
      'remarks': task.remarks,
      'userTaskId': task.userTaskId,
      'accountProfileToServerReason': task.accountProfileToServerReason,
      'isDocumentDetails': task.isDocumentDetails,
      'isDynamicUpdateSend': task.isDynamicUpdateSend,
      'isInventoryUpdateSend': task.isInventoryUpdateSend,
      'isAccountingUpdateSend': task.isAccountingUpdateSend,
      'updatedDocumentPid': task.updatedDocumentPid,
      'isGpsOff': task.isGpsOff,
      'isMobileDataOff': task.isMobileDataOff,
      'executiveTaskPlanPid': task.executiveTaskPlanPid,
      'clientPrimaryKeyId': task.clientPrimaryKeyId,
      'interimSave': task.interimSave,
      'simState': task.simState,
      'mockLocationStatus': task.mockLocationStatus,
      'withCustomer': task.withCustomer,
    };
  }

  connectDatabase(map) async {
    OurDatabase(table: this.table, query: this.query).insertTable(toMap(map));
  }
}

//table PRODUCT_PROFILE
class ProductProfile {
  final String table = 'ProductProfile';
  final String query =
      'CREATE TABLE ProductProfile(id INT, pid STRING, name STRING, alias STRING, description STRING, price DOUBLE, sku STRING, unitQty DOUBLE, size DOUBLE,' +
          'productCategoryPid STRING, productCategoryName STRING, taxRate DOUBLE, mrp DOUBLE, filesPid STRING, stockQty DOUBLE, productGroupId STRING,added BOOLEAN)';

  var id;
  String pid;
  String name;
  String alias;
  String description;
  double price;
  String sku;
  double unitQty;
  double size;
  String productCategoryPid;
  String productCategoryName;
  double taxRate;
  double mrp;
  String filesPid;
  double stockQty;
  var productGroupId;
  bool added;

  ProductProfile(
      {this.id,
      this.pid,
      this.name,
      this.alias,
      this.description,
      this.price,
      this.sku,
      this.unitQty,
      this.size,
      this.productCategoryPid,
      this.productCategoryName,
      this.taxRate,
      this.mrp,
      this.filesPid,
      this.stockQty,
      this.productGroupId,
      this.added});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'name': mp['name'],
      'alias': mp['alias'],
      'description': mp['description'],
      'price': mp['price'],
      'sku': mp['sku'],
      'unitQty': mp['unitQty'],
      'size': mp['size'],
      'productCategoryPid': mp['productCategoryPid'],
      'productCategoryName': mp['productCategoryName'],
      'taxRate': mp['taxRate'],
      'mrp': mp['mrp'],
      'filesPid': mp['filesPid'],
      'stockQty': mp['stockQty'],
      'productGroupId': mp['productGroupId'],
      'added': mp['added'],
    };
  }

  ProductProfile fromMap(mp) {
    ProductProfile productProf = ProductProfile();
    productProf.id = mp['id'];
    productProf.pid = mp['pid'];
    productProf.name = mp['name'];
    productProf.alias = mp['alias'];
    productProf.description = mp['description'];
    productProf.price = mp['price'];
    productProf.sku = mp['sku'];
    productProf.unitQty = mp['unitQty'];
    productProf.size = mp['size'];
    productProf.productCategoryPid = mp['productCategoryPid'];
    productProf.productCategoryName = mp['productCategoryName'];
    productProf.taxRate = mp['taxRate'];
    productProf.mrp = mp['mrp'];
    productProf.filesPid = mp['filesPid'];
    productProf.stockQty = mp['stockQty'];
    productProf.productGroupId = mp['productGroupId'];
    productProf.added = mp['added'] == 1 ? true : false;
    return productProf;
  }

  Future<Map<String, dynamic>> getProductProfileByPid(var pid) async {
    if (await OurDatabase(table: this.table).databaseExists()) {
      var mp = await OurDatabase(table: this.table)
          .getTable(where: 'pid', whereArgs: pid) as List;
      if (mp.length > 0) {
        return mp[0] as Map<String, dynamic>;
      } else
        return null;
    } else
      return null;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(mp));
      });
    }
  }
}

//tables for PRODUCT_CATEGORY
class ProductCategory {
  final String table = 'ProductCategory';
  final String query =
      'CREATE TABLE ProductCategory(id INT, pid STRING, name STRING, alias STRING, description STRING, isSelected BOOLEAN)';

  var id;
  String pid;
  String name;
  String alias;
  String description;
  bool isSelected;

  ProductCategory(
      {this.id,
      this.pid,
      this.name,
      this.alias,
      this.description,
      this.isSelected});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'name': mp['name'],
      'alias': mp['alias'],
      'description': mp['description'],
      'isSelected': mp['isSelected']
    };
  }

  ProductCategory fromMap(mp) {
    ProductCategory prodcat = ProductCategory();
    prodcat.id = mp['id'];
    prodcat.pid = mp['pid'];
    prodcat.name = mp['name'];
    prodcat.alias = mp['alias'];
    prodcat.description = mp['description'];
    prodcat.isSelected = mp['isSelected'] == 1 ? true : false;
    return prodcat;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        mp.forEach((key, value) {
          OurDatabase(table: this.table, query: this.query)
              .insertTable(toMap(mp));
        });
      });
    }
  }
}

//tables for DOCUMENT_PRODUCT_CATEGORY
class DocumentProductCategory {
  final String table = 'DocumentProductCategory';
  final String query =
      'CREATE TABLE DocumentProductCategory(id INT, documentPid STRING, documentName STRING, productCategoryPid STRING, productCategoryName STRING )';

  var id;
  String documentPid;
  String documentName;
  String productCategoryPid;
  String productCategoryName;

  DocumentProductCategory(
      {this.id,
      this.documentPid,
      this.documentName,
      this.productCategoryPid,
      this.productCategoryName});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'documentPid': mp['documentPid'],
      'documentName': mp['documentName'],
      'productCategoryPid': mp['productCategoryPid'],
      'productCategoryName': mp['productCategoryName']
    };
  }

  DocumentProductCategory fromMap(mp) {
    DocumentProductCategory docproductcat = DocumentProductCategory();
    docproductcat.id = mp['id'];
    docproductcat.documentPid = mp['documentPid'];
    docproductcat.documentName = mp['documentName'];
    docproductcat.productCategoryPid = mp['productCategoryPid'];
    docproductcat.productCategoryName = mp['productCategoryName'];
    return docproductcat;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        mp.forEach((key, value) {
          OurDatabase(table: this.table, query: this.query)
              .insertTable(toMap(mp));
        });
      });
    }
  }
}

//tables for PRODUCT_GROUP
class ProductGroup {
  final String table = 'ProductGroup';
  final String query =
      'CREATE TABLE ProductGroup(id INT, pid STRING, name STRING, alias STRING, description STRING, isSelected BOOLEAN, sortOrderNo INT)';

  var id;
  String pid;
  String name;
  String alias;
  String description;
  bool isSelected;
  var sortOrderNo;

  ProductGroup(
      {this.id,
      this.pid,
      this.name,
      this.alias,
      this.description,
      this.isSelected,
      this.sortOrderNo});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'name': mp['name'],
      'alias': mp['alias'],
      'description': mp['description'],
      'isSelected': mp['isSelected'],
      'sortOrderNo': mp['sortOrderNo']
    };
  }

  ProductGroup fromMap(mp) {
    ProductGroup prodgroup = ProductGroup();
    prodgroup.id = mp['id'];
    prodgroup.pid = mp['pid'];
    prodgroup.name = mp['name'];
    prodgroup.alias = mp['alias'];
    prodgroup.description = mp['description'];
    prodgroup.isSelected = mp['isSelected'] == 1 ? true : false;
    prodgroup.sortOrderNo = mp['sortOrderNo'];
    return prodgroup;
  }

  Future<Map<String, dynamic>> getProductGroupsByPid(var pid) async {
    if (await OurDatabase(table: this.table).databaseExists()) {
      var mp = await OurDatabase(table: this.table)
          .getTable(where: 'pid', whereArgs: pid) as List;
      if (mp.length > 0) {
        return mp[0] as Map<String, dynamic>;
      } else
        return null;
    } else
      return null;
  }

//TODO
  connectDatabase(Response response) async {
    String apiType = await Preference().getApiType();
    if (apiType == "fetch") {
      if (await OurDatabase(table: this.table).databaseExists()) {
        OurDatabase(table: this.table).deleteAll();
      }
      if (await OurDatabase(table: ProductGroup().table).databaseExists()) {
        OurDatabase(table: ProductGroup().table).deleteAll();
      }
      if (await OurDatabase(table: ProductToProductGroup().table)
          .databaseExists()) {
        OurDatabase(table: ProductToProductGroup().table).deleteAll();
      }

      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        mp.forEach((key, value) async {
          var productGroup = mp['productGroupDTO'];
          var productGroupInDb =
              await getProductGroupsByPid(productGroup['pid']);
          if (productGroupInDb == null) {
            OurDatabase(table: this.table, query: this.query)
                .insertTable(toMap(productGroup as Map<String, dynamic>));

            var li = mp['productProfiles'] as List;
            li.forEach((element) async {
              var product = element as Map<String, dynamic>;
              //print('pro $product\n');
              var productProfileInDb =
                  await ProductProfile().getProductProfileByPid(product['pid']);
              //print(productProfileInDb);
              if (productProfileInDb == null) {
                product['productGroupPid'] = productGroup['pid'];
                //print('updated $product');
                OurDatabase(
                        table: ProductProfile().table,
                        query: ProductProfile().query)
                    .insertTable(product);

                var map = {
                  'id': null,
                  'pid': product['pid'],
                  'name': product['name'],
                  'alias': product['alias'],
                  'description': product['description'],
                  'price': product['price'],
                  'sku': product['sku'],
                  'unitQty': product['unitQty'],
                  'productCategoryPid': product['productCategoryPid'],
                  'productCategoryName': product['productCategoryName'],
                  'divisionPid': product['divisionPid'],
                  'divisionName': product['divisionName'],
                  'productGroupPid': productGroup['pid']
                };
                OurDatabase(
                        table: ProductToProductGroup().table,
                        query: ProductToProductGroup().query)
                    .insertTable(map);
              }
            });
          }
        });
      });
    }
  }
}

//tables for DOCUMENT_PRODUCT_GROUP
class DocumentProductGroup {
  final String table = 'DocumentProductGroup';
  final String query =
      'CREATE TABLE DocumentProductGroup(id INT, documentPid STRING, documentName STRING, productGroupPid STRING, productGroupName STRING, sortOrder STRING)';

  var id;
  String documentPid;
  String documentName;
  String productGroupPid;
  String productGroupName;
  var sortOrder;

  DocumentProductGroup(
      {this.id,
      this.documentPid,
      this.documentName,
      this.productGroupPid,
      this.productGroupName,
      this.sortOrder});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'documentPid': mp['documentPid'],
      'documentName': mp['documentName'],
      'productGroupPid': mp['productGroupPid'],
      'productGroupName': mp['productGroupName'],
      'sortOrder': mp['sortOrder']
    };
  }

  DocumentProductGroup fromMap(mp) {
    DocumentProductGroup docprodgroup = DocumentProductGroup();
    docprodgroup.id = mp['id'];
    docprodgroup.documentPid = mp['documentPid'];
    docprodgroup.documentName = mp['documentName'];
    docprodgroup.productGroupPid = mp['productGroupPid'];
    docprodgroup.productGroupName = mp['productGroupName'];
    docprodgroup.sortOrder = mp['sortOrder'];
    return docprodgroup;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        mp.forEach((key, value) {
          OurDatabase(table: this.table, query: this.query)
              .insertTable(toMap(mp));
        });
      });
    }
  }
}

//tables for VoucherColoum
class VoucherColoum {
  final table = 'VoucherColoum';
  final query =
      'CREATE TABLE VoucherColoum(id STRING,documentPid STRING,documentName STRING,inventoryVoucherColumn STRING,label STRING,enabled BOOLEAN)';

  var id;
  String documentPid;
  String documentName;
  String inventoryVoucherColumn;
  String label;
  bool enabled;

  VoucherColoum(
      {this.id,
      this.documentPid,
      this.documentName,
      this.inventoryVoucherColumn,
      this.label,
      this.enabled});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'documentPid': mp['documentPid'],
      'documentName': mp['documentName'],
      'inventoryVoucherColumn': mp['inventoryVoucherColumn'],
      'label': mp['label'],
      'enabled': mp['enabled']
    };
  }

  VoucherColoum fromMap(mp) {
    VoucherColoum voucher = VoucherColoum();
    voucher.id = mp['id'];
    voucher.documentPid = mp['documentPid'];
    voucher.documentName = mp['documentName'];
    voucher.inventoryVoucherColumn = mp['inventoryVoucherColumn'];
    voucher.label = mp['label'];
    voucher.enabled = mp['enabled'];
    return voucher;
  }

  connectDatabase(Response response) async {
    var lists = json.decode(response.body) as List;
    lists.forEach((element) {
      var mp = element as Map<String, dynamic>;
      mp.forEach((key, value) {
        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(mp));
      });
    });
  }
}

//table INVENTORY_VOUCHER_DETAILS
class InventoryVoucherDetails {
  final String table = 'InventoryVoucherDetails';
  final String query =
      'CREATE TABLE InventoryVoucherDetails( id INT, detailId INT, inventoryVoucherHeaderId INT, itemName STRING, itemPid STRING, stock DOUBLE, quantity DOUBLE, refQuantity DOUBLE, batchQuantity DOUBLE, freeQty DOUBLE, sellingRate DOUBLE, mrp DOUBLE, purchaseRate DOUBLE, offer STRING, rateOfVat DOUBLE, discountInPercentage DOUBLE, discountInRupees DOUBLE, unitQty DOUBLE, sku STRING, productCategoryPid STRING, productCategoryName STRING, productGroupPid STRING, productGroupName STRING, priceLevel STRING, totalAmount DOUBLE, refTotalAmount DOUBLE, batchTotalAmount DOUBLE, batchNumber DOUBLE, batchDate DOUBLE, length STRING, width STRING, thickness STRING, size DOUBLE, color STRING, remarks STRING, isShowDetails BOOLEAN, sourceStockLocationPid STRING, sourceStockLocationName STRING, destinationStockLocationPid STRING, destinationStockLocationName STRING, pidFromServer STRING, isInventorySearch BOOLEAN, referenceInventoryVoucherHeaderPid STRING, referenceInventoryVoucherDetailId INT, isBatchAllocated BOOLEAN, inventoryVoucherHeaderDate STRING, description STRING, itemtype STRING, pieces DOUBLE, ratePerPiece DOUBLE, stockLocationPid STRING, stockLocationName STRING, bookingNo STRING, bookingDate  STRING, deliveryDate STRING )';

  var id;
  var detailId;
  var inventoryVoucherHeaderId;
  String itemName;
  String itemPid;
  double stock;
  double quantity;
  double refQuantity;
  double batchQuantity;
  double freeQty;
  double sellingRate;
  double mrp;
  double purchaseRate;
  String offer;
  double rateOfVat;
  double discountInPercentage;
  double discountInRupees;
  double unitQty;
  String sku;
  String productCategoryPid;
  String productCategoryName;
  String productGroupPid;
  String productGroupName;
  String priceLevel;
  double totalAmount;
  double refTotalAmount;
  double batchTotalAmount;
  double batchNumber;
  String batchDate;
  String length;
  String width;
  String thickness;
  double size;
  bool color;
  String remarks;
  bool isShowDetails;
  String sourceStockLocationPid;
  String sourceStockLocationName;
  String destinationStockLocationPid;
  String destinationStockLocationName;
  String pidFromServer;
  bool isInventorySearch;
  String referenceInventoryVoucherHeaderPid;
  String referenceInventoryVoucherDetailId;
  bool isBatchAllocated;
  String inventoryVoucherHeaderDate;
  String description;
  String itemtype;
  double pieces;
  double ratePerPiece;
  String stockLocationPid;
  String stockLocationName;
  String bookingNo;
  String bookingDate;
  String deliveryDate;

  InventoryVoucherDetails({
    this.id,
    this.detailId,
    this.inventoryVoucherHeaderId,
    this.itemName,
    this.itemPid,
    this.stock,
    this.quantity,
    this.refQuantity,
    this.batchQuantity,
    this.freeQty,
    this.sellingRate,
    this.mrp,
    this.purchaseRate,
    this.offer,
    this.rateOfVat,
    this.discountInPercentage,
    this.discountInRupees,
    this.unitQty,
    this.sku,
    this.productCategoryPid,
    this.productCategoryName,
    this.productGroupPid,
    this.productGroupName,
    this.priceLevel,
    this.totalAmount,
    this.refTotalAmount,
    this.batchTotalAmount,
    this.batchNumber,
    this.batchDate,
    this.length,
    this.width,
    this.thickness,
    this.size,
    this.color,
    this.remarks,
    this.isShowDetails,
    this.sourceStockLocationPid,
    this.sourceStockLocationName,
    this.destinationStockLocationPid,
    this.destinationStockLocationName,
    this.pidFromServer,
    this.isInventorySearch,
    this.referenceInventoryVoucherHeaderPid,
    this.referenceInventoryVoucherDetailId,
    this.isBatchAllocated,
    this.inventoryVoucherHeaderDate,
    this.description,
    this.itemtype,
    this.pieces,
    this.ratePerPiece,
    this.stockLocationPid,
    this.stockLocationName,
    this.bookingNo,
    this.bookingDate,
    this.deliveryDate,
  });

  Map<String, dynamic> toMap(mp) {
    return {
      'id': null,
      'detailId': mp.detailId,
      'inventoryVoucherHeaderId': mp.inventoryVoucherHeaderId,
      'itemName': mp.itemName,
      'itemPid': mp.itemPid,
      'stock': mp.stock,
      'quantity': mp.quantity,
      'refQuantity': mp.refQuantity,
      'batchQuantity': mp.batchQuantity,
      'freeQty': mp.freeQty,
      'sellingRate': mp.sellingRate,
      'mrp': mp.mrp,
      'purchaseRate': mp.purchaseRate,
      'offer': mp.offer,
      'rateOfVat': mp.rateOfVat,
      'discountInPercentage': mp.discountInPercentage,
      'discountInRupees': mp.discountInRupees,
      'unitQty': mp.unitQty,
      'sku': mp.sku,
      'productCategoryPid': mp.productCategoryPid,
      'productCategoryName': mp.productCategoryName,
      'productGroupPid': mp.productGroupPid,
      'productGroupName': mp.productGroupName,
      'priceLevel': mp.priceLevel,
      'totalAmount': mp.totalAmount,
      'refTotalAmount': mp.refTotalAmount,
      'batchTotalAmount': mp.batchTotalAmount,
      'batchNumber': mp.batchNumber,
      'batchDate': mp.batchDate,
      'length': mp.length,
      'width': mp.width,
      'thickness': mp.thickness,
      'size': mp.size,
      'color': mp.color,
      'remarks': mp.remarks,
      'isShowDetails': mp.isShowDetails,
      'sourceStockLocationPid': mp.sourceStockLocationPid,
      'sourceStockLocationName': mp.sourceStockLocationName,
      'destinationStockLocationPid': mp.destinationStockLocationPid,
      'destinationStockLocationName': mp.destinationStockLocationName,
      'pidFromServer': mp.pidFromServer,
      'isInventorySearch': mp.isInventorySearch,
      'referenceInventoryVoucherHeaderPid':
          mp.referenceInventoryVoucherHeaderPid,
      'referenceInventoryVoucherDetailId': mp.referenceInventoryVoucherDetailId,
      'isBatchAllocated': mp.isBatchAllocated,
      'inventoryVoucherHeaderDate': mp.inventoryVoucherHeaderDate,
      'description': mp.description,
      'itemtype': mp.itemtype,
      'pieces': mp.pieces,
      'ratePerPiece': mp.ratePerPiece,
      'stockLocationPid': mp.stockLocationPid,
      'stockLocationName': mp.stockLocationName,
      'bookingNo': mp.bookingNo,
      'bookingDate': mp.bookingDate,
      'deliveryDate': mp.deliveryDate,
    };
  }

  InventoryVoucherDetails fromMap(mp) {
    InventoryVoucherDetails invenvoucher = InventoryVoucherDetails();
    invenvoucher.id = mp['id'];
    invenvoucher.detailId = mp['detailId'];
    invenvoucher.inventoryVoucherHeaderId = mp['inventoryVoucherHeaderId'];
    invenvoucher.itemName = mp['itemName'];
    invenvoucher.itemPid = mp['itemPid'];
    invenvoucher.stock = mp['stock'];
    invenvoucher.quantity = mp['quantity'];
    invenvoucher.refQuantity = mp['refQuantity'];
    invenvoucher.batchQuantity = mp['batchQuantity'];
    invenvoucher.freeQty = mp['freeQty'];
    invenvoucher.sellingRate = mp['sellingRate'];
    invenvoucher.mrp = mp['mrp'];
    invenvoucher.purchaseRate = mp['purchaseRate'];
    invenvoucher.offer = mp['offer'];
    invenvoucher.rateOfVat = mp['rateOfVat'];
    invenvoucher.discountInPercentage = mp['discountInPercentage'];
    invenvoucher.discountInRupees = mp['discountInRupees'];
    invenvoucher.unitQty = mp['unitQty'];
    invenvoucher.sku = mp['sku'];
    invenvoucher.productCategoryPid = mp['productCategoryPid'];
    invenvoucher.productCategoryName = mp['productCategoryName'];
    invenvoucher.productGroupPid = mp['productGroupPid'];
    invenvoucher.productGroupName = mp['productGroupName'];
    invenvoucher.priceLevel = mp['priceLevel'];
    invenvoucher.totalAmount = mp['totalAmount'];
    invenvoucher.refTotalAmount = mp['refTotalAmount'];
    invenvoucher.batchTotalAmount = mp['batchTotalAmount'];
    invenvoucher.batchNumber = mp['batchNumber'];
    invenvoucher.batchDate = mp['batchDate'];
    invenvoucher.length = mp['length'];
    invenvoucher.width = mp['width'];
    invenvoucher.thickness = mp['thickness'];
    invenvoucher.size = mp['size'];
    invenvoucher.color = mp['color'] == 1 ? true : false;
    invenvoucher.remarks = mp['remarks'];
    invenvoucher.isShowDetails = mp['isShowDetails'] == 1 ? true : false;
    invenvoucher.sourceStockLocationPid = mp['sourceStockLocationPid'];
    invenvoucher.sourceStockLocationName = mp['sourceStockLocationName'];
    invenvoucher.destinationStockLocationPid =
        mp['destinationStockLocationPid'];
    invenvoucher.destinationStockLocationName =
        mp['destinationStockLocationName'];
    invenvoucher.pidFromServer = mp['pidFromServer'];
    invenvoucher.isInventorySearch =
        mp['isInventorySearch'] == 1 ? true : false;
    invenvoucher.referenceInventoryVoucherHeaderPid =
        mp['referenceInventoryVoucherHeaderPid'];
    invenvoucher.referenceInventoryVoucherDetailId =
        mp['referenceInventoryVoucherDetailId'];
    invenvoucher.isBatchAllocated = mp['isBatchAllocated'] == 1 ? true : false;
    invenvoucher.inventoryVoucherHeaderDate = mp['inventoryVoucherHeaderDate'];
    invenvoucher.description = mp['description'];
    invenvoucher.itemtype = mp['itemtype'];
    invenvoucher.pieces = mp['pieces'];
    invenvoucher.ratePerPiece = mp['ratePerPiece'];
    invenvoucher.stockLocationPid = mp['stockLocationPid'];
    invenvoucher.stockLocationName = mp['stockLocationName'];
    invenvoucher.bookingNo = mp['bookingNo'];
    invenvoucher.bookingDate = mp['bookingDate'];
    invenvoucher.deliveryDate = mp['deliveryDate'];
    return invenvoucher;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(mp));
      });
    }
  }
}

//tables for DOCUMENT_ACCOUNT_TYPE
class DocumentAccountType {
  final String table = 'DocumentAccountType';
  final String query =
      'CREATE TABLE DocumentAccountType(id INT, documentPid STRING, documentName STRING, accountTypePid STRING, accountTypeName STRING, accountTypeColumn STRING)';

  var id;
  String documentPid;
  String documentName;
  String accountTypePid;
  String accountTypeName;
  String accountTypeColumn;
  DocumentAccountType(
      {this.id,
      this.documentPid,
      this.documentName,
      this.accountTypePid,
      this.accountTypeName,
      this.accountTypeColumn});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'documentPid': mp['documentPid'],
      'documentName': mp['documentName'],
      'accountTypePid': mp['accountTypePid'],
      'accountTypeName': mp['accountTypeName'],
      'accountTypeColumn': mp['accountTypeColumn']
    };
  }

  DocumentAccountType fromMap(mp) {
    DocumentAccountType docproductgroup = DocumentAccountType();
    docproductgroup.id = mp['id'];
    docproductgroup.documentPid = mp['documentPid'];
    docproductgroup.documentName = mp['documentName'];
    docproductgroup.accountTypePid = mp['accountTypePid'];
    docproductgroup.accountTypeName = mp['accountTypeName'];
    docproductgroup.accountTypeColumn = mp['accountTypeColumn'];
    return docproductgroup;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;

        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(mp));
      });
    }
  }
}

//table for DYNAMIC_FORMULA
class DynamicFormula {
  final String table = 'DynamicFormula';
  final String query =
      'CREATE TABLE DynamicFormula(id INT, jsCode STRING, jsCodeName STRING, documentPid STRING, documentName STRING )';

  var id;
  String jsCode;
  String jsCodeName;
  String documentPid;
  String documentName;

  DynamicFormula({
    this.id,
    this.jsCode,
    this.jsCodeName,
    this.documentPid,
    this.documentName,
  });

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'jsCode': mp['jsCode'],
      'jsCodeName': mp['jsCodeName'],
      'documentPid': mp['documentPid'],
      'documentName': mp['documentName'],
    };
  }

  DynamicFormula fromMap(mp) {
    DynamicFormula dynformula = DynamicFormula();
    dynformula.id = mp['id'];
    dynformula.jsCode = mp['jsCode'];
    dynformula.jsCodeName = mp['jsCodeName'];
    dynformula.documentPid = mp['documentPid'];
    dynformula.documentName = mp['documentName'];
    return dynformula;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        mp.forEach((key, value) {
          OurDatabase(table: this.table, query: this.query)
              .insertTable(toMap(mp));
        });
      });
    }
  }
}

// table for PRODUCT_PRICE_LIST
class ProductPriceList {
  final String table = 'ProductPriceList ';
  final String query =
      'CREATE TABLE ProductPriceList (id INT, pid STRING, priceLevelPid STRING, priceLevelName STRING, productProfilePid STRING,productProfileName STRING,rangeFrom DOUBLE,rangeTo DOUBLE,price DOUBLE )';

  var id;
  String pid;
  String priceLevelPid;
  String priceLevelName;
  String productProfilePid;
  String productProfileName;
  double rangeFrom;
  double rangeTo;
  double price;

  ProductPriceList({
    this.id,
    this.pid,
    this.priceLevelPid,
    this.priceLevelName,
    this.productProfileName,
    this.rangeFrom,
    this.rangeTo,
    this.price,
  });

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'priceLevelPid': mp['priceLevelPid'],
      'priceLevelName': mp['priceLevelName'],
      'productProfileName': mp['productProfileName'],
      'rangeFrom': mp['rangeFrom'],
      'rangeTo': mp['rangeTo'],
      'price': mp['price'],
    };
  }

  ProductPriceList fromMap(mp) {
    ProductPriceList prodpricelist = ProductPriceList();
    prodpricelist.id = mp['id'];
    prodpricelist.pid = mp['pid'];
    prodpricelist.priceLevelPid = mp['priceLevelPid'];
    prodpricelist.priceLevelName = mp['priceLevelName'];
    prodpricelist.productProfileName = mp['productProfileName'];
    prodpricelist.rangeFrom = mp['rangeFrom'];
    prodpricelist.rangeTo = mp['rangeTo'];
    prodpricelist.price = mp['price'];
    return prodpricelist;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        mp.forEach((key, value) {
          if (key == "levelListDTOs") {
            //  //print("levelListDTOs ------------------------------");
            var li = value as List;
            li.forEach((element) {
              //  //print(element as Map<String,dynamic>);
              OurDatabase(table: this.table, query: this.query)
                  .insertTable(toMap(element as Map<String, dynamic>));
            });
          }
        });
      });
    }
  }
}

// table for STOCK_LOCATIONS
class StockLocations {
  final String table = 'StockLocations';
  final String query =
      'CREATE TABLE StockLocations  (id INT, pid STRING, name STRING, alias STRING, description STRING,stockLocationType STRING )';

  var id;
  String pid;
  String name;
  String alias;
  String description;
  String stockLocationType;

  StockLocations({
    this.id,
    this.pid,
    this.name,
    this.alias,
    this.description,
    this.stockLocationType,
  });

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'name': mp['name'],
      'alias': mp['alias'],
      'description': mp['description'],
      'stockLocationType': mp['stockLocationType'],
    };
  }

  StockLocations fromMap(mp) {
    StockLocations stockloc = StockLocations();
    stockloc.id = mp['id'];
    stockloc.pid = mp['pid'];
    stockloc.name = mp['name'];
    stockloc.alias = mp['alias'];
    stockloc.description = mp['description'];
    stockloc.stockLocationType = mp['stockLocationType'];
    return stockloc;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      //print(response.body);
      var lists = json.decode(response.body) as List;
      //print(lists);
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(mp));
      });
    }
  }
}

//table for PRICE_LEVEL
class PriceLevel {
  final String table = 'PriceLevel';
  final String query =
      'CREATE TABLE PriceLevel ( pid STRING, name STRING, alias STRING, description STRING, activated BOOLEAN, sortOrder STRING)';

  String pid;
  String name;
  String alias;
  String description;
  bool activated;
  String sortOrder;

  PriceLevel(
      {this.pid,
      this.name,
      this.alias,
      this.description,
      this.activated,
      this.sortOrder});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'pid': mp['pid'],
      'name': mp['name'],
      'alias': mp['alias'],
      'description': mp['description'],
      'activated': mp['activated'],
      'sortOrder': mp['sortOrder']
    };
  }

  PriceLevel fromMap(mp) {
    PriceLevel priceLev = PriceLevel();
    priceLev.pid = mp['pid'];
    priceLev.name = mp['name'];
    priceLev.alias = mp['alias'];
    priceLev.description = mp['description'];
    priceLev.activated = mp['activated'] == 1 ? true : false;
    priceLev.sortOrder = mp['sortOrder'];
    return priceLev;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      //print("PriceLevel============================");
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(mp));
      });
    }
  }
}

//table for DOCUMENT_PRICE_LEVEL
class DocumentPriceLevel {
  final String table = 'DocumentPriceLevel';
  final String query =
      'CREATE TABLE DocumentPriceLevel ( id INT,priceLevelPid STRING, priceLevelName STRING, documentPid STRING, documentName STRING)';
  var id;
  String priceLevelPid;
  String priceLevelName;
  String documentPid;
  String documentName;

  DocumentPriceLevel({
    this.id,
    this.priceLevelPid,
    this.priceLevelName,
    this.documentPid,
    this.documentName,
  });

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'priceLevelPid': mp['priceLevelPid'],
      'priceLevelName': mp['priceLevelName'],
      'documentPid': mp['documentPid'],
      'documentName': mp['documentName']
    };
  }

  DocumentPriceLevel fromMap(mp) {
    DocumentPriceLevel docpricelevel = DocumentPriceLevel();
    docpricelevel.id = mp['id'];
    docpricelevel.priceLevelPid = mp['priceLevelPid'];
    docpricelevel.priceLevelName = mp['priceLevelName'];
    docpricelevel.documentPid = mp['documentPid'];
    docpricelevel.documentName = mp['documentName'];
    return docpricelevel;
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        //print("dfdffdfdfsdf");
        mp.forEach((key, value) {
          //print(mp);
          OurDatabase(table: this.table, query: this.query)
              .insertTable(toMap(mp));
        });
      });
    }
  }
}

//table for RECEIVABLE_PAYABLE
class ReceivablePayable {
  final String table = 'ReceivablePayable ';
  final String query =
      'CREATE TABLE ReceivablePayable  (id INT, pid STRING, accountName STRING, accountPid STRING, receivablePayableType STRING,referenceDocumentNumber STRING,referenceDocumentDate STRING,referenceDocumentType STRING,referenceDocumentAmount DOUBLE,referenceDocumentBalanceAmount DOUBLE, remarks STRING,billOverDue INT,allocatedAmount STRING,closingBalance DOUBLE,supplierAccountPid STRING,supplierAccountName STRING,receivablePayableId STRING,pdcAmount DOUBLE,finalBalance DOUBLE,pdcDate STRING)';
  var id;
  String pid;
  String accountName;
  String accountPid;
  String receivablePayableType;
  String referenceDocumentNumber;
  String referenceDocumentDate;
  String referenceDocumentType;
  double referenceDocumentAmount;
  double referenceDocumentBalanceAmount;
  String remarks;
  int billOverDue;
  String allocatedAmount;
  double closingBalance;
  String supplierAccountPid;
  String supplierAccountName;
  String receivablePayableId;
  double pdcAmount;
  double finalBalance;
  String pdcDate;

  ReceivablePayable(
      {this.id,
      this.pid,
      this.accountName,
      this.accountPid,
      this.receivablePayableType,
      this.referenceDocumentNumber,
      this.referenceDocumentDate,
      this.referenceDocumentType,
      this.referenceDocumentAmount,
      this.referenceDocumentBalanceAmount,
      this.remarks,
      this.billOverDue,
      this.allocatedAmount,
      this.closingBalance,
      this.supplierAccountPid,
      this.supplierAccountName,
      this.receivablePayableId,
      this.pdcAmount,
      this.finalBalance,
      this.pdcDate});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'accountName': mp['accountName'],
      'accountPid': mp['accountPid'],
      'receivablePayableType': mp['receivablePayableType'],
      'referenceDocumentNumber': mp['referenceDocumentNumber'],
      'referenceDocumentDate': mp['referenceDocumentDate'],
      'referenceDocumentType': mp['referenceDocumentType'],
      'referenceDocumentAmount': mp['referenceDocumentAmount'],
      'referenceDocumentBalanceAmount': mp['referenceDocumentBalanceAmount'],
      'remarks': mp['remarks'],
      'billOverDue': mp['billOverDue'],
      'allocatedAmount': mp['allocatedAmount'],
      'closingBalance': mp['closingBalance'],
      'supplierAccountPid': mp['supplierAccountPid'],
      'supplierAccountName': mp['supplierAccountName'],
      'receivablePayableId': mp['receivablePayableId'],
      'pdcAmount': mp['pdcAmount'],
      'finalBalance': mp['finalBalance'],
      'pdcDate': mp['pdcDate']
    };
  }

  ReceivablePayable fromMap(mp) {
    ReceivablePayable recpayable = ReceivablePayable();
    recpayable.id = mp['id'];
    recpayable.pid = mp['pid'];
    recpayable.accountName = mp['accountName'];
    recpayable.accountPid = mp['accountPid'];
    recpayable.receivablePayableType = mp['receivablePayableType'];
    recpayable.referenceDocumentNumber = mp['referenceDocumentNumber'];
    recpayable.referenceDocumentDate = mp['referenceDocumentDate'];
    recpayable.referenceDocumentType = mp['referenceDocumentType'];
    recpayable.referenceDocumentAmount = mp['referenceDocumentAmount'];
    recpayable.referenceDocumentBalanceAmount =
        mp['referenceDocumentBalanceAmount'];
    recpayable.remarks = mp['remarks'];
    recpayable.billOverDue = mp['billOverDue'];
    recpayable.allocatedAmount = mp['allocatedAmount'];
    recpayable.closingBalance = mp['closingBalance'];
    recpayable.supplierAccountPid = mp['supplierAccountPid'];
    recpayable.supplierAccountName = mp['supplierAccountName'];
    recpayable.receivablePayableId = mp['receivablePayableId'];
    recpayable.pdcAmount = mp['pdcAmount'];
    recpayable.finalBalance = mp['finalBalance'];
    recpayable.pdcDate = mp['pdcDate'];
  }

  connectDatabase(Response response) async {
    if (!await OurDatabase(table: this.table).databaseExists()) {
      var lists = json.decode(response.body) as List;
      //print("ReceivablePayable============================");
      lists.forEach((element) {
        var mp = element as Map<String, dynamic>;
        //print(mp);
        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(mp));
      });
    }
  }
}

//table for TASK_EXECUTION_DOCUMENT
class TaskExecutionDocument {
  final String table = 'TaskExecutionDocument ';
  final String query =
      'CREATE TABLE TaskExecutionDocument  (id INT, taskExecutionHeaderId INT, documentPid STRING, isEdit BOOLEAN, numberOfItems DOUBLE,totalAmount DOUBLE)';
  var id;
  var taskExecutionHeaderId;
  String documentPid;
  bool isEdit;
  double numberOfItems;
  double totalAmount;

  TaskExecutionDocument({
    this.id,
    this.taskExecutionHeaderId,
    this.documentPid,
    this.isEdit,
    this.numberOfItems,
    this.totalAmount,
  });

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'taskExecutionHeaderId': mp['taskExecutionHeaderId'],
      'documentPid': mp['documentPid'],
      'isEdit': mp['isEdit'],
      'numberOfItems': mp['numberOfItems'],
      'totalAmount': mp['totalAmount'],
    };
  }

  TaskExecutionDocument fromMap(mp) {
    TaskExecutionDocument taskexcdoc = TaskExecutionDocument();
    taskexcdoc.id = mp['id'];
    taskexcdoc.taskExecutionHeaderId = mp['taskExecutionHeaderId'];
    taskexcdoc.documentPid = mp['documentPid'];
    taskexcdoc.isEdit = mp['isEdit'] == 1 ? true : false;
    taskexcdoc.numberOfItems = mp['numberOfItems'];
    taskexcdoc.totalAmount = mp['totalAmount'];
    return taskexcdoc;
  }

  connectDatabase(Response response) async {}
}

//table for VOUCHER_HEADER
class VoucherHeader {
  final String table = 'VoucherHeader  ';
  final String query =
      'CREATE TABLE VoucherHeader   (id INT, taskExicutionId INT, accountProfileName STRING, documentDate STRING, accountProfilePid STRING,documentName STRING,documentPid STRING,employeeName STRING,employeePid STRING,outstandingAmount DOUBLE, remarks STRING,totalAmount DOUBLE,documentNumberLocal STRING,pidFromServer STRING,documentTakeDate STRING,voucherType STRING,isUpdate BOOLEAN,isNew BOOLEAN,imageRefNo STRING)';
  var id;
  var taskExicutionId;
  String accountProfileName;
  String documentDate;
  String accountProfilePid;
  String documentName;
  String documentPid;
  String employeeName;
  String employeePid;
  double outstandingAmount;
  String remarks;
  double totalAmount;
  String documentNumberLocal;
  String pidFromServer;
  String documentTakeDate;
  String voucherType;
  bool isUpdate;
  bool isNew;
  String imageRefNo;

  VoucherHeader({
    this.id,
    this.taskExicutionId,
    this.accountProfileName,
    this.documentDate,
    this.accountProfilePid,
    this.documentName,
    this.documentPid,
    this.employeeName,
    this.employeePid,
    this.outstandingAmount,
    this.remarks,
    this.totalAmount,
    this.documentNumberLocal,
    this.pidFromServer,
    this.documentTakeDate,
    this.voucherType,
    this.isUpdate,
    this.isNew,
    this.imageRefNo,
  });

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'taskExicutionId': mp['taskExicutionId'],
      'accountProfileName': mp['accountProfileName'],
      'documentDate': mp['documentDate'],
      'accountProfilePid': mp['accountProfilePid'],
      'documentName': mp['documentName'],
      'documentPid': mp['documentPid'],
      'employeeName': mp['employeeName'],
      'employeePid': mp['employeePid'],
      'outstandingAmount': mp['outstandingAmount'],
      'remarks': mp['remarks'],
      'totalAmount': mp['totalAmount'],
      'documentNumberLocal': mp['documentNumberLocal'],
      'pidFromServer': mp['pidFromServer'],
      'documentTakeDate': mp['documentTakeDate'],
      'voucherType': mp['voucherType'],
      'isUpdate': mp['isUpdate'],
      'isNew': mp['isNew'],
      'imageRefNo': mp['imageRefNo'],
    };
  }

  VoucherHeader fromMap(mp) {
    VoucherHeader voucherhead = VoucherHeader();
    voucherhead.id = mp['id'];
    voucherhead.taskExicutionId = mp['taskExicutionId'];
    voucherhead.accountProfileName = mp['accountProfileName'];
    voucherhead.documentDate = mp['documentDate'];
    voucherhead.accountProfilePid = mp['accountProfilePid'];
    voucherhead.documentName = mp['documentName'];
    voucherhead.documentPid = mp['documentPid'];
    voucherhead.employeeName = mp['employeeName'];
    voucherhead.employeePid = mp['employeePid'];
    voucherhead.outstandingAmount = mp['outstandingAmount'];
    voucherhead.remarks = mp['remarks'];
    voucherhead.totalAmount = mp['totalAmount'];
    voucherhead.documentNumberLocal = mp['documentNumberLocal'];
    voucherhead.pidFromServer = mp['pidFromServer'];
    voucherhead.documentTakeDate = mp['documentTakeDate'];
    voucherhead.voucherType = mp['voucherType'];
    voucherhead.isUpdate = mp['isUpdate'] == 1 ? true : false;
    voucherhead.isNew = mp['isNew'] == 1 ? true : false;
    voucherhead.imageRefNo = mp['imageRefNo'];
    return voucherhead;
  }

  connectDatabase(Response response) async {}
}

//table for ACCOUNTING_VOUCHER_ENTRY
class AccountingVoucherEntry {
  final String table = 'AccountingVoucherEntry';
  final String query =
      'CREATE TABLE AccountingVoucherEntry (id INT,mode STRING, amount DOUBLE, accountingHeaderId INT, instrumentNumber STRING, instrumentDate STRING,bankPid STRING,bankName STRING,byAccountPid STRING,byAccountName STRING,toAccountPid STRING, toAccountName STRING,voucherNumber STRING,voucherDate STRING,referenceNumber STRING,remarks STRING,isSaved BOOLEAN,isDetailsShow BOOLEAN,incomeExpenseHeadPid STRING,incomeExpenseHeadName STRING,provisionalReceiptNumber STRING)';
  var id;
  String mode;
  double amount;
  var accountingHeaderId;
  String instrumentNumber;
  String instrumentDate;
  String bankPid;
  String bankName;
  String byAccountPid;
  String byAccountName;
  String toAccountPid;
  String toAccountName;
  String voucherNumber;
  String voucherDate;
  String referenceNumber;
  String remarks;
  bool isSaved;
  bool isDetailsShow;
  String incomeExpenseHeadPid;
  String incomeExpenseHeadName;
  String provisionalReceiptNumber;

  AccountingVoucherEntry({
    this.id,
    this.mode,
    this.amount,
    this.accountingHeaderId,
    this.instrumentNumber,
    this.instrumentDate,
    this.bankPid,
    this.bankName,
    this.byAccountPid,
    this.byAccountName,
    this.toAccountPid,
    this.toAccountName,
    this.voucherNumber,
    this.voucherDate,
    this.referenceNumber,
    this.remarks,
    this.isSaved,
    this.isDetailsShow,
    this.incomeExpenseHeadPid,
    this.incomeExpenseHeadName,
    this.provisionalReceiptNumber,
  });

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'mode': mp['mode'],
      'amount': mp['amount'],
      'accountingHeaderId': mp['accountingHeaderId'],
      'instrumentNumber': mp['instrumentNumber'],
      'instrumentDate': mp['instrumentDate'],
      'bankPid': mp['bankPid'],
      'bankName': mp['bankName'],
      'byAccountPid': mp['byAccountPid'],
      'byAccountName': mp['byAccountName'],
      'toAccountPid': mp['toAccountPid'],
      'toAccountName': mp['toAccountName'],
      'voucherNumber': mp['voucherNumber'],
      'voucherDate': mp['voucherDate'],
      'referenceNumber': mp['referenceNumber'],
      'remarks': mp['remarks'],
      'isSaved': mp['isSaved'],
      'isDetailsShow': mp['isDetailsShow'],
      'incomeExpenseHeadPid': mp['incomeExpenseHeadPid'],
      'incomeExpenseHeadName': mp['incomeExpenseHeadName'],
      'provisionalReceiptNumber': mp['provisionalReceiptNumber']
    };
  }

  AccountingVoucherEntry fromMap(mp) {
    AccountingVoucherEntry accvoucherentry = AccountingVoucherEntry();
    accvoucherentry.id = mp['id'];
    accvoucherentry.mode = mp['mode'];
    accvoucherentry.amount = mp['amount'];
    accvoucherentry.accountingHeaderId = mp['accountingHeaderId'];
    accvoucherentry.instrumentNumber = mp['instrumentNumber'];
    accvoucherentry.instrumentDate = mp['instrumentDate'];
    accvoucherentry.bankPid = mp['bankPid'];
    accvoucherentry.bankName = mp['bankName'];
    accvoucherentry.byAccountPid = mp['byAccountPid'];
    accvoucherentry.byAccountName = mp['byAccountName'];
    accvoucherentry.toAccountPid = mp['toAccountPid'];
    accvoucherentry.toAccountName = mp['toAccountName'];
    accvoucherentry.voucherNumber = mp['voucherNumber'];
    accvoucherentry.voucherDate = mp['voucherDate'];
    accvoucherentry.referenceNumber = mp['referenceNumber'];
    accvoucherentry.remarks = mp['remarks'];
    accvoucherentry.isSaved = mp['isSaved'] == 1 ? true : false;
    accvoucherentry.isDetailsShow = mp['isDetailsShow'] == 1 ? true : false;
    accvoucherentry.incomeExpenseHeadPid = mp['incomeExpenseHeadPid'];
    accvoucherentry.incomeExpenseHeadName = mp['incomeExpenseHeadName'];
    accvoucherentry.provisionalReceiptNumber = mp['provisionalReceiptNumber'];
    return accvoucherentry;
  }

  connectDatabase(Response response) async {}
}

//table for INCOME_EXPENSE_HEAD
class IncomeExpenseHead {
  final String table = 'IncomeExpenseHead ';
  final String query =
      'CREATE TABLE IncomeExpenseHead  (id INT,pid STRING, name STRING, alias STRING, activated BOOLEAN)';
  var id;
  String pid;
  String name;
  String alias;
  bool activated;

  IncomeExpenseHead({
    this.id,
    this.pid,
    this.name,
    this.alias,
    this.activated,
  });

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'name': mp['name'],
      'alias': mp['alias'],
      'activated': mp['activated'],
    };
  }

  IncomeExpenseHead fromMap(mp) {
    IncomeExpenseHead incomeexphead = IncomeExpenseHead();
    incomeexphead.id = mp['id'];
    incomeexphead.pid = mp['pid'];
    incomeexphead.name = mp['name'];
    incomeexphead.alias = mp['alias'];
    incomeexphead.activated = mp['activated'] == 1 ? true : false;
    return incomeexphead;
  }

  connectDatabase(Response response) async {}
}

//table for GUIDED_SELLING_CONFIGURATION
class GuidedSellingConfiguration {
  final String table = 'GuidedSellingConfiguration ';
  final String query =
      'CREATE TABLE GuidedSellingConfiguration   (id INT,guidedSellingFilterItems BOOLEAN,favouriteProductGroupPid STRING, favouriteProductGroupName STRING, favouriteItemCompulsory BOOLEAN, promotionProductGroupPid STRING, promotionProductGroupName STRING, promotionItemCompulsory BOOLEAN,guidedSellingInfoDocumentPid STRING, guidedSellingInfoDocumentName STRING)';
  var id;
  bool guidedSellingFilterItems;
  String favouriteProductGroupPid;
  String favouriteProductGroupName;
  bool favouriteItemCompulsory;
  String promotionProductGroupPid;
  String promotionProductGroupName;
  bool promotionItemCompulsory;
  String guidedSellingInfoDocumentPid;
  String guidedSellingInfoDocumentName;

  GuidedSellingConfiguration({
    this.id,
    this.guidedSellingFilterItems,
    this.favouriteProductGroupPid,
    this.favouriteProductGroupName,
    this.favouriteItemCompulsory,
    this.promotionProductGroupPid,
    this.promotionProductGroupName,
    this.promotionItemCompulsory,
    this.guidedSellingInfoDocumentPid,
    this.guidedSellingInfoDocumentName,
  });

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'guidedSellingFilterItems': mp['guidedSellingFilterItems'],
      'favouriteProductGroupPid': mp['favouriteProductGroupPid'],
      'favouriteProductGroupName': mp['favouriteProductGroupName'],
      'favouriteItemCompulsory': mp['favouriteItemCompulsory'],
      'promotionProductGroupPid': mp['promotionProductGroupPid'],
      'promotionProductGroupName': mp['promotionProductGroupName'],
      'promotionItemCompulsory': mp['promotionItemCompulsory'],
      'guidedSellingInfoDocumentPid': mp['guidedSellingInfoDocumentPid'],
      'guidedSellingInfoDocumentName': mp['guidedSellingInfoDocumentName'],
    };
  }

  GuidedSellingConfiguration fromMap(mp) {
    GuidedSellingConfiguration guidedsellconfig = GuidedSellingConfiguration();
    guidedsellconfig.id = mp['id'];
    guidedsellconfig.guidedSellingFilterItems =
        mp['guidedSellingFilterItems'] == 1 ? true : false;
    guidedsellconfig.favouriteProductGroupPid = mp['favouriteProductGroupPid'];
    guidedsellconfig.favouriteProductGroupName =
        mp['favouriteProductGroupName'];
    guidedsellconfig.favouriteItemCompulsory =
        mp['favouriteItemCompulsory'] == 1 ? true : false;
    guidedsellconfig.promotionProductGroupPid = mp['promotionProductGroupPid'];
    guidedsellconfig.promotionProductGroupName =
        mp['promotionProductGroupName'];
    guidedsellconfig.promotionItemCompulsory =
        mp['promotionItemCompulsory'] == 1 ? true : false;
    guidedsellconfig.guidedSellingInfoDocumentPid =
        mp['guidedSellingInfoDocumentPid'];
    guidedsellconfig.guidedSellingInfoDocumentName =
        mp['guidedSellingInfoDocumentName'];
    return guidedsellconfig;
  }

  connectDatabase(List list) async {
    if (list != null) {
      list.forEach((element) {
        var map = element as Map<String, dynamic>;
        OurDatabase(table: this.table, query: this.query)
            .insertTable(toMap(map));
      });
    }
  }
}

//table for PRODUCT_TO_PRODUCT_GROUP
class ProductToProductGroup {
  final String table = 'ProductToProductGroup ';
  final String query =
      'CREATE TABLE ProductToProductGroup (id INT, pid STRING, name STRING, alias STRING, description STRING, price DOUBLE, sku STRING, unitQty STRING, productCategoryPid STRING, productCategoryName STRING, divisionPid STRING, divisionName STRING, productGroupPid STRING)';

  var id;
  String pid;
  String name;
  String alias;
  String description;
  double price;
  String sku;
  String unitQty;
  String productCategoryPid;
  String productCategoryName;
  String divisionPid;
  String divisionName;
  String productGroupPid;

  ProductToProductGroup(
      {this.id,
      this.pid,
      this.name,
      this.alias,
      this.description,
      this.price,
      this.sku,
      this.unitQty,
      this.productCategoryPid,
      this.productCategoryName,
      this.divisionPid,
      this.divisionName,
      this.productGroupPid});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'name': mp['name'],
      'alias': mp['alias'],
      'description': mp['description'],
      'price': mp['price'],
      'sku': mp['sku'],
      'unitQty': mp['unitQty'],
      'productCategoryPid': mp['productCategoryPid'],
      'productCategoryName': mp['productCategoryName'],
      'divisionPid': mp['divisionPid'],
      'divisionName': mp['divisionName'],
      'productGroupPid': mp['productGroupPid'],
    };
  }

  ProductToProductGroup fromMap(mp) {
    ProductToProductGroup prodoprogroup = ProductToProductGroup();
    prodoprogroup.id = mp['id'];
    prodoprogroup.pid = mp['pid'];
    prodoprogroup.name = mp['name'];
    prodoprogroup.alias = mp['alias'];
    prodoprogroup.description = mp['description'];
    prodoprogroup.price = mp['price'];
    prodoprogroup.sku = mp['sku'];
    prodoprogroup.unitQty = mp['unitQty'];
    prodoprogroup.productCategoryPid = mp['productCategoryPid'];
    prodoprogroup.productCategoryName = mp['productCategoryName'];
    prodoprogroup.divisionPid = mp['divisionPid'];
    prodoprogroup.divisionName = mp['divisionName'];
    prodoprogroup.productGroupPid = mp['productGroupPid'];
    return prodoprogroup;
  }

  connectDatabase(Map<String, dynamic> mp) async {}
}

//  table for FavoriteResponse
class FavoriteResponse {
  final String table = 'FavoriteResponse ';
  final String query =
      'CREATE TABLE FavoriteResponse  (userPid STRING, activityPid STRING, activityName STRING, documentPid STRING, documentName STRING, sortOrder INT)';

  String userPid;
  String activityPid;
  String activityName;
  String documentPid;
  String documentName;
  int sortOrder;

  FavoriteResponse(
      {this.userPid,
      this.activityPid,
      this.activityName,
      this.documentPid,
      this.documentName,
      this.sortOrder});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'userPid': mp['userPid'],
      'activityPid': mp['activityPid'],
      'activityName': mp['activityName'],
      'documentPid': mp['documentPid'],
      'documentName': mp['documentName'],
      'sortOrder': mp['sortOrder'],
    };
  }

  FavoriteResponse fromMap(mp) {
    FavoriteResponse favresponse = FavoriteResponse();
    favresponse.userPid = mp['userPid'];
    favresponse.activityPid = mp['activityPid'];
    favresponse.activityName = mp['activityName'];
    favresponse.documentPid = mp['documentPid'];
    favresponse.documentName = mp['documentName'];
    favresponse.sortOrder = mp['sortOrder'];
    return favresponse;
  }

  connectDatabase(Response response) async {}
}

//table for ACTIVITY_NOTIFICATION_DTO
class ActivityNotificationDTO {
  final String table = 'ActivityNotificationDTO  ';
  final String query =
      'CREATE TABLE ActivityNotificationDTO   (notificationType STRING, activityPid STRING, documentPid STRING, sendCustomer BOOLEAN, other BOOLEAN, phoneNumbers STRING)';

  String notificationType;
  String activityPid;
  String documentPid;
  bool sendCustomer;
  bool other;
  String phoneNumbers;

  ActivityNotificationDTO(
      {this.notificationType,
      this.activityPid,
      this.documentPid,
      this.sendCustomer,
      this.other,
      this.phoneNumbers});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'notificationType': mp['notificationType'],
      'activityPid': mp['activityPid'],
      'documentPid': mp['documentPid'],
      'sendCustomer': mp['sendCustomer'],
      'other': mp['other'],
      'phoneNumbers': mp['phoneNumbers'],
    };
  }

  ActivityNotificationDTO fromMap(mp) {
    ActivityNotificationDTO activityNotification = ActivityNotificationDTO();
    activityNotification.notificationType = mp['notificationType'];
    activityNotification.activityPid = mp['activityPid'];
    activityNotification.documentPid = mp['documentPid'];
    activityNotification.sendCustomer = mp['sendCustomer'] == 1 ? true : false;
    activityNotification.other = mp['other'] == 1 ? true : false;
    activityNotification.phoneNumbers = mp['phoneNumbers'];
    return activityNotification;
  }

  connectDatabase(Response response) async {}
}

//table for DAYPLAN_EXECUTE_CONFIG
class DayplanExecuteConfig {
  final String table = 'DayplanExecuteConfig   ';
  final String query =
      'CREATE TABLE DayplanExecuteConfig    (id INT, name STRING, sortOrder INT)';

  var id;
  String name;
  int sortOrder;

  DayplanExecuteConfig({
    this.id,
    this.name,
    this.sortOrder,
  });

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'name': mp['name'],
      'sortOrder': mp['sortOrder'],
    };
  }

  DayplanExecuteConfig fromMap(mp) {
    DayplanExecuteConfig dayplanexcconfig = DayplanExecuteConfig();
    dayplanexcconfig.id = mp['id'];
    dayplanexcconfig.name = mp['name'];
    dayplanexcconfig.sortOrder = mp['sortOrder'];
    return dayplanexcconfig;
  }

  connectDatabase(Response response) async {}
}

//table for EXECUTIVE_ACTIVITY_UPDATE
class ExecutiveActivityUpdate {
  final String table = 'ExecutiveActivityUpdate  ';
  final String query =
      'CREATE TABLE ExecutiveActivityUpdate   (id INT, pid STRING, name STRING, alias STRING, description STRING, hasDefaultAccount BOOLEAN,target DOUBLE, achieved DOUBLE, today DOUBLE)';

  var id;
  String pid;
  String name;
  String alias;
  String description;
  bool hasDefaultAccount;
  double target;
  double achieved;
  double today;

  ExecutiveActivityUpdate({
    this.id,
    this.pid,
    this.name,
    this.alias,
    this.description,
    this.hasDefaultAccount,
    this.target,
    this.achieved,
    this.today,
  });

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      'id': mp['id'],
      'pid': mp['pid'],
      'name': mp['name'],
      'alias': mp['alias'],
      'description': mp['description'],
      'hasDefaultAccount': mp['hasDefaultAccount'],
      'target': mp['target'],
      'achieved': mp['achieved'],
      'today': mp['today'],
    };
  }

  ExecutiveActivityUpdate fromMap(mp) {
    ExecutiveActivityUpdate excactivityupdate = ExecutiveActivityUpdate();
    excactivityupdate.id = mp['id'];
    excactivityupdate.pid = mp['pid'];
    excactivityupdate.name = mp['name'];
    excactivityupdate.alias = mp['alias'];
    excactivityupdate.description = mp['description'];
    excactivityupdate.hasDefaultAccount =
        mp['hasDefaultAccount'] == 1 ? true : false;
    excactivityupdate.target = mp['target'];
    excactivityupdate.achieved = mp['achieved'];
    excactivityupdate.today = mp['today'];
    return excactivityupdate;
  }

  connectDatabase(Response response) async {}
}

//table INVENTORY_VOUCHER_DETAILS
class InventoryVoucherHeader {
  final String table = 'InventoryVoucherHeader';
  final String query =
      'CREATE TABLE InventoryVoucherHeader( id INT, taskExecutionHeaderId INT, documentPid STRING, documentName STRING, documentDate STRING, numberOfProducts DOUBLE, totalAmount DOUBLE, documentVolume DOUBLE, remarks STRING, documentNumberLocal STRING, receiverAccountPid STRING, receiverAccountName STRING, supplierAccountPid STRING, supplierAccountName STRING, sourceStockLocationPid STRING ,sourceStockLocationName STRING, destinationStockLocationPid STRING, destinationStockLocationName STRING, employeePid STRING, employeeName STRING, selectedPriceLevelPid STRING, selectedPriceLevelName STRING, pidFromServer STRING, discountInPercentage STRING,discountInAmount STRING, isUpdate BOOLEAN, isNew BOOLEAN, referenceInvoiceNumber STRING, roundedOff DOUBLE, bookingNo STRING, bookingDate STRING, deliveryDate STRING, validationDays STRING )';
  var id;
  var taskExecutionHeaderId;
  String documentPid;
  String documentName;
  String documentDate;
  double numberOfProducts;
  double totalAmount;
  double documentVolume;
  String remarks;
  String documentNumberLocal;
  String receiverAccountPid;
  String receiverAccountName;
  String supplierAccountPid;
  String supplierAccountName;
  String sourceStockLocationPid;
  String sourceStockLocationName;
  String destinationStockLocationPid;
  String destinationStockLocationName;
  String employeePid;
  String employeeName;
  String selectedPriceLevelPid;
  String selectedPriceLevelName;
  String pidFromServer;
  double discountInPercentage;
  double discountInAmount;
  bool isUpdate;
  bool isNew;
  String referenceInvoiceNumber;
  double roundedOff;
  String bookingNo;
  String bookingDate;
  String deliveryDate;
  String validationDays;
  InventoryVoucherHeader({
    this.id,
    this.taskExecutionHeaderId,
    this.documentPid,
    this.documentName,
    this.documentDate,
    this.numberOfProducts,
    this.totalAmount,
    this.documentVolume,
    this.remarks,
    this.documentNumberLocal,
    this.receiverAccountPid,
    this.receiverAccountName,
    this.supplierAccountPid,
    this.supplierAccountName,
    this.sourceStockLocationPid,
    this.sourceStockLocationName,
    this.destinationStockLocationPid,
    this.destinationStockLocationName,
    this.employeePid,
    this.employeeName,
    this.selectedPriceLevelPid,
    this.selectedPriceLevelName,
    this.pidFromServer,
    this.discountInPercentage,
    this.discountInAmount,
    this.isUpdate,
    this.isNew,
    this.referenceInvoiceNumber,
    this.roundedOff,
    this.bookingNo,
    this.bookingDate,
    this.deliveryDate,
    this.validationDays,
  });

  Map<String, dynamic> toMap(mp) {
    return {
      'id': null,
      'taskExecutionHeaderId': mp.taskExecutionHeaderId,
      'documentPid': mp.documentPid,
      'documentName': mp.documentName,
      'documentDate': mp.documentDate,
      'numberOfProducts': mp.numberOfProducts,
      'totalAmount': mp.totalAmount,
      'documentVolume': mp.documentVolume,
      'remarks': mp.remarks,
      'documentNumberLocal': mp.documentNumberLocal,
      'receiverAccountPid': mp.receiverAccountPid,
      'receiverAccountName': mp.receiverAccountName,
      'supplierAccountPid': mp.supplierAccountPid,
      'supplierAccountName': mp.supplierAccountName,
      'sourceStockLocationPid': mp.sourceStockLocationPid,
      'sourceStockLocationName': mp.sourceStockLocationName,
      'destinationStockLocationPid': mp.destinationStockLocationPid,
      'destinationStockLocationName': mp.destinationStockLocationName,
      'employeePid': mp.employeePid,
      'employeeName': mp.employeeName,
      'selectedPriceLevelPid': mp.selectedPriceLevelPid,
      'selectedPriceLevelName': mp.selectedPriceLevelName,
      'pidFromServer': mp.pidFromServer,
      'discountInPercentage': mp.discountInPercentage,
      'discountInAmount': mp.discountInAmount,
      'isUpdate': mp.isUpdate,
      'isNew': mp.isNew,
      'referenceInvoiceNumber': mp.referenceInvoiceNumber,
      'roundedOff': mp.roundedOff,
      'bookingNo': mp.bookingNo,
      'bookingDate': mp.bookingDate,
      'deliveryDate': mp.deliveryDate,
      'validationDays': mp.validationDays,
    };
  }

  InventoryVoucherHeader fromMap(mp) {
    InventoryVoucherHeader invenvoucher = InventoryVoucherHeader();
    invenvoucher.id = mp['id'];
    invenvoucher.taskExecutionHeaderId = mp['taskExecutionHeaderId'];
    invenvoucher.documentPid = mp['documentPid'];
    invenvoucher.documentName = mp['documentName'];
    invenvoucher.documentDate = mp['documentDate'];
    invenvoucher.numberOfProducts = mp['numberOfProducts'];
    invenvoucher.totalAmount = mp['totalAmount'];
    invenvoucher.documentVolume = mp['documentVolume'];
    invenvoucher.remarks = mp['remarks'];
    invenvoucher.documentNumberLocal = mp['documentNumberLocal'];
    invenvoucher.receiverAccountPid = mp['receiverAccountPid'];
    invenvoucher.receiverAccountName = mp['receiverAccountName'];
    invenvoucher.supplierAccountPid = mp['supplierAccountPid'];
    invenvoucher.supplierAccountName = mp['supplierAccountName'];
    invenvoucher.sourceStockLocationPid = mp['sourceStockLocationPid'];
    invenvoucher.sourceStockLocationName = mp['sourceStockLocationName'];
    invenvoucher.destinationStockLocationPid =
        mp['destinationStockLocationPid'];
    invenvoucher.destinationStockLocationName =
        mp['destinationStockLocationName'];
    invenvoucher.employeePid = mp['employeePid'];
    invenvoucher.employeeName = mp['employeeName'];
    invenvoucher.selectedPriceLevelPid = mp['selectedPriceLevelPid'];
    invenvoucher.selectedPriceLevelName = mp['selectedPriceLevelName'];
    invenvoucher.pidFromServer = mp['pidFromServer'];
    invenvoucher.discountInPercentage = mp['discountInPercentage'];
    invenvoucher.discountInAmount = mp['discountInAmount'];
    invenvoucher.isUpdate = mp['isUpdate'];
    invenvoucher.isNew = mp['isNew'];
    invenvoucher.referenceInvoiceNumber = mp['referenceInvoiceNumber'];
    invenvoucher.roundedOff = mp['roundedOff'];
    invenvoucher.bookingNo = mp['bookingNo'];
    invenvoucher.bookingDate = mp['bookingDate'];
    invenvoucher.deliveryDate = mp['deliveryDate'];
    invenvoucher.validationDays = mp['validationDays'];

    return invenvoucher;
  }

  connectDatabase(mp) async {}
}

class GeoLocationInfo {
  final String table = 'GeoLocationInfo';
  final String query =
      'CREATE TABLE GeoLocationInfo( latitude DOUBLE, longitude DOUBLE, isSuccess DOUBLE, isFlightModeOn BOOLEAN,isGPSLocation BOOLEAN, errorMessage STRING,mcc INT, mnc INT, lac INT, cid INT , isMockLocation BOOLEAN )';

  double latitude;
  double longitude;
  bool isSuccess;
  bool isFlightModeOn;
  bool isGPSLocation;
  String errorMessage;
  int mcc;
  int mnc;
  int lac;
  int cid;
  bool isMockLocation;
  GeoLocationInfo({
    this.latitude,
    this.longitude,
    this.isSuccess,
    this.isFlightModeOn,
    this.isGPSLocation,
    this.errorMessage,
    this.mcc,
    this.mnc,
    this.lac,
    this.cid,
    this.isMockLocation,
  });
  Map<String, dynamic> toMap(mp) {
    return {
      'latitude': mp.latitude,
      'longitude': mp.longitude,
      'isSuccess': mp.isSuccess,
      'isFlightModeOn': mp.isFlightModeOn,
      'isGPSLocation': mp.isGPSLocation,
      'errorMessage': mp.errorMessage,
      'mcc': mp.mcc,
      'mnc': mp.mnc,
      'lac': mp.lac,
      'cid': mp.cid,
      'isMockLocation': mp.isMockLocation,
    };
  }
}

class TaskExecutionJson {
  final String table = 'TaskExecutionJson';
  final String query =
      'CREATE TABLE TaskExecutionJson(  pid STRING,taskExecutionId INT, latitude DOUBLE, longitude DOUBLE mcc STRING,mnc STRING,lac STRING,cellId STRING,date STRING,remarks STRING,activityPid STRING,activityName STRING,accountTypePid STRING,accountTypeName STRING,accountProfilePid STRING,accountProfileName STRING,locationType STRING,isGpsOff BOOLEAN ,isMobileDataOff BOOLEAN,executiveTaskPlanPid STRING, startLatitude DOUBLE,startLongitude DOUBLE ,startMcc STRING,startMnc STRING,startLac STRING,startCellId STRING,startLocationType STRING, startIsGpsOff BOOLEAN,startIsMobileDataOff BOOLEAN,startTimen STRING,endTime STRING,clientTransactionKey STRING , interimSave BOOLEAN,sendDate STRING, punchInDate STRING,  mockLocationStatus BOOLEAN,  withCustomer BOOLEAN, )';
  String pid;
  var taskExecutionId;
  double latitude;
  double longitude;
  String mcc;
  String mnc;
  String lac;
  String cellId;
  String date;
  String remarks;
  String activityPid;
  String activityName;
  String accountTypePid;
  String accountTypeName;
  String accountProfilePid;
  String accountProfileName;
  String locationType;
  bool isGpsOff;
  bool isMobileDataOff;
  String executiveTaskPlanPid;
  double startLatitude;
  double startLongitude;
  String startMcc;
  String startMnc;
  String startLac;
  String startCellId;
  LocationType startLocationType;
  bool startIsGpsOff;
  bool startIsMobileDataOff;
  String startTime;
  String endTime;
  String clientTransactionKey;
  bool interimSave;
  String sendDate;
  String punchInDate;
  bool mockLocationStatus;
  bool withCustomer;

  TaskExecutionJson({
    this.pid,
    this.taskExecutionId,
    this.latitude,
    this.longitude,
    this.mcc,
    this.mnc,
    this.lac,
    this.cellId,
    this.date,
    this.remarks,
    this.activityPid,
    this.activityName,
    this.accountTypePid,
    this.accountTypeName,
    this.accountProfilePid,
    this.accountProfileName,
    this.locationType,
    this.isGpsOff,
    this.isMobileDataOff,
    this.executiveTaskPlanPid,
    this.startLatitude,
    this.startLongitude,
    this.startMcc,
    this.startMnc,
    this.startLac,
    this.startCellId,
    this.startLocationType,
    this.startIsGpsOff,
    this.startIsMobileDataOff,
    this.startTime,
    this.endTime,
    this.clientTransactionKey,
    this.interimSave,
    this.sendDate,
    this.punchInDate,
    this.mockLocationStatus,
    this.withCustomer,
  });
  Map<String, dynamic> toMap(mp) {
    return {
      'pid': mp.pid,
      'taskExecutionId': mp.taskExecutionId,
      'latitude': mp.latitude,
      'longitude': mp.longitude,
      'mcc': mp.mcc,
      'mnc': mp.mnc,
      'lac': mp.lac,
      'cellId': mp.cellId,
      'date': mp.date,
      'remarks': mp.remarks,
      'activityPid': mp.activityPid,
      'activityName': mp.activityName,
      'accountTypePid': mp.accountTypePid,
      'accountTypeName': mp.accountTypeName,
      'accountProfilePid': mp.accountProfilePid,
      'accountProfileName': mp.accountProfileName,
      'locationType': mp.locationType,
      'isGpsOff': mp.isGpsOff,
      'isMobileDataOff': mp.isMobileDataOff,
      'executiveTaskPlanPid': mp.executiveTaskPlanPid,
      'startLatitude': mp.startLatitude,
      'startLongitude': mp.startLongitude,
      'startMcc': mp.startMcc,
      'startMnc': mp.startMnc,
      'startLac': mp.startLac,
      'startCellId': mp.startCellId,
      'startLocationType': mp.startLocationType,
      'startIsGpsOff': mp.startIsGpsOff,
      'startIsMobileDataOff': mp.startIsMobileDataOff,
      'startTime': mp.startTime,
      'endTime': mp.endTime,
      'clientTransactionKey': mp.clientTransactionKey,
      'interimSave': mp.interimSave,
      'sendDate': mp.sendDate,
      'punchInDate': mp.punchInDate,
      'mockLocationStatus': mp.mockLocationStatus,
      'withCustomer': mp.withCustomer,
    };
  }

  TaskExecutionJson fromMap(mp) {
    TaskExecutionJson taskexecutionJson = TaskExecutionJson();

    taskexecutionJson.pid;
    mp.pid;
    taskexecutionJson.taskExecutionId;
    mp.taskExecutionId;
    taskexecutionJson.latitude;
    mp.latitude;
    taskexecutionJson.longitude;
    mp.longitude;
    taskexecutionJson.mcc;
    mp.mcc;
    taskexecutionJson.mnc;
    mp.mnc;
    taskexecutionJson.lac;
    mp.lac;
    taskexecutionJson.cellId;
    mp.cellId;
    taskexecutionJson.date;
    mp.date;
    taskexecutionJson.remarks;
    mp.remarks;
    taskexecutionJson.activityPid;
    mp.activityPid;
    taskexecutionJson.activityName;
    mp.activityName;
    taskexecutionJson.accountTypePid;
    mp.accountTypePid;
    taskexecutionJson.accountTypeName;
    mp.accountTypeName;
    taskexecutionJson.accountProfilePid;
    mp.accountProfilePid;
    taskexecutionJson.accountProfileName;
    mp.accountProfileName;
    taskexecutionJson.locationType;
    mp.locationType;
    taskexecutionJson.isGpsOff;
    mp.isGpsOff;
    taskexecutionJson.isMobileDataOff;
    mp.isMobileDataOff;
    taskexecutionJson.executiveTaskPlanPid;
    mp.executiveTaskPlanPid;
    taskexecutionJson.startLatitude;
    mp.startLatitude;
    taskexecutionJson.startLongitude;
    mp.startLongitude;
    taskexecutionJson.startMcc;
    mp.startMcc;
    taskexecutionJson.startMnc;
    mp.startMnc;
    taskexecutionJson.startLac;
    mp.startLac;
    taskexecutionJson.startCellId;
    mp.startCellId;
    taskexecutionJson.startLocationType;
    mp.startLocationType;
    taskexecutionJson.startIsGpsOff;
    mp.startIsGpsOff;
    taskexecutionJson.startIsMobileDataOff;
    mp.startIsMobileDataOff;
    taskexecutionJson.startTime;
    mp.startTime;
    taskexecutionJson.endTime;
    mp.endTime;
    taskexecutionJson.clientTransactionKey;
    mp.clientTransactionKey;
    taskexecutionJson.interimSave;
    mp.interimSave;
    taskexecutionJson.sendDate;
    mp.sendDate;
    taskexecutionJson.punchInDate;
    mp.punchInDate;
    taskexecutionJson.mockLocationStatus;
    mp.mockLocationStatus;
    taskexecutionJson.withCustomer;
    mp.withCustomer;
    return taskexecutionJson;
  }
}

class TaskExecutionSubmissionJson {
  final String table = 'TaskExecutionSubmissionJson';
  final String query =
      'CREATE TABLE TaskExecutionSubmissionJson( executiveTaskExecutionDTO STRING,inventoryVouchers LIST)';
  TaskExecutionJson executiveTaskExecutionDTO;
//List<DynamicDocumentHeader> dynamicDocuments;
  List<InventoryVoucherHeaderJson> inventoryVouchers;
//List<AccountingVoucherHeader> accountingVouchers;
  TaskExecutionSubmissionJson({
    this.executiveTaskExecutionDTO,
// this.dynamicDocuments,
    this.inventoryVouchers,
// this.accountingVouchers,
  });
  Map<String, dynamic> toMap(mp) {
    return {
      'executiveTaskExecutionDTO': mp.executiveTaskExecutionDTO,
// 'dynamicDocuments':mp.dynamicDocuments,
      'inventoryVouchers': mp.inventoryVouchers,
// 'accountingVouchers':mp.accountingVouchers,
    };
  }

  Map<String, dynamic> toJson(mp) {
    Map<String, dynamic> inventoryVouchersMap = {};
    var inventoryVouchersLs = mp.inventoryVouchers as List;
    inventoryVouchersLs.forEach((element) {
      inventoryVouchersMap.addAll(InventoryVoucherHeader().toMap(element));
    });

    List datalist = [];
    datalist.add(inventoryVouchersMap);
    return {
      'executiveTaskExecutionDTO':
          TaskExecutionJson().toMap(mp.executiveTaskExecutionDTO),
// 'dynamicDocuments':mp.dynamicDocuments,
      'inventoryVouchers': datalist
// 'accountingVouchers':mp.accountingVouchers,
    };
  }
}

class InventoryVoucherDetailsJson {
  final String table = 'InventoryVoucherDetailsJson';
  final String query =
      'CREATE TABLE InventoryVoucherDetailsJson(detailId INT,productPid STRING,productName STRING,quantity DOUBLE,freeQuantity DOUBLE,sellingRate DOUBLE,taxPercentage DOUBLE,discountPercentage DOUBLE,batchNumber DOUBLE,rowTotal DOUBLE,batchDate STRING,purchaseRate STRING,length DOUBLE,width DOUBLE,thickness DOUBLE,size DOUBLE,discountAmount DOUBLE,taxAmount DOUBLE,sourceStockLocationPid STRING,sourceStockLocationName STRING ,destinationStockLocationPid STRING,destinationStockLocationName STRING,referenceInventoryVoucherHeaderPid STRING,referenceInventoryVoucherDetailId STRING,remarks STRING,mrpDOUBLE,itemtype STRING,pieces DOUBLE ,ratePerPiece STRING,stockLocationPid STRING,stockLocationName STRING,bookingNo STRING,bookingDate STRING,deliveryDate STRING )';
  var detailId;
  String productPid;
  String productName;
  double quantity;
  double freeQuantity;
  double sellingRate;
  double taxPercentage;
  double discountPercentage;
  double batchNumber;
  double rowTotal;
  String batchDate;
  String purchaseRate;
  double length;
  double width;
  double thickness;
  double size;
  double discountAmount;
  double taxAmount;
  String sourceStockLocationPid;
  String sourceStockLocationName;
  String destinationStockLocationPid;
  String destinationStockLocationName;
  String referenceInventoryVoucherHeaderPid;
  String referenceInventoryVoucherDetailId;
  String remarks;
  double mrp;
  String itemtype;
  double pieces;
  double ratePerPiece;
  String stockLocationPid;
  String stockLocationName;
  String bookingNo;
  String bookingDate;
  String deliveryDate;
  List<InventoryBatchAllocationJson> inventoryVoucherBatchDetails;

  InventoryVoucherDetailsJson({
    this.detailId,
    this.productPid,
    this.productName,
    this.quantity,
    this.freeQuantity,
    this.sellingRate,
    this.taxPercentage,
    this.discountPercentage,
    this.batchNumber,
    this.rowTotal,
    this.batchDate,
    this.purchaseRate,
    this.length,
    this.width,
    this.thickness,
    this.size,
    this.discountAmount,
    this.taxAmount,
    this.sourceStockLocationPid,
    this.sourceStockLocationName,
    this.destinationStockLocationPid,
    this.destinationStockLocationName,
    this.referenceInventoryVoucherHeaderPid,
    this.referenceInventoryVoucherDetailId,
    this.remarks,
    this.mrp,
    this.itemtype,
    this.pieces,
    this.ratePerPiece,
    this.stockLocationPid,
    this.stockLocationName,
    this.bookingNo,
    this.bookingDate,
    this.deliveryDate,
    this.inventoryVoucherBatchDetails,
  });
  Map<String, dynamic> toMap(mp) {
    return {
      'detailId': mp.detailId,
      'productPid': mp.productPid,
      'productName': mp.productName,
      'quantity': mp.quantity,
      'freeQuantity': mp.freeQuantity,
      'sellingRate': mp.sellingRate,
      'taxPercentage': mp.taxPercentage,
      'discountPercentage': mp.discountPercentage,
      'batchNumber': mp.batchNumber,
      'rowTotal': mp.rowTotal,
      'batchDate': mp.batchDate,
      'purchaseRate': mp.purchaseRate,
      'length': mp.length,
      'width': mp.width,
      'thickness': mp.thickness,
      'size': mp.size,
      'discountAmount': mp.discountAmount,
      'taxAmount': mp.taxAmount,
      'sourceStockLocationPid': mp.sourceStockLocationPid,
      'sourceStockLocationName': mp.sourceStockLocationName,
      'destinationStockLocationPid': mp.destinationStockLocationPid,
      'destinationStockLocationName': mp.destinationStockLocationName,
      'referenceInventoryVoucherHeaderPid':
          mp.referenceInventoryVoucherHeaderPid,
      'referenceInventoryVoucherDetailId': mp.referenceInventoryVoucherDetailId,
      'remarks': mp.remarks,
      'mrp': mp.mrp,
      'itemtype': mp.itemtype,
      'pieces': mp.pieces,
      'ratePerPiece': mp.ratePerPiece,
      'stockLocationPid': mp.stockLocationPid,
      'stockLocationName': mp.stockLocationName,
      'bookingNo': mp.bookingNo,
      'bookingDate': mp.bookingDate,
      'deliveryDate': mp.deliveryDate,
      'inventoryVoucherBatchDetails': mp.inventoryVoucherBatchDetails
    };
  }

  fromMap(mp) {
    InventoryVoucherDetailsJson inventoryvoucherdetailjson =
        InventoryVoucherDetailsJson();
    inventoryvoucherdetailjson.detailId = mp.detailId;
    inventoryvoucherdetailjson.productPid = mp.productPid;
    inventoryvoucherdetailjson.productName = mp.productName;
    inventoryvoucherdetailjson.quantity = mp.quantity;
    inventoryvoucherdetailjson.freeQuantity = mp.freeQuantity;
    inventoryvoucherdetailjson.sellingRate = mp.sellingRate;
    inventoryvoucherdetailjson.taxPercentage = mp.taxPercentage;
    inventoryvoucherdetailjson.discountPercentage = mp.discountPercentage;
    inventoryvoucherdetailjson.batchNumber = mp.batchNumber;
    inventoryvoucherdetailjson.rowTotal = mp.rowTotal;
    inventoryvoucherdetailjson.batchDate = mp.batchDate;
    inventoryvoucherdetailjson.purchaseRate = mp.purchaseRate;
    inventoryvoucherdetailjson.length = mp.length;
    inventoryvoucherdetailjson.width = mp.width;
    inventoryvoucherdetailjson.thickness = mp.thickness;
    inventoryvoucherdetailjson.size = mp.size;
    inventoryvoucherdetailjson.discountAmount = mp.discountAmount;
    inventoryvoucherdetailjson.taxAmount = mp.taxAmount;
    inventoryvoucherdetailjson.sourceStockLocationPid =
        mp.sourceStockLocationPid;
    inventoryvoucherdetailjson.sourceStockLocationName =
        mp.sourceStockLocationName;
    inventoryvoucherdetailjson.destinationStockLocationPid =
        mp.destinationStockLocationPid;
    inventoryvoucherdetailjson.destinationStockLocationName =
        mp.destinationStockLocationName;
    inventoryvoucherdetailjson.referenceInventoryVoucherHeaderPid =
        mp.referenceInventoryVoucherHeaderPid;
    inventoryvoucherdetailjson.referenceInventoryVoucherDetailId =
        mp.referenceInventoryVoucherDetailId;
    inventoryvoucherdetailjson.remarks = mp.remarks;
    inventoryvoucherdetailjson.mrp = mp.mrp;
    inventoryvoucherdetailjson.itemtype = mp.itemtype;
    inventoryvoucherdetailjson.pieces = mp.pieces;
    inventoryvoucherdetailjson.ratePerPiece = mp.ratePerPiece;
    inventoryvoucherdetailjson.stockLocationPid = mp.stockLocationPid;
    inventoryvoucherdetailjson.stockLocationName = mp.stockLocationName;
    inventoryvoucherdetailjson.bookingNo = mp.bookingNo;
    inventoryvoucherdetailjson.bookingDate = mp.bookingDate;
    inventoryvoucherdetailjson.deliveryDate = mp.deliveryDate;
    return inventoryvoucherdetailjson;
  }
}

class InventoryBatchAllocationJson {
  final String table = 'InventoryBatchAllocationJson';
  final String query =
      'CREATE TABLE InventoryBatchAllocationJson( inventoryVoucherDetailId STRING,productProfilePid STRING,productProfileName STRING,batchNumber STRING,batchDate STRING,remarks STRING,quantity DOUBLE,stockLocationPid STRING )';

  String inventoryVoucherDetailId;
  String productProfilePid;
  String productProfileName;
  String batchNumber;
  String batchDate;
  String remarks;
  double quantity;
  String stockLocationPid;
  String stockLocationName;
  InventoryBatchAllocationJson({
    this.inventoryVoucherDetailId,
    this.productProfilePid,
    this.productProfileName,
    this.batchNumber,
    this.batchDate,
    this.remarks,
    this.quantity,
    this.stockLocationPid,
    this.stockLocationName,
  });
  Map<String, dynamic> toMap(mp) {
    return {
      'inventoryVoucherDetailI': mp.inventoryVoucherDetailId,
      'productProfilePi': mp.productProfilePid,
      'productProfileNam': mp.productProfileName,
      'batchNumbe': mp.batchNumber,
      'batchDat': mp.batchDate,
      'remark': mp.remarks,
      'quantit': mp.quantity,
      'stockLocationPi': mp.stockLocationPid,
      'stockLocationNam': mp.stockLocationName,
    };
  }
}

class InventoryVoucherHeaderJson {
  final String table = 'InventoryVoucherHeaderJson';
  final String query =
      'CREATE TABLE InventoryVoucherHeaderJson(pid STRING,documentNumberLocal STRING,documentNumberServer STRING,documentPid STRING,documentName STRING,documentDate STRING,receiverAccountPid STRING,receiverAccountName STRING,supplierAccountPid STRING,supplierAccountName STRING,employeePid STRING,employeeName STRING,documentTotal DOUBLE,documentVolume DOUBLE,docDiscountPercentage DOUBLE,docDiscountAmount DOUBLE,isNew BOOLEAN,priceLevelPid STRING,priceLevelName STRING,referenceInvoiceNumber STRING,roundedOff DOUBLE,bookingId STRING,bookingDate STRING,bookingDate STRING,deliveryDateDocument STRING,validationDays STRING)';
  String pid;
  String documentNumberLocal;
  String documentNumberServer;
  String documentPid;
  String documentName;
  String documentDate;
  String receiverAccountPid;
  String receiverAccountName;
  String supplierAccountPid;
  String supplierAccountName;
  String employeePid;
  String employeeName;
  double documentTotal;
  double documentVolume;
  double docDiscountPercentage;
  double docDiscountAmount;
  bool isNew;
  String priceLevelPid;
  String priceLevelName;
  String referenceInvoiceNumber;
  double roundedOff;
  String bookingId;
  String bookingDate;
  String deliveryDateDocument;
  String validationDays;
  List<InventoryVoucherDetailsJson> inventoryVoucherDetails;
  InventoryVoucherHeaderJson({
    this.pid,
    this.documentNumberLocal,
    this.documentNumberServer,
    this.documentPid,
    this.documentName,
    this.documentDate,
    this.receiverAccountPid,
    this.receiverAccountName,
    this.supplierAccountPid,
    this.supplierAccountName,
    this.employeePid,
    this.employeeName,
    this.documentTotal,
    this.documentVolume,
    this.docDiscountPercentage,
    this.docDiscountAmount,
    this.isNew,
    this.priceLevelPid,
    this.priceLevelName,
    this.referenceInvoiceNumber,
    this.roundedOff,
    this.bookingId,
    this.bookingDate,
    this.deliveryDateDocument,
    this.validationDays,
    this.inventoryVoucherDetails,
  });

  Map<String, dynamic> toMap(mp) {
    return {
      'pid': mp.id,
      'documentNumberLocal': mp.taskExecutionHeaderId,
      // 'documentNumberServer': mp.documentNumberServer,
      'documentPid': mp.documentPid,
      'documentName': mp.documentName,
      'documentDate': mp.documentDate,
      'receiverAccountPid': mp.receiverAccountPid,
      'receiverAccountName': mp.receiverAccountName,
      'supplierAccountPid': mp.supplierAccountPid,
      'supplierAccountNam': mp.supplierAccountName,
      'employeePid': mp.employeePid,
      'employeeName': mp.employeeName,
      // 'documentTotal': mp.documentTotal,
      'documentVolume': mp.documentVolume,
      // 'docDiscountPercentage': mp.docDiscountPercentage,
      // 'docDiscountAmount': mp.docDiscountAmount,
      'isNew': mp.isNew,
      // 'priceLevelPid': mp.priceLevelPid,
      // 'priceLevelName': mp.priceLevelName,
      'referenceInvoiceNumber': mp.referenceInvoiceNumber,
      'roundedOff': mp.roundedOff,
      // 'bookingId': mp.bookingId,
      'bookingDate': mp.bookingDate,
      // 'deliveryDateDocument': mp.deliveryDateDocument,
      'validationDays': mp.validationDays,
    };
  }

  fromMap(mp, List<InventoryVoucherDetailsJson> list) {
    InventoryVoucherHeaderJson inventoryVoucherHeaderJson =
        InventoryVoucherHeaderJson();
    inventoryVoucherHeaderJson.pid = mp.id;
    // inventoryVoucherHeaderJson.documentNumberLocal = mp.documentNumberLocal;
    //  inventoryVoucherHeaderJson.documentNumberServer = mp.documentNumberServer;
    inventoryVoucherHeaderJson.documentPid = mp.documentPid;
    inventoryVoucherHeaderJson.documentName = mp.documentName;
    inventoryVoucherHeaderJson.documentDate = mp.documentDate;
    inventoryVoucherHeaderJson.receiverAccountPid = mp.receiverAccountPid;
    inventoryVoucherHeaderJson.receiverAccountName = mp.receiverAccountName;
    inventoryVoucherHeaderJson.supplierAccountPid = mp.supplierAccountPid;
    inventoryVoucherHeaderJson.supplierAccountName = mp.supplierAccountName;
    inventoryVoucherHeaderJson.employeePid = mp.employeePid;
    inventoryVoucherHeaderJson.employeeName = mp.employeeName;
    //  inventoryVoucherHeaderJson.documentTotal = mp.documentTotal;
    // inventoryVoucherHeaderJson.documentVolume = mp.documentVolume;
    // inventoryVoucherHeaderJson.docDiscountPercentage = mp.docDiscountPercentage;
    // inventoryVoucherHeaderJson.docDiscountAmount = mp.docDiscountAmount;
    inventoryVoucherHeaderJson.isNew = mp.isNew;
    // inventoryVoucherHeaderJson.priceLevelPid = mp.priceLevelPid;
    // inventoryVoucherHeaderJson.priceLevelName = mp.priceLevelName;
    inventoryVoucherHeaderJson.referenceInvoiceNumber =
        mp.referenceInvoiceNumber;
    inventoryVoucherHeaderJson.roundedOff = mp.roundedOff;
    // inventoryVoucherHeaderJson.bookingId = mp.bookingId;
    inventoryVoucherHeaderJson.bookingDate = mp.bookingDate;
    // inventoryVoucherHeaderJson.deliveryDateDocument = mp.deliveryDateDocument;
    inventoryVoucherHeaderJson.validationDays = mp.validationDays;
    inventoryVoucherHeaderJson.inventoryVoucherDetails = list;
    return inventoryVoucherHeaderJson;
  }
}
