import 'package:salesnrich/Controller/constans.dart';
import 'package:salesnrich/Model/database.dart';

class SharedPrefernce {
  final String table = 'sharedPrefernce';
  final String query =
      'CREATE TABLE sharedPrefernce(default_product_loading_from STRING,inventory_filtering_status BOOLEAN,inventory_prev_next_status BOOLEAN, api_type STRING)';

  String default_product_loading_from;
  bool inventory_filtering_status;
  bool inventory_prev_next_status;
  String api_type;

  SharedPrefernce(
      {this.default_product_loading_from,
      this.inventory_filtering_status,
      this.inventory_prev_next_status,
      this.api_type});

  Map<String, dynamic> toMap(Map<String, dynamic> mp) {
    return {
      PREF_KEY_DEFAULT_PRODUCT_LOADING_FROM:
          mp[PREF_KEY_DEFAULT_PRODUCT_LOADING_FROM],
      PREF_KEY_INVENTORY_FILTERING_STATUS:
          mp[PREF_KEY_INVENTORY_FILTERING_STATUS],
      PREF_KEY_INVENTORY_PREV_NEXT_STATUS:
          mp[PREF_KEY_INVENTORY_PREV_NEXT_STATUS],
      PREF_KEY_API_TYPE: mp[PREF_KEY_API_TYPE],
    };
  }

  SharedPrefernce fromMap(Map mp) {}

  connectDatabase(Map<String, dynamic> mp) async {
    OurDatabase(table: this.table, query: this.query).insertTable(toMap(mp));
  }
}
