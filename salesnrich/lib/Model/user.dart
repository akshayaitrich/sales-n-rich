import 'package:salesnrich/Model/database.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as path;

class UserModel {
  String tokenId;
  String username;
  String userPassword;

  UserModel({this.tokenId, this.username, this.userPassword});

  Map<String, dynamic> toMap() {
    return {
      'tokenId': tokenId,
      'username': username,
      'userPassword': userPassword
    };
  }

  UserModel fromMap(Map mp) {
    return UserModel(
        tokenId: mp['tokenId'],
        username: mp['username'],
        userPassword: mp['userPassword']);
  }
}

class UserDatabase {
  Future<Database> database() async {
    Future<Database> database =
        openDatabase(path.join(await getDatabasesPath(), 'User.db'), version: 1,
            onCreate: (db, version) {
      db.execute(
          'CREATE TABLE USER(tokenId STRING, username STRING, userPassword STRING)');
    });
    return database;
  }

  void insertUser(UserModel user) async {
    Database db = await database();
    db.insert('USER', user.toMap());
  }

  Future<bool> userExists() async {
    final Database db = await database();
    var list = await db.query('USER');
    if (list == null || list.length == 0) {
      return false;
    } else {
      return true;
    }
  }

  Future<UserModel> getUser() async {
    final Database db = await database();
    final value = await db.query('USER');

    UserModel model = UserModel();
    model.tokenId = value[0]['tokenId'];
    model.username = value[0]['username'];
    model.userPassword = value[0]['userPassword'];
    return model;
    //eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzbzFAbWVjIiwiYXV0aCI6IlJPTEVfRVhFQ1VUSVZFIiwiY29tcGFueUlkIjozMDQ2NDMsImV4cCI6MTYxNjMwNTg5OH0.W_JToGBZCNpDH10bKEuHFyXqauEGI1DfoWkrFil63yZtzUMX9H_V7syDnknco0BvIAb7ajHXIuwVJBHIgcPI9g
  }

  Future update(UserModel user) async {
    Database db = await database();
    db.update('USER', user.toMap(),
        where: 'tokenId=?', whereArgs: [user.tokenId]);
  }

  Future delete() async {
    Database db = await database();
    db.delete('USER');
  }
}
