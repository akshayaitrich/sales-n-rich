// ignore_for_file: unused_import, unused_local_variable
import 'dart:convert';
import 'package:airplane_mode_checker/airplane_mode_checker.dart';
import 'package:cell_info/CellResponse.dart';
import 'package:cell_info/SIMInfoResponse.dart';
import 'package:cell_info/cell_info.dart';
import 'package:cell_info/models/common/cell_type.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoding/geocoding.dart';
// import 'package:geolocator/geolocator.dart';
import 'package:salesnrich/Controller/APIs.dart';
import 'package:salesnrich/Controller/constans.dart';
import 'package:salesnrich/Controller/enums.dart';
import 'package:salesnrich/Controller/keygenerator.dart';
import 'package:salesnrich/Controller/preferenceOperation.dart';
import 'package:salesnrich/Controller/staticFields.dart';
import 'package:salesnrich/Model/Tables.dart';
import 'package:salesnrich/Model/database.dart';
import 'package:salesnrich/Model/sharedPreference.dart';
import 'package:salesnrich/ViewModel/DocumentVM.dart';
import 'package:sim_info/sim_info.dart';
import 'package:sizer/sizer.dart';
import '../WebServer.dart';
import 'home_Activity.dart';

class DocumentPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return DocumentPageState();
  }
}

class DocumentPageState extends State<DocumentPage> {
  static ExecutiveActivity excutiveActivity;
  static AccountProfile accountProfile;
  static List<Widget> activityDocuments = [];
  static BuildContext contextt;
  static var outstanding = 0.0;
  static TaskExecution taskExecution;
  static TextEditingController remarks = TextEditingController();
  String location = 'Null, Press Button';
  String Address = 'search';
  static double position = 0.0;
  static String mcc;
  static String mnc;
  static List<int> voucherid;
  var status;
  static CellsResponse cellsResponse1;
  static String currentDBM = "";
  @override
  void initState() {
    initPlatformState();
    inserDocuments();
  }

  @override
  Widget build(BuildContext context) {
    contextt = context;
    return WillPopScope(
        onWillPop: () => onBackPressed(),
        child: Scaffold(
            appBar: StaticFields.getAppbar(tittle: 'Documents'),
            drawer: MenuItems_Widget(),
            body: Container(
                padding: EdgeInsets.all(5.0.w),
                color: Colors.grey[200],
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                        child: Container(
                      padding: EdgeInsets.all(3.0.w),
                      color: Colors.white,
                      child: SingleChildScrollView(child: getWidget()),
                    )),
                    Container(color: Colors.white, child: four()),
                    Row(
                      children: [
                        Expanded(
                            child: FlatButton(
                                onPressed: (() {}),
                                color: Colors.green,
                                child: Text('Save'))),
                        Expanded(
                            child: FlatButton(
                                onPressed: (() {
                                  onSendButtonClick();
                                }),
                                color: Colors.blue[200],
                                child: Text('Send')))
                      ],
                    )
                  ],
                ))));
  }

  void inserDocuments() {
    Document_VM().getDocumetsByActivityPid(excutiveActivity.pid);
  }

  Widget getWidget() {
    return Column(
      children: [
        Row(
          children: [Expanded(child: one()), two()],
        ),
        three(),
      ],
    );
  }

  Widget one() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(excutiveActivity.name,
            style: TextStyle(fontSize: 3.0.h, fontWeight: FontWeight.bold)),
        SizedBox(height: 1.0.h),
        Text(accountProfile.name),
        SizedBox(height: 0.7.h),
        //Text(accountProfile.address),
        Text('${accountProfile.city}, ${accountProfile.location}')
      ],
    );
  }

  Widget two() {
    //var closingBalance = double.parse(accountProfile['closingBalance']);
    return Row(children: [
      Column(children: [
        Text('Outstanding'),
        Text('$outstanding'),
        FlatButton(
            onPressed: (() {}),
            child: Text('Details', style: TextStyle(color: Colors.white)),
            color: Colors.grey[700])
      ]),
      SizedBox(width: 1.0.w),
      Container(
          color: Colors.orange,
          width: 5.50.w,
          height: 10.0.h,
          alignment: Alignment.center,
          child: IconButton(icon: Icon(Icons.arrow_right), onPressed: (() {})))
    ]);
  }

  Widget three() {
    return Column(children: activityDocuments);
  }

  int maxLine = 1;
  Widget four() {
    return TextFormField(
        onChanged: (value) {
          remarks.text = value;
          if (value.length > (35 * maxLine)) {
            setState(() {
              maxLine++;
            });
          }
        },
        maxLines: maxLine,
        decoration: InputDecoration(hintText: 'Type here ....'));
  }

  Future<bool> onBackPressed() async {
    if (InventoryVoucherForm_VM.inventoryVoucherDetailsList.length == 0) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomeActivity()));
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
                titlePadding: EdgeInsets.only(left: 4.0.w, top: 2.0.h),
                title: Text('Exit', style: TextStyle(fontSize: 13.0.sp)),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('NO',
                          style: TextStyle(color: Colors.orange[800]))),
                  TextButton(
                      onPressed: () {
                        InventoryVoucherForm_VM.inventoryVoucherDetailsList
                            .clear();
                        InventoryVoucherFormScreenState.selectedndex.clear();
                        InventoryVoucherFormScreenState.cartlist.clear();
                        InventoryVoucherFormScreenState.noofitemsincart = '';
                        InventoryVoucherFormScreenState.carttotal = 0.0;
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HomeActivity()));
                      },
                      child: Text('YES',
                          style: TextStyle(color: Colors.orange[800]))),
                ],
                content: Text('Are you sure to cancel your order ? ',
                    style: TextStyle(fontSize: 12.0.sp)));
          });
    }
  }

  void onSendButtonClick() async {
    mcc = await SimInfo.getMobileCountryCode;
    mnc = await SimInfo.getMobileNetworkCode;
    status = await AirplaneModeChecker.checkAirplaneMode();
    var configuration =
        await OurDatabase(table: MobileSettingsConfiguration().table)
            .getTables();
    MobileSettingsConfiguration mobileSettingsConfiguration =
        MobileSettingsConfiguration().fromMap(configuration[0]);
    showTaskExecutionSendDialog();

    //  Position position = await getGeoLocationPosition();
    //   location = 'Lat: ${position.latitude} , Long: ${position.longitude}';
    //   final status = await AirplaneModeChecker.checkAirplaneMode();
  }

  void showTaskExecutionSendDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              titlePadding: EdgeInsets.only(left: 4.0.w, top: 2.0.h),
              title: Text('SEND', style: TextStyle(fontSize: 13.0.sp)),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('NO',
                        style: TextStyle(color: Colors.orange[800]))),
                TextButton(
                    onPressed: () {
                      manageSendButtonEvent();
                      saveAndSendTaskExecutionSubmission();
                    },
                    child: Text('YES',
                        style: TextStyle(color: Colors.orange[800]))),
              ],
              content: Text('Are you sure to sent your activity ? ',
                  style: TextStyle(fontSize: 12.0.sp)));
        });
  }

  void manageSendButtonEvent() async {
    bool isDocumentHeader = await checkAnyDocumentOrRemarkExist();
    if (isDocumentTaken()) {
      if (isActivityComplited()) {
        if (isDocumentHeader) {
          if (accountProfile.pid == null) {
            // postNewAccountProfileToServer(accountProfile, "from_send");

            // handleSendSendButton();
          }
        } else {
          ToastHelper().show("No Data to Send");
        }
      }
    }
  }

  void manageSaveAndSendButtonEvent(String status) {
    if (taskExecution == null) {
      Document_VM().generateTaskExecution(null, false);
      onActionSaveSetLocationDetails();
    }
    if (status == "from_send") {}
  }

  Future<void> initPlatformState() async {
    CellsResponse cellsResponse;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      String platformVersion = await CellInfo.getCellInfo;
      final body = json.decode(platformVersion);

      cellsResponse = CellsResponse.fromJson(body);

      CellType currentCellInFirstChip = cellsResponse.primaryCellList[0];
      if (currentCellInFirstChip.type == "LTE") {
        currentDBM =
            "LTE dbm = " + currentCellInFirstChip.lte.signalLTE.dbm.toString();
      } else if (currentCellInFirstChip.type == "NR") {
        currentDBM =
            "NR dbm = " + currentCellInFirstChip.nr.signalNR.dbm.toString();
      } else if (currentCellInFirstChip.type == "WCDMA") {
        currentDBM = "WCDMA dbm = " +
            currentCellInFirstChip.wcdma.signalWCDMA.dbm.toString();

        print('currentDBM = ' + currentDBM);
      }

      String simInfo = await CellInfo.getSIMInfo;
      final simJson = json.decode(simInfo);
      print(
          "desply name ${SIMInfoResponse.fromJson(simJson).simInfoList[0].displayName}");
    } on PlatformException {
      cellsResponse1 = null;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      cellsResponse1 = cellsResponse;
    });
  }

  generateTaskExecutionJson(TaskExecution taskExecution) {
    TaskExecutionJson taskExecutionJson = new TaskExecutionJson();
    taskExecutionJson.pid = taskExecution.serverUniqueId;

    var j = cellsResponse1.neighboringCellList.length;
    var k = cellsResponse1.primaryCellList.length.toString();
    var l = currentDBM;
    if (taskExecution.latitude == null) {
      taskExecutionJson.latitude = position;
    } else {
      taskExecutionJson.latitude = taskExecution.latitude;
    }
    if (taskExecution.longitude == null) {
      taskExecutionJson.longitude = position;
    } else {
      taskExecutionJson.longitude = taskExecution.longitude;
    }

    if (taskExecution.mcc == null) {
      taskExecutionJson.mcc = mcc;
    } else {
      taskExecutionJson.mcc = taskExecution.mcc;
    }

    if (taskExecution.mnc == null) {
      taskExecutionJson.mnc = mnc;
    } else {
      taskExecutionJson.mnc = taskExecution.mnc;
    }
    if (taskExecution.lac == null) {
      taskExecutionJson.lac = "0";
    } else {
      taskExecutionJson.lac = taskExecution.lac;
    }

    if (taskExecution.cellId == null) {
      taskExecutionJson.cellId = k;
    } else {
      taskExecutionJson.cellId = taskExecution.cellId.toString();
    }

    taskExecutionJson.remarks = taskExecution.remarks;
    taskExecutionJson.date = taskExecution.sendDate;

    if (taskExecution.sendVisitDate != null) {
      taskExecutionJson.sendDate = taskExecution.sendVisitDate;
    }

    if (taskExecution.successSendDate != null) {
      taskExecutionJson.punchInDate = taskExecution.successSendDate;
    }

    taskExecutionJson.mockLocationStatus = taskExecution.mockLocationStatus;
    taskExecutionJson.activityPid = taskExecution.executiveActivityPid;
    taskExecutionJson.activityName = taskExecution.executiveActivityName;
    taskExecutionJson.interimSave = taskExecution.interimSave;

    taskExecutionJson.accountTypePid = taskExecution.accountTypePid;
    taskExecutionJson.accountTypeName = taskExecution.accountTypeName;
    taskExecutionJson.accountProfilePid = taskExecution.accountProfilePid;
    taskExecutionJson.accountProfileName = taskExecution.accountProfileName;
    taskExecutionJson.isGpsOff = taskExecution.isGpsOff;
    taskExecutionJson.isMobileDataOff = taskExecution.isMobileDataOff;
    taskExecutionJson.executiveTaskPlanPid = taskExecution.executiveTaskPlanPid;

    taskExecutionJson.clientTransactionKey = taskExecution.clientPrimaryKeyId;

    if (taskExecution.isFlightModeStatus) {
      taskExecutionJson.locationType = LocationType.FlightMode.name;
    } else if (taskExecution.isLocationStatus) {
      taskExecutionJson.locationType = LocationType.GpsLocation.name;
    } else if (taskExecution.isTowerLocationStatus) {
      taskExecutionJson.locationType = LocationType.TowerLocation.name;
    } else {
      taskExecutionJson.locationType = LocationType.NoLocation.name;
    }

    ///////saveActivityDuration//////////
    if (taskExecution.startTimeLocationFindStatus != null) {
      if (taskExecution.startTimeLocationFindStatus > 1) {
        if (taskExecution.startTimeLatitude != null) {
          taskExecutionJson.startLatitude = taskExecution.startTimeLatitude;
        }

        if (taskExecution.startTimeLongitude != null) {
          taskExecutionJson.startLongitude = taskExecution.startTimeLongitude;
        }

        taskExecutionJson.startMcc = taskExecution.startTimeMcc;
        taskExecutionJson.startMnc = taskExecution.startTimeMnc;
        taskExecutionJson.startLac = taskExecution.startTimeLac;
        taskExecutionJson.startCellId = taskExecution.startTimeCellId;
        if (taskExecution.withCustomer != null) {
          taskExecutionJson.withCustomer = taskExecution.withCustomer;
        }

        if (taskExecution.iStartTimeGpsOff != null) {
          taskExecutionJson.startIsGpsOff =
              taskExecution.isStartTimeLocationStatus;
        }

        if (taskExecution.isStartTimeMobileDataOff != null) {
          taskExecutionJson.startIsMobileDataOff =
              taskExecution.isStartTimeMobileDataOff;
        }

        taskExecutionJson.startTime = taskExecution.startTimeDate;
        taskExecutionJson.endTime = taskExecution.endTimeDate;

        if (taskExecution.isStartTimeFlightModeStatus == null) {
          taskExecution.isStartTimeFlightModeStatus = false;
        }

        if (taskExecution.isStartTimeLocationStatus == null) {
          taskExecution.isStartTimeLocationStatus = false;
        }

        if (taskExecution.isStartTimeTowerLocationStatus == null) {
          taskExecution.isStartTimeTowerLocationStatus = false;
        }

        if (taskExecution.isStartTimeFlightModeStatus) {
          taskExecutionJson.startLocationType = LocationType.GpsLocation;
        } else if (taskExecution.isStartTimeLocationStatus) {
          taskExecutionJson.startLocationType = LocationType.GpsLocation;
        } else if (taskExecution.isStartTimeTowerLocationStatus) {
          taskExecutionJson.startLocationType = LocationType.TowerLocation;
        } else {
          taskExecutionJson.startLocationType = LocationType.NoLocation;
        }
      }
    }

    return taskExecutionJson;
  }

  // Future<Position> getGeoLocationPosition() async {
  //   bool serviceEnabled;
  //   LocationPermission permission;
  //   // Test if location services are enabled.
  //   serviceEnabled = await Geolocator.isLocationServiceEnabled();
  //   if (!serviceEnabled) {
  //     // Location services are not enabled don't continue
  //     // accessing the position and request users of the
  //     // App to enable the location services.
  //     await Geolocator.openLocationSettings();
  //     return Future.error('Location services are disabled.');
  //   }
  //   permission = await Geolocator.checkPermission();
  //   if (permission == LocationPermission.denied) {
  //     permission = await Geolocator.requestPermission();
  //     if (permission == LocationPermission.denied) {
  //       // Permissions are denied, next time you could try
  //       // requesting permissions again (this is also where
  //       // Android's shouldShowRequestPermissionRationale
  //       // returned true. According to Android guidelines
  //       // your App should show an explanatory UI now.
  //       return Future.error('Location permissions are denied');
  //     }
  //   }
  //   if (permission == LocationPermission.deniedForever) {
  //     // Permissions are denied forever, handle appropriately.
  //     return Future.error(
  //         'Location permissions are permanently denied, we cannot request permissions.');
  //   }
  //   // When we reach here, permissions are granted and we can
  //   // continue accessing the position of the device.
  //   return await Geolocator.getCurrentPosition(
  //       desiredAccuracy: LocationAccuracy.high);
  // }

  void saveAndSendTaskExecutionSubmission() async {
    generateinventoryvoucherjson();
    TaskExecutionSubmissionJson taskExecutionSubmissionJson =
        TaskExecutionSubmissionJson();
    List<InventoryVoucherHeaderJson> inventoryvoucherheader = [];
    var ls = await OurDatabase(table: InventoryVoucherHeader().table).getTable(
        where: 'taskExecutionHeaderId', whereArgs: taskExecution.id) as List;

    ls.forEach((element) {
      var data =
          InventoryVoucherHeader().fromMap(element as Map<String, dynamic>);
      inventoryvoucherheader.add(InventoryVoucherHeaderJson()
          .fromMap(data, generateinventoryvoucherjson()));
    });

    taskExecutionSubmissionJson.executiveTaskExecutionDTO =
        generateTaskExecutionJson(taskExecution);

    taskExecutionSubmissionJson.inventoryVouchers = inventoryvoucherheader;

    await OurDatabase(
            table: TaskExecutionSubmissionJson().table,
            query: TaskExecutionSubmissionJson().query)
        .insertTable(TaskExecutionSubmissionJson()
            .toMap(taskExecutionSubmissionJson)) as List;
    var response = await WebServer().postSavedTaskExecution(
        url: ServiceGenerator.TASKEXECUTIONPOST,
        data: taskExecutionSubmissionJson);
    if (response.statusCode == 200) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomeActivity()));
    } else {
      Navigator.pop(context);
      return ToastHelper().show('Error while sending data,please try later!!');
    }
  }

  generateinventoryvoucherjson() async {
    List<InventoryVoucherDetailsJson> inventoryVoucherDetailsjsonlist = [];
    InventoryVoucherDetailsJson inventoryVoucherDetailsJson =
        InventoryVoucherDetailsJson();
    var inventoryVoucherDetails =
        await OurDatabase(table: InventoryVoucherDetails().table).getTable(
            where: "inventoryVoucherHeaderId",
            whereArgs:
                InventoryVoucherFormScreenState.inventoryVoucherHeaderId);
    inventoryVoucherDetails.forEach((element) async {
      var element1 = InventoryVoucherDetails().fromMap(element);
      // inventoryVoucherDetailsJson.referenceInventoryVoucherDetailId =
      //     DocumentPageState.voucherid;
      inventoryVoucherDetailsJson.productName = element1.itemName;
      inventoryVoucherDetailsJson.productPid = element1.itemPid;
      inventoryVoucherDetailsJson.quantity = element1.quantity;
      inventoryVoucherDetailsJson.freeQuantity = element1.freeQty;
      var date = new DateTime.now();
      inventoryVoucherDetailsJson.referenceInventoryVoucherHeaderPid =
          InventoryVoucherFormScreenState.inventoryVoucherHeaderId as String;
      inventoryVoucherDetailsJson.bookingDate =
          "${date.year}-${date.month}-${date.day}";
      inventoryVoucherDetailsJson.itemtype = element1.itemtype;
      inventoryVoucherDetailsJson.mrp = element1.mrp;
      inventoryVoucherDetailsJson.sellingRate = element1.sellingRate;
      inventoryVoucherDetailsJson.ratePerPiece = element1.ratePerPiece;
      inventoryVoucherDetailsJson.remarks = element1.remarks;
      inventoryVoucherDetailsJson.taxAmount == element1.totalAmount;
      // inventoryVoucherDetailsJson.inventoryVoucherBatchDetails = [];
      var id = await OurDatabase(
              table: InventoryVoucherDetailsJson().table,
              query: InventoryVoucherDetailsJson().query)
          .insertTable(InventoryVoucherDetailsJson().toMap(inventoryVoucherDetailsJson));
      inventoryVoucherDetailsjsonlist.add(
          InventoryVoucherDetailsJson().fromMap(inventoryVoucherDetailsJson));
    });
    print("yes");
    return inventoryVoucherDetailsjsonlist;
  }

  void generateInventoryBatchAllocationJson() {
    InventoryBatchAllocationJson inventoryBatchAllocationJson =
        InventoryBatchAllocationJson();
    inventoryBatchAllocationJson.inventoryVoucherDetailId;
  }

  void sendTaskExecutionSubmission() {}

  void submitTaskExecutionProcess() {}

  Future<bool> checkAnyDocumentOrRemarkExist() async {
    bool isDocumentHeader = false;

    if (taskExecution != null) {
      isDocumentHeader = await checkIsDocumentHeader(taskExecution.id);
      if (isDocumentHeader) {
        isDocumentHeader = true;
      } else if (getDocumentRemarks() != null) {
        if (getDocumentRemarks().length > 1) {
          isDocumentHeader = true;
        }
      } else {
        isDocumentHeader = false;
      }
    } else {}
    return isDocumentHeader;
  }

  bool isDocumentTaken() {
    return false;
  }

  bool isActivityComplited() {
    bool activityCompleted = false;

    if (excutiveActivity.completePlans != null) {
      if (excutiveActivity.completePlans) {
      } else {
        return true;
      }
    }

    return true;
  }

  void handleSaveSendButton() {}

  void onActionSaveSetLocationDetails() async {
    taskExecution.latitude = position;
    taskExecution.longitude = position;
    taskExecution.mcc = mcc;
    taskExecution.mnc = mnc;
    taskExecution.lac = '0';
    taskExecution.cellId = cellsResponse1.primaryCellList.length.toString();
    ;
    taskExecution.isFlightModeStatus = status;
    taskExecution.isLocationStatus = position != 0.0 ? true : false;
    taskExecution.isTowerLocationStatus = false;

    // taskExecution.mockLocationStatus=Utils.isMockLocationOn(getContext());
    // taskExecution.setIsGpsOff(GpsNetworkUtils.isGpsEnabled(getActivity()));
    // taskExecution.setIsMobileDataOff(GpsNetworkUtils.isNetworkEnabled(getActivity()));

    taskExecution.remarks = getDocumentRemarks();
    //   taskExecution.setAccountProfileToServerReason(getAccountProfileToServerReasonText());
    //   taskExecution.accountProfilePid=accountProfile.getPid();

    var taskExecutionId = taskExecution.id;

    //   taskExecution.isDocumentDetails=checkIsDocumentHeader(taskExecutionId));

    //   taskExecution.setEndTimeDate(DateUtils.getCurrentDate(Constant.DATE_FORMAT));
    //   taskExecution.setEndTimeTime(DateUtils.getCurrentDate("dd-MM-yyyy"));

    //   /// start Location details set start ...
    bool customerLocationSpendTimeRequiredStatus;
    //   if (customerLocationSpendTimeRequiredStatus) {
    //       if (getExecutiveCustomerLocationSpentTimeDetails() != null) {
    //           if (accountProfile.getPid().equalsIgnoreCase(getExecutiveCustomerLocationSpentTimeDetails().
    //                   getAccountProfilePid()) &&
    //                   (executiveActivity.getPid().
    //                           equalsIgnoreCase(getExecutiveCustomerLocationSpentTimeDetails().
    //                                   getActivityPid()))) {
    //               taskExecution.setStartTimeLatitude(executiveCustomerLocationSpentTimeDetails.
    //                       getStartTimeLatitude());
    //               taskExecution.setStartTimeLongitude(executiveCustomerLocationSpentTimeDetails.
    //                       getStartTimeLongitude());
    //               taskExecution.setStartTimeMcc(executiveCustomerLocationSpentTimeDetails.
    //                       getStartTimeMcc());
    //               taskExecution.setStartTimeMnc(executiveCustomerLocationSpentTimeDetails.
    //                       getStartTimeMnc());
    //               taskExecution.setStartTimeCellId(executiveCustomerLocationSpentTimeDetails.
    //                       getStartTimeCellId());
    //               taskExecution.setStartTimeLac(executiveCustomerLocationSpentTimeDetails.
    //                       getStartTimeLac());
    //               taskExecution.setIsStartTimeLocationStatus(executiveCustomerLocationSpentTimeDetails.
    //                       getStartTimeLocationStatus());
    //               taskExecution.setIsStartTimeTowerLocationStatus(executiveCustomerLocationSpentTimeDetails.
    //                       getStartTimeTowerLocationStatus());
    //               taskExecution.setIsStartTimeFlightModeStatus(executiveCustomerLocationSpentTimeDetails.
    //                       getStartTimeFlightModeStatus());
    //               taskExecution.setIStartTimeGpsOff(executiveCustomerLocationSpentTimeDetails.
    //                       getiStartTimeGpsOff());
    //               taskExecution.setIsStartTimeMobileDataOff(executiveCustomerLocationSpentTimeDetails.
    //                       getStartTimeMobileDataOff());
    //               taskExecution.setStartTimeLocationFindStatus(executiveCustomerLocationSpentTimeDetails.
    //                       getStartTimeLocationFindStatus());
    //               taskExecution.setStartTimeDate(executiveCustomerLocationSpentTimeDetails.
    //                       getStartTimeDate());
    //               taskExecution.setStartTimeTime(executiveCustomerLocationSpentTimeDetails.
    //                       getStartTimeTime());
    //           }

    //       } else {
    //           taskExecution.setStartTimeLocationFindStatus(3);
    //       }
    //   } else {
    //       taskExecution.setStartTimeLocationFindStatus(1);
    //   }
    //   //start location details end ...///

    //   dataProcessController.getDatabase().updateTaskExecution(taskExecution);

    //   if (fromWhich.equalsIgnoreCase("from_save")) {
    //       saveTaskExecutionSubmission();
    //   } else if (fromWhich.equalsIgnoreCase("from_saveAndSend")) {
    //       saveAndSendTaskExecutionSubmission();
    //   } else {
    //       // sendSms(taskExecutionSubmissionJson);
  }

  Future<bool> checkIsDocumentHeader(id) async {
    bool isDocumentHeader = false;

    // List<VoucherHeader> voucherHeaderList =
    //     await OurDatabase(table: VoucherHeader().table)
    //         .getTable(where: 'taskExecutionId', whereArgs: id);

    var inventoryHeaderList =
        await OurDatabase(table: InventoryVoucherHeader().table)
            .getTable(where: 'taskExecutionHeaderId', whereArgs: id);
    // List<TaskExecutionDynamicDocumentHeader>
    //     taskExecutionDynamicDocumentHeaderList = dataProcessController
    //         .getDatabase()
    //         .getTaskExecutionDynamicDocumentHeaderListByTaskExecutionHeaderId(
    //             taskExecutionId);

    // if (voucherHeaderList.length > 0) {
    //   return isDocumentHeader = true;
    // } else
    if (inventoryHeaderList.length > 0) {
      return isDocumentHeader = true;
      // } else if (taskExecutionDynamicDocumentHeaderList.size() > 0) {
      //   return isDocumentHeader = true;
    } else {
      return isDocumentHeader = false;
    }
  }

  String getDocumentRemarks() {
    String documentRemarks = "";

    if (remarks.text == null || remarks.text == "") {
      documentRemarks = "";
    } else {
      documentRemarks = remarks.text;
    }

    return documentRemarks;
  }
}

class InventoryVoucherFormScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return InventoryVoucherFormScreenState();
  }
}

class InventoryVoucherFormScreenState
    extends State<InventoryVoucherFormScreen> {
  TextEditingController reciver_controller, supplier_controller;
  static TaskExecution taskExecution;
  static String accountPid;
  static List receiver = [""];
  static String recie;
  static List supplier = [""];
  static String suppli;
  static double price_with_tax = 0.0;
  static List<Widget> productsWidget = [];
  static BuildContext contextt;
  static Document document;
  static List selectedndex = [];
  String tittle;
  bool scroll = true;
  List l1 = [];
  static var carttotal;
  static List dynamicFormulalist = [];
  bool save_visile = false;
  static bool inventoryFilteringStatus;
  static bool bt_taken_order_save;
  static bool prevNextStatus;
  static bool defaultLoadingFrom;
  static String noofitemsincart;

  List dynamicFormulaList;
  var guidedSellingConfiguration;
  var favouriteProductGroupPid;
  bool isFavouriteCompulsary;
  var promoteProductGroupPid;
  bool isPromoteCompulsary;
  static int inventoryVoucherHeaderId;
  static List<Widget> cartlist = [];
  static Future<int> taskExecutionHeaderId;
  @override
  void initState() {
    // inventoryVoucherDetailsList.clear();
    doTask();
    reciver_controller = TextEditingController();
    supplier_controller = TextEditingController();
    InventoryVoucherForm_VM().getGuidedSellingConfiguration();
    InventoryVoucherForm_VM().getReceiverSupplierAccountTypes();
    tittle = (Document_VM.taskExecution as TaskExecution).executiveActivityName;
    InventoryVoucherForm_VM().getProductProfile();
    InventoryVoucherForm_VM().getDynamicFormulaByDocumentPid();
    carttotal = 0.0;
    if (document.save == 1) {
      setState(() {
        save_visile = true;
      });
    } else {
      setState(() {
        save_visile = false;
      });
    }
    recie = receiver[0];
    suppli = supplier[0];
  }

  @override
  Widget build(BuildContext context) {
    contextt = context;
    return WillPopScope(
        onWillPop: null,
        child: Scaffold(
            appBar: AppBar(
                actions: [
                  Builder(
                    builder: (context) => cartlist.isNotEmpty
                        ? IconButton(
                            icon: Icon(Icons.shopping_cart),
                            onPressed: () {
                              carttotal = 0.0;
                              if (Inventory.totalpriceslist.length > 1) {
                                for (var i = 0;
                                    i < Inventory.totalpriceslist.length;
                                    i++) {
                                  carttotal += Inventory.totalpriceslist[i];
                                  Scaffold.of(context).openEndDrawer();
                                  setState(() {});
                                }
                              } else if (Inventory.totalpriceslist.length ==
                                  1) {
                                carttotal = Inventory.totalpriceslist[0];
                                Scaffold.of(context).openEndDrawer();
                                setState(() {});
                              } else if (Inventory.totalpriceslist.isEmpty) {
                                carttotal = 0.0;
                              }
                            },
                            tooltip: MaterialLocalizations.of(context)
                                .openAppDrawerTooltip,
                          )
                        : Container(),
                  ),
                ],
                title: Row(children: [
                  Image(
                      height: 10.0.h,
                      width: 10.0.w,
                      image: AssetImage('assets/salesnrichWhite.png')),
                  Text(tittle)
                ])),
            drawer: MenuItems_Widget(),
            endDrawer: SafeArea(
              child: Drawer(
                child: cartlist.isNotEmpty ? endDrawer() : null,
              ),
            ),
            body: Container(
              padding: EdgeInsets.all(2.0.w),
              child: Column(children: [
                Visibility(child: section1(), visible: scroll),
                Expanded(
                    child: Container(
                        child: section2(),
                        padding: EdgeInsets.all(2.0.w),
                        decoration: BoxDecoration(
                            border:
                                Border.all(color: Colors.grey, width: 0.05.w))))
              ]),
            ),
            floatingActionButton: FloatingActionButton(
                child: Icon(Icons.file_present),
                backgroundColor: Colors.orange,
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                            insetPadding: EdgeInsets.zero,
                            contentPadding: EdgeInsets.zero,
                            clipBehavior: Clip.antiAliasWithSaveLayer,
                            titlePadding: EdgeInsets.only(left: 0),
                            title: Container(
                              height: 50,
                              child: Padding(
                                padding: EdgeInsets.only(top: 14, left: 5.0.w),
                                child: Text('DOCUMENT LIST',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12.0.sp)),
                              ),
                              color: Colors.blue,
                            ),
                            content: Container(
                              child: Column(),
                              width: 50.0.h,
                            ));
                      });
                })));
  }

  Widget section1() {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text('Doc Name : $tittle'),
      SizedBox(height: 1.0.h),
      Text(
          'Doc No. : ${(Document_VM.taskExecution as TaskExecution).clientPrimaryKeyId}'),
      SizedBox(height: 1.0.h),
      Container(
          color: Colors.grey[300],
          child: Row(
            children: [
              Expanded(
                child: TextField(
                  decoration: InputDecoration(hintText: recie),
                  enabled: false,
                  controller: reciver_controller,
                ),
              ),
              PopupMenuButton(
                icon: Icon(Icons.keyboard_arrow_down_rounded),
                color: Colors.white,
                onSelected: ((value) {
                  reciver_controller.text = value;
                  recie = value;
                  setState(() {});
                }),
                itemBuilder: (BuildContext context) {
                  return receiver.map((e) {
                    return PopupMenuItem(
                      value: e,
                      child: Text(e),
                    );
                  }).toList();
                },
              )
            ],
          )),
      SizedBox(height: 1.0.h),
      Container(
          color: Colors.grey[300],
          child: Row(
            children: [
              Expanded(
                child: TextField(
                  decoration: InputDecoration(hintText: suppli),
                  enabled: false,
                  controller: supplier_controller,
                ),
              ),
              PopupMenuButton(
                icon: Icon(Icons.keyboard_arrow_down_rounded),
                color: Colors.white,
                onSelected: ((value) {
                  supplier_controller.text = value;
                  suppli = value;
                  setState(() {});
                }),
                itemBuilder: (BuildContext context) {
                  return supplier.map((e) {
                    return PopupMenuItem(
                      value: e,
                      child: Text(e),
                    );
                  }).toList();
                },
              )
            ],
          )),
      SizedBox(height: 1.0.h),
      Text('Price Level : Piece Rate', style: TextStyle(fontSize: 12.0.sp))
    ]);
  }

  Widget section2() {
    return Column(children: [
      Row(children: [
        Expanded(child: TextField(
          onChanged: (value) {
            InventoryVoucherForm_VM().search(value);
          },
        )),
        Container(
            color: Colors.orange,
            child: IconButton(
                icon: Image(
                    image:
                        AssetImage('assets/ic_filter_outline_white_24dp.png')),
                onPressed: (() {}))),
        SizedBox(width: 0.5.w),
        Container(
            color: Colors.grey,
            child: IconButton(
                icon: Image(
                    image: scroll == true
                        ? AssetImage('assets/ic_arrow_upward_white_24dp.png')
                        : AssetImage(
                            'assets/ic_arrow_downword_white_24dp.png')),
                color: Colors.white,
                onPressed: (() {
                  scroll == true ? scroll = false : scroll = true;
                  setState(() {});
                })))
      ]),
      SizedBox(width: 1.0.h),
      Expanded(
          child: Container(
              child: ListView.builder(
                  itemCount: productsWidget.length,
                  itemBuilder: (context, index) {
                    // InventoryVoucherFormScreenState
                    //     .inventoryVoucherDetailsList.length = index;
                    return Card(child: productsWidget[index]);
                  })))
    ]);
  }

  Widget endDrawer() {
    return Container(
        width: 160,
        color: Colors.white,
        child:
            Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          Container(
            padding: EdgeInsets.all(2.0.h),
            alignment: Alignment.center,
            child: Text('Order',
                style: TextStyle(color: Colors.white, fontSize: 12.0.sp)),
            color: Colors.orange,
          ),
          Expanded(
              child: Visibility(
                  visible: true,
                  child: ListView.separated(
                      separatorBuilder: (context, index) => Divider(
                            color: Colors.black38,
                          ),
                      itemCount: cartlist.length,
                      itemBuilder: (context, index) {
                        noofitemsincart = cartlist.length.toString();

                        return ListTile(title: cartlist[index], onTap: (() {}));
                      }))),
          Container(
            child:
                Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
              Text(
                'Total :  $carttotal (${cartlist.length})',
                style: TextStyle(fontSize: 11.0.sp, color: Colors.black),
              ),
              GestureDetector(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          titlePadding: EdgeInsets.only(left: 0),
                          title: Container(
                              padding: EdgeInsetsDirectional.all(1.0.h),
                              child: Center(
                                child: Text('Discount for your orders',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 12.0.sp)),
                              ),
                              color: Colors.orangeAccent[700]),
                          content: Container(
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                        flex: 4,
                                        child: TextFormField(
                                            decoration: InputDecoration(
                                                hintText: "Discount(%)"))),
                                    Expanded(
                                        flex: 1,
                                        child: SizedBox(
                                          width: 10.0.w,
                                        )),
                                    Expanded(
                                        flex: 4,
                                        child: TextFormField(
                                            decoration: InputDecoration(
                                                hintText: "Discount(Rs)"))),
                                  ],
                                ),
                              ],
                            ),
                            height: 25.0.h,
                            width: 195.0.w,
                          ),
                        );
                      });
                },
                child: Text(
                  '   Discount & Coupons',
                  style: TextStyle(fontSize: 11.0.sp, color: Colors.black),
                ),
              ),
            ]),
            color: Colors.grey[300],
            padding: EdgeInsets.only(top: 1.0.h, bottom: 1.0.h, left: 1.0.w),
          ),
          Container(
              child: TextButton(
                  onPressed: (() {
                    generateInventoryvoucherheaderdetails();

                    FocusScopeNode currentFocus = FocusScope.of(context);
                    DocumentPageState.taskExecution = taskExecution;

                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DocumentPage()));
                  }),
                  child: Text('Save',
                      style:
                          TextStyle(color: Colors.white, fontSize: 12.0.sp))),
              color: Colors.blue)
        ]));
  }

  generateInventoryvoucherheaderdetails() async {
    // var date = new DateTime.now();
    // var date1 =
    //     "${date.year}-0${date.month}-0${date.day}T${date.hour}:${date.minute}:${date.second}.${date.millisecond}${date.microsecond}";
    InventoryVoucherHeader voucherDetailheader = InventoryVoucherHeader();
    // voucherDetailheader.taskExecutionHeaderId = taskExecution.id;
    voucherDetailheader.documentPid =
        InventoryVoucherFormScreenState.document.pid;
    voucherDetailheader.documentName =
        InventoryVoucherFormScreenState.document.name;
    voucherDetailheader.documentDate = Document_VM.date1;
    voucherDetailheader.numberOfProducts =
        double.parse(InventoryVoucherFormScreenState.noofitemsincart);
    voucherDetailheader.totalAmount = carttotal;
    InventoryVoucherForm_VM.supplierAccountProfileList.forEach((element) {
      if (element.name == InventoryVoucherFormScreenState.suppli) {
        voucherDetailheader.supplierAccountPid = element.pid;
      }
    });
    InventoryVoucherForm_VM.receiverAccountProfileList.forEach((element) {
      if (element.name == InventoryVoucherFormScreenState.recie) {
        voucherDetailheader.receiverAccountPid = element.pid;
      }
    });
    voucherDetailheader.supplierAccountName =
        InventoryVoucherFormScreenState.suppli;
    // voucherDetailheader.taskExecutionHeaderId =taskExecutionHeaderId;
    voucherDetailheader.receiverAccountName =
        InventoryVoucherFormScreenState.recie;
    voucherDetailheader.referenceInvoiceNumber = Document_VM.invoiceNo;
    var id = await OurDatabase(
            table: InventoryVoucherHeader().table,
            query: InventoryVoucherHeader().query)
        .insertTable(InventoryVoucherHeader().toMap(voucherDetailheader));

    selectedndex.forEach((element) async {
//object + id
      InventoryVoucherForm_VM
          .inventoryVoucherDetailsList[element].inventoryVoucherHeaderId = id;
      inventoryVoucherHeaderId = id;
      var inventoryVoucherDetailsListdata = InventoryVoucherDetails()
          .toMap(InventoryVoucherForm_VM.inventoryVoucherDetailsList[element]);
      var voucherid = await OurDatabase(
              table: InventoryVoucherDetails().table,
              query: InventoryVoucherDetails().query)
          .insertTable(inventoryVoucherDetailsListdata);
      DocumentPageState.voucherid.add( voucherid) ;
    });
  }

  void doTask() async {
    var preference =
        await OurDatabase(table: SharedPrefernce().table).getTables();
    if (document.save == 1) {
      bt_taken_order_save = true;
    } else {
      bt_taken_order_save = false;
    }

    //// data loading status ....start ////

    inventoryFilteringStatus = await Preference().getInventoryFilteringStatus();

    prevNextStatus = await Preference().getInventoryPrevNextStatus();

    defaultLoadingFrom = await Preference().getDefaulproductloadingfrom();
    //// data loading status ....end ////

    var data = await OurDatabase(table: DynamicFormula().table)
        .getTable(where: "documentPid", whereArgs: document.pid);
    if (data != null) {
      dynamicFormulaList = data;
    }
    if (await OurDatabase(table: GuidedSellingConfiguration().table)
        .databaseExists()) {
      guidedSellingConfiguration =
          await OurDatabase(table: GuidedSellingConfiguration().table)
              .getTables();
    }

    if (guidedSellingConfiguration != null) {
      if (guidedSellingConfiguration['guidedSellingFilterItems'] != null) {
        inventoryFilteringStatus =
            guidedSellingConfiguration['guidedSellingFilterItems'];
        if (inventoryFilteringStatus) {
          if (guidedSellingConfiguration['favouriteProductGroupPid'] != null) {
            favouriteProductGroupPid =
                guidedSellingConfiguration['favouriteProductGroupPid'];

            if (guidedSellingConfiguration['favouriteItemCompulsory'] == 1) {
              isFavouriteCompulsary = true;
            } else {
              isFavouriteCompulsary = false;
            }
          }

          if (guidedSellingConfiguration['promotionProductGroupPid'] != null) {
            promoteProductGroupPid =
                guidedSellingConfiguration['promotionProductGroupPid'];
            if (guidedSellingConfiguration['promotionItemCompulsory'] == 1) {
              isPromoteCompulsary = true;
            } else {
              isPromoteCompulsary = false;
            }
          }
          loadFavouriteGroupProductProfiles();
        }
      } else {
        inventoryFilteringStatus = false;
      }
    } else {
      inventoryFilteringStatus = false;
    }
  }

  void loadFavouriteGroupProductProfiles() {
    // List<AccountPurchaseHistory> accountPurchaseHistoryList = dataProcessController.
    //         getDatabase().
    //         getAccountPurchaseHistoryByAccountPid(taskExecution.
    //                 getAccountProfilePid());

    // List<ProductProfile> productProfileList = new ArrayList<>();
    // for (AccountPurchaseHistory accountPurchaseHistory : accountPurchaseHistoryList) {
    //     ProductProfile productProfile = dataProcessController.
    //             getDatabase().
    //             getProductProfileByPid(accountPurchaseHistory.getProductPid());
    //     if (productProfile != null) {
    //         productProfileList.add(productProfile);
    //     }
    // }

    // Collections.sort(productProfileList, new ProductNameComparator());

    // favouriteProductProfileList = productProfileList;//14-1-17

    // favouriteProductCount = favouriteProductProfileList.size();

    // loadPromoteGroupProductProfiles();
  }
}

class AccountingVoucherFormScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AccountingVoucherFormScreenState();
  }
}

class AccountingVoucherFormScreenState
    extends State<AccountingVoucherFormScreen> {
  static BuildContext contextt;
  DateTime _dateTime;
  String tittle;
  static String header = "";
  static List modeList = [];
  String str;
  String finalDate = '';
  List<Widget> cartlist = [];
  static TaskExecution taskExecution;
  static var accountPid;
  static Document document;

  TextEditingController cashcheqrtgscontroller,
      amount_controller,
      receiptNumber_controller,
      bankName_controller,
      chequeNumber_controller,
      remarks_controller;

  getCurrentDate() {
    var date = new DateTime.now().toString();

    var dateParse = DateTime.parse(date);

    var formattedDate = "${dateParse.day}-${dateParse.month}-${dateParse.year}";

    setState(() {
      finalDate = formattedDate.toString();
    });
  }

  @override
  void initState() {
    AccountingvoucherForm_VM().setDrawrHeader();
    AccountingvoucherForm_VM().initializeUi();
    cashcheqrtgscontroller = TextEditingController();
    amount_controller = TextEditingController();
    receiptNumber_controller = TextEditingController();
    bankName_controller = TextEditingController();
    chequeNumber_controller = TextEditingController();
    remarks_controller = TextEditingController();

    // str = modeList[0];
    tittle = "Doc Name : ${document.name}";
    getCurrentDate();
    //cashcheqrtgscontroller = str as TextEditingController;
    setState(() {});
    super.initState();
  }

  bool banknamevisible = true, cheqnovisible = true, datevisible = true;
  bool bank = false, cheq = false, date = false;
  showdate() {
    showDatePicker(
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(2001),
            lastDate: DateTime(2022))
        .then((date) {
      setState(() {
        _dateTime = date;
      });
    });
  }

  check1() {
    if (str == modeList[1] && str == modeList[2]) {
      banknamevisible = true;
      cheqnovisible = true;
      datevisible = true;
    } else {
      banknamevisible = false;
      cheqnovisible = false;
      datevisible = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    contextt = context;
    return Scaffold(
        endDrawer: SafeArea(child: Drawer(child: endDrawer())),
        appBar: AppBar(
            title: Row(children: [
          Image(
              height: 10.0.h,
              width: 10.0.w,
              image: AssetImage('assets/salesnrichWhite.png')),
          Text(tittle)
        ])),
        body: Container(
          padding: EdgeInsets.all(2.0.w),
          child: Column(
            children: [
              Container(
                color: Colors.grey[300],
                padding: EdgeInsets.all(2.0.w),
                child: sec1(),
              ),
              SizedBox(
                height: 30,
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.all(2.0.w),
                  child: sec2(),
                ),
              ),
              Container(child: sec3())
            ],
          ),
        ));
  }

  Widget sec1() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Doc Name :'),
        SizedBox(height: 1.0.h),
        Text('Doc No :'),
        SizedBox(height: 1.0.h),
        Container(
          color: Colors.white38,
          child: DropdownButton(
            isExpanded: true,
            underline: SizedBox(),
            onTap: (() {}),
          ),
        ),
        //SizedBox(height: 1.0.h),
        SizedBox(height: 40),
        Container(
            color: Colors.white38,
            child: DropdownButton(
              isExpanded: true,
              underline: SizedBox(),
              onTap: (() {}),
            )),
        SizedBox(
          height: 40,
        ),
      ],
    );
  }

  Widget sec2() {
    return Column(
      children: [
        //Row(
        // children: [
        Stack(children: [
          Expanded(
              child: TextField(
            decoration: InputDecoration(hintText: str),
            controller: cashcheqrtgscontroller,
          )),
          //Spacer(),
          Container(
              padding: EdgeInsets.only(left: 290),
              child: DropdownButton(
                  dropdownColor: Colors.orange,
                  underline: SizedBox(),
                  items: modeList
                      .map((e) =>
                          DropdownMenuItem(value: ('$e'), child: Text('$e')))
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      str = value;
                    });
                  }))
        ]),
        Row(children: [
          Expanded(
              child: TextField(
            controller: amount_controller,
            decoration: InputDecoration(hintText: 'Amount'),
          )),
          Icon(Icons.money_off_rounded)
        ]),
        Expanded(
            child: TextField(
          controller: receiptNumber_controller,
          decoration: InputDecoration(hintText: 'Provisional Receipt No.'),
        )),
        Visibility(
            visible: str == modeList[1]
                ? true
                : false || str == modeList[2]
                    ? true
                    : false,
            child: Expanded(
                child: TextField(
              controller: bankName_controller,
              decoration: InputDecoration(hintText: 'Bank Name'),
            ))),
        Visibility(
            visible: str == modeList[1]
                ? true
                : false || str == modeList[2]
                    ? true
                    : false,
            child: Expanded(
                child: TextField(
              controller: chequeNumber_controller,
              decoration: InputDecoration(hintText: 'Cheque No.'),
            ))),
        Visibility(
            visible: str == modeList[1]
                ? true
                : false || str == modeList[2]
                    ? true
                    : false,
            child: Stack(children: [
              Expanded(
                child: TextField(),
              ),
              Row(
                children: [
                  Text(_dateTime == null
                      ? finalDate
                      : _dateTime.day.toString() + '-'),
                  Text(_dateTime == null
                      ? ''
                      : _dateTime.month.toString() + '-'),
                  Text(_dateTime == null ? '' : _dateTime.year.toString())
                ],
              ),
              Container(
                  padding: EdgeInsets.only(left: 330),
                  child: IconButton(
                      onPressed: () {
                        showdate();
                      },
                      icon: Icon(Icons.date_range_outlined)))
            ])),
        Expanded(
            child: TextField(
          controller: remarks_controller,
          decoration: InputDecoration(hintText: 'Remarks'),
        ))
      ],
    );
  }

  Widget sec3() {
    return Row(
      children: [
        Expanded(
          child: FlatButton(
            onPressed: () {},
            child: Text('ADD'),
            color: Colors.blue,
          ),
        ),
        FlatButton(
          onPressed: () {},
          child: Icon(Icons.photo_album),
          color: Colors.blue[900],
        ),
        FlatButton(
          onPressed: () {},
          child: Icon(Icons.camera_alt),
          color: Colors.orange,
        ),
      ],
    );
  }

  Widget endDrawer() {
    return Container(
        color: Colors.white,
        child:
            Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          Container(
            padding: EdgeInsets.all(2.0.h),
            alignment: Alignment.center,
            child: Text(header, style: TextStyle(color: Colors.white)),
            color: Colors.orange,
          ),
          Expanded(
              child: Visibility(
                  visible: false,
                  child: ListView.separated(
                      separatorBuilder: (context, index) => Divider(
                            color: Colors.black38,
                          ),
                      itemCount: cartlist.length,
                      itemBuilder: (context, index) =>
                          (ListTile(title: cartlist[index], onTap: (() {})))))),
          // Container(
          //   child:Row(children: [
          //     Text('data',style :TextStyle(color: Colors.red) )
          //   ],),
          //   color: Colors.blue[300],
          //   padding: EdgeInsets.all(2.0.h),
          // ),
          Container(
              child: TextButton(
                  onPressed: (() {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => HomeActivity()));
                  }),
                  child: Text('Save', style: TextStyle(color: Colors.white))),
              color: Colors.blue)
        ]));
  }

  getCAshCheckRtgs() {
    return cashcheqrtgscontroller.text;
  }

  getAmount() {
    double amount = 0;
    try {
      amount = double.parse(amount_controller.text);
    } catch (e) {
      amount = 0;
    }
    return amount;
  }

  geProvisonalReceiptNumber() {
    return receiptNumber_controller.text;
  }

  getRemarks() {
    return remarks_controller.text;
  }
}
