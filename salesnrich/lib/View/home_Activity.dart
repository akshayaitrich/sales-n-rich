// ignore_for_file: missing_return

import 'dart:io';
import 'dart:ui';
import 'package:f_logs/model/flog/flog.dart';
import 'package:flutter/material.dart';
// import 'package:geolocator/geolocator.dart';
import 'package:lottie/lottie.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:salesnrich/Controller/preferenceOperation.dart';
import 'package:salesnrich/Controller/staticFields.dart';
import 'package:salesnrich/Model/Tables.dart';
import 'package:salesnrich/Model/database.dart';
import 'package:salesnrich/View/document.dart';
import 'package:salesnrich/ViewModel/HomeActivityVM.dart';
import 'package:sizer/sizer.dart';

class HomeActivity extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeActivityState();
  }
}

class HomeActivityState extends State<HomeActivity> {
  static String title = 'welcome';
  //static BuildContext contextt;
  BuildContext contextt;
  Color orenge = Colors.orange;
  bool As = true;
  bool pr = false;
  bool ps = false;
  bool v = true;
  static Widget page = Column(children: [
    Expanded(child: Text('88')),
  ]);
  @override
  void initState() {
   // getpostion();
    // getFirst();
  }

  // getpostion() async {
  //   DocumentPageState.position = await Geolocator.getCurrentPosition(
  //       desiredAccuracy: LocationAccuracy.high);
  // }

  @override
  Widget build(BuildContext context) {
    contextt = context;
    return WillPopScope(
        // ignore: missing_return
        onWillPop: (() {
          //exitAlert();
        }),
        child: Scaffold(
            appBar: AppBar(
                title: Row(children: [
              Image(
                  height: 10.0.h,
                  width: 10.0.w,
                  image: AssetImage('assets/salesnrichWhite.png')),
              Text(title)
            ])),
            drawer: MenuItems_Widget(),
            body: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(children: [
                    RaisedButton(
                        color: As ? orenge : null,
                        onPressed: () {
                          _ChangeState().getFirst();

                          setState(() {
                            As = true;
                            pr = false;
                            ps = false;
                          });
                        },
                        child: Text("Account Summary",
                            style: TextStyle(fontSize: 12.0.sp))),
                    RaisedButton(
                        color: pr ? orenge : null,
                        onPressed: () {
                          _ChangeState().getTwo();

                          setState(() {
                            pr = true;
                            As = false;
                            ps = false;
                          });
                        },
                        child: Text("Performance",
                            style: TextStyle(fontSize: 12.0.sp))),
                    RaisedButton(
                        color: ps ? orenge : null,
                        onPressed: () {
                          _ChangeState().getThird();

                          setState(() {
                            ps = true;
                            pr = false;
                            As = false;
                          });
                        },
                        child: Text("Payment Summary",
                            style: TextStyle(fontSize: 12.0.sp)))
                  ]),
                ),
                SizedBox(
                  height: 45.0.h,
                  child: Expanded(
                    child: Change(),
                    flex: 6,
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Container(
                    margin: EdgeInsets.only(left: 9.0.w, right: 9.0.w),
                    color: Colors.white,
                    height: 315,
                    child: ListView(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                  width: 0.10.w, color: Colors.black),
                            ),
                          ),
                          child: SizedBox(
                            child: Container(
                                height: 75,
                                child: Center(
                                    child: Text(
                                  "SALES ORDER",
                                  style: TextStyle(fontSize: 12.0.sp),
                                ))),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                  width: 0.10.w, color: Colors.black),
                            ),
                          ),
                          child: SizedBox(
                              height: 75,
                              child: Center(
                                  child: Text(
                                "RECEIPT",
                                style: TextStyle(fontSize: 12.0.sp),
                              ))),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                  width: 0.10.w, color: Colors.black),
                            ),
                          ),
                          child: SizedBox(
                              height: 75,
                              child: Center(
                                  child: Text(
                                "PURCHASE",
                                style: TextStyle(fontSize: 12.0.sp),
                              ))),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                  width: 0.10.w, color: Colors.black),
                            ),
                          ),
                          child: SizedBox(
                              height: 75,
                              child: Center(
                                  child: Text(
                                "VAN SALES",
                                style: TextStyle(fontSize: 12.0.sp),
                              ))),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                  width: 0.10.w, color: Colors.black),
                            ),
                          ),
                          child: SizedBox(
                              height: 75,
                              child: Center(
                                  child: Text(
                                "VANSALES RETURN",
                                style: TextStyle(fontSize: 12.0.sp),
                              ))),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(flex: 1, child: BottomButtons())
              ],
            )));
  }
}

class Change extends StatefulWidget {
  @override
  _ChangeState createState() => _ChangeState();
}

class _ChangeState extends State<Change> {
  static Widget _widget = Container(color: Colors.green);
  static BuildContext contextt;

  @override
  void initState() {
    getFirst();
  }

  @override
  Widget build(BuildContext context) {
    contextt = context;
    return _widget;
  }

  getFirst() {
    _widget = Container(
        //   padding: EdgeInsets.all(10),
        child: Center(
      child: Column(children: [
        Card(
            child: Container(
                width: 90.0.w,
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          height: 20.0.h,
                          width: 10.0.w,
                          color: Colors.deepOrange,
                          child: Center(
                              child: RotatedBox(
                                  quarterTurns: 135,
                                  child: Text(
                                    "DAY",
                                    style: TextStyle(
                                        fontSize: 11.0.sp,
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  )))),
                      SizedBox(
                        width: 5.0.w,
                      ),
                      Container(
                        // height: 10.0.h,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 4.0.h,
                            ),
                            Container(
                              child: Padding(
                                padding: EdgeInsets.only(left: 4.0.w),
                                child: Text(
                                  "Sales",
                                  style: TextStyle(
                                      color: Colors.green, fontSize: 12.0.sp),
                                ),
                              ),
                              decoration: BoxDecoration(
                                border: Border(
                                  left:
                                      BorderSide(width: 4, color: Colors.green),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 2.0.h,
                            ),
                            Container(
                              child: Padding(
                                padding: EdgeInsets.only(left: 4.0.w),
                                child: Text(
                                  "Reciept",
                                  style: TextStyle(
                                      color: Colors.orange, fontSize: 12.0.sp),
                                ),
                              ),
                              decoration: BoxDecoration(
                                border: Border(
                                  left: BorderSide(
                                      width: 4, color: Colors.orange),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 2.0.h,
                            ),
                            Container(
                              child: Padding(
                                padding: EdgeInsets.only(left: 4.0.w),
                                child: Text(
                                  "Visit",
                                  style: TextStyle(
                                      color: Colors.blue, fontSize: 12.0.sp),
                                ),
                              ),
                              decoration: BoxDecoration(
                                border: Border(
                                  left:
                                      BorderSide(width: 4, color: Colors.blue),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 3.5.w,
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 2.5.h),
                        height: 80.0.sp,
                        width: 1.0.sp,
                        color: Colors.black26,
                      ),
                      SizedBox(
                        width: 5.0.w,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            color: Colors.white,
                            height: 4.5.h,
                            width: 15.0.w,
                            alignment: Alignment.center,
                            child: Text(
                              "Count ",
                              style: TextStyle(
                                fontSize: 12.0.sp,
                                color: Colors.purple,
                              ),
                            ),
                          ),
                          Text("0.0",
                              style: TextStyle(
                                fontSize: 12.0.sp,
                              )),
                          SizedBox(
                            height: 15,
                          ),
                          Text("0.0",
                              style: TextStyle(
                                fontSize: 12.0.sp,
                              )),
                          SizedBox(
                            height: 15,
                          ),
                          Text("0.0",
                              style: TextStyle(
                                fontSize: 12.0.sp,
                              ))
                        ],
                      ),
                      SizedBox(
                        width: 5.0.w,
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 2.5.h),
                        height: 80.0.sp,
                        width: 1.0.sp,
                        color: Colors.black26,
                      ),
                      SizedBox(
                        width: 5.0.w,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            color: Colors.white,
                            height: 4.5.h,
                            width: 15.0.w,
                            alignment: Alignment.center,
                            child: Text(
                              "Total",
                              style: TextStyle(
                                fontSize: 12.0.sp,
                                color: Colors.purple,
                              ),
                            ),
                          ),
                          Text("0.0",
                              style: TextStyle(
                                fontSize: 12.0.sp,
                              )),
                          SizedBox(
                            height: 15,
                          ),
                          Text("0.0",
                              style: TextStyle(
                                fontSize: 12.0.sp,
                              )),
                          SizedBox(
                            height: 15,
                          ),
                          Text("0.0",
                              style: TextStyle(
                                fontSize: 12.0.sp,
                              ))
                        ],
                      ),
                      SizedBox(
                        width: 5.0.w,
                      ),
                    ]))),
        Card(
          child: Container(
              width: 90.0.w,
              child:
                  Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
                SizedBox(
                  width: 5.0.w,
                ),
                Container(
                  // height: 10.0.h,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 5.0.h,
                      ),
                      Container(
                        child: Padding(
                          padding: EdgeInsets.only(left: 4.0.w),
                          child: Text(
                            "Sales",
                            style: TextStyle(
                                color: Colors.green, fontSize: 12.0.sp),
                          ),
                        ),
                        decoration: BoxDecoration(
                          border: Border(
                            left: BorderSide(width: 4, color: Colors.green),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 2.0.h,
                      ),
                      Container(
                        child: Padding(
                          padding: EdgeInsets.only(left: 4.0.w),
                          child: Text(
                            "Reciept",
                            style: TextStyle(
                                color: Colors.orange, fontSize: 12.0.sp),
                          ),
                        ),
                        decoration: BoxDecoration(
                          border: Border(
                            left: BorderSide(width: 4, color: Colors.orange),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 2.0.h,
                      ),
                      Container(
                        child: Padding(
                          padding: EdgeInsets.only(left: 4.0.w),
                          child: Text(
                            "Visit",
                            style: TextStyle(
                                color: Colors.blue, fontSize: 12.0.sp),
                          ),
                        ),
                        decoration: BoxDecoration(
                          border: Border(
                            left: BorderSide(width: 4, color: Colors.blue),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: 3.5.w,
                ),
                Container(
                  margin: EdgeInsets.only(top: 2.5.h),
                  height: 80.0.sp,
                  width: 1.0.sp,
                  color: Colors.black26,
                ),
                SizedBox(
                  width: 5.0.w,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      color: Colors.white,
                      height: 4.5.h,
                      width: 15.0.w,
                      alignment: Alignment.center,
                      child: Text(
                        "Count ",
                        style: TextStyle(
                          fontSize: 12.0.sp,
                          color: Colors.purple,
                        ),
                      ),
                    ),
                    Text("0.0",
                        style: TextStyle(
                          fontSize: 12.0.sp,
                        )),
                    SizedBox(
                      height: 3.0.w,
                    ),
                    Text("0.0",
                        style: TextStyle(
                          fontSize: 12.0.sp,
                        )),
                    SizedBox(
                      height: 3.0.w,
                    ),
                    Text("0.0",
                        style: TextStyle(
                          fontSize: 12.0.sp,
                        ))
                  ],
                ),
                SizedBox(width: 5.0.w),
                Container(
                  margin: EdgeInsets.only(top: 2.5.h),
                  height: 80.0.sp,
                  width: 1.0.sp,
                  color: Colors.black26,
                ),
                SizedBox(
                  width: 5.0.w,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      color: Colors.white,
                      height: 4.5.h,
                      width: 15.0.w,
                      alignment: Alignment.center,
                      child: Text(
                        "Total",
                        style: TextStyle(
                          fontSize: 12.0.sp,
                          color: Colors.purple,
                        ),
                      ),
                    ),
                    Text("0.0",
                        style: TextStyle(
                          fontSize: 12.0.sp,
                        )),
                    SizedBox(
                      height: 3.0.w,
                    ),
                    Text("0.0",
                        style: TextStyle(
                          fontSize: 12.0.sp,
                        )),
                    SizedBox(
                      height: 3.0.w,
                    ),
                    Text("0.0",
                        style: TextStyle(
                          fontSize: 12.0.sp,
                        ))
                  ],
                ),
                SizedBox(
                  width: 7.0.w,
                ),
                Container(
                    height: 20.0.h,
                    width: 10.0.w,
                    color: Colors.deepPurple[200],
                    child: Center(
                        child: RotatedBox(
                            quarterTurns: 135,
                            child: Text(
                              "MONTH",
                              style: TextStyle(
                                  fontSize: 11.0.sp,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            )))),
              ])),
        )
      ]),
    ));
  }

  getTwo() {
    _widget = Container(
      //  padding: EdgeInsets.all(2.5.h),
      child: ListView(children: [
        SizedBox(
          height: 5.0.h,
        ),
        Card(
          child: Container(
            child: Row(
              children: [
                SizedBox(
                  width: 2.0.w,
                ),
                Center(
                  child: CircularPercentIndicator(
                    radius: 13.0.h,
                    lineWidth: 4.0,
                    percent: 0.00,
                    center: Text("0.0 %",
                        style: TextStyle(
                          fontSize: 13.0.sp,
                        )),
                    progressColor: Colors.green,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                    left: 2.5.w,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 50.0.w,
                        alignment: Alignment.topLeft,
                        child: Text(
                          "THIS MONTH",
                          style: TextStyle(
                              fontSize: 13.5.sp, color: Colors.grey[800]),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 2.5.w),
                        width: 60.0.w,
                        alignment: Alignment.topLeft,
                        decoration: BoxDecoration(
                          border: Border(
                            bottom:
                                BorderSide(width: 0.20.w, color: Colors.black),
                          ),
                        ),
                      ),
                      Row(children: [
                        Container(
                          color: Colors.white,
                          width: 25.0.w,
                          child: Text("Target",
                              style: TextStyle(
                                  color: Colors.grey[800], fontSize: 10.0.sp)),
                        ),
                        Container(width: 10.0.w),
                        Container(
                          alignment: Alignment.center,
                          color: Colors.white,
                          padding: const EdgeInsets.only(top: 10, bottom: 10),
                          width: 5.0.w,
                          child: Text(
                            "0.0",
                            style: TextStyle(
                                color: Colors.grey[800], fontSize: 10.0.sp),
                          ),
                        ),
                      ]),
                      Row(children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          color: Colors.white,
                          width: 25.0.w,
                          child: Text(
                            "Achieved",
                            style: TextStyle(
                                color: Colors.grey[800], fontSize: 10.0.sp),
                          ),
                        ),
                        Container(width: 10.0.w),
                        Container(
                          alignment: Alignment.center,
                          color: Colors.white,
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          width: 5.0.w,
                          child: Text(
                            "0.0",
                            style: TextStyle(
                                color: Colors.grey[800], fontSize: 10.0.sp),
                          ),
                        ),
                      ]),
                      Row(children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          color: Colors.white,
                          width: 25.0.w,
                          child: Text(
                            "Pending",
                            style: TextStyle(
                                color: Colors.grey[800], fontSize: 10.0.sp),
                          ),
                        ),
                        Container(width: 10.0.w),
                        Container(
                          alignment: Alignment.center,
                          color: Colors.white,
                          padding: EdgeInsets.only(top: 10, bottom: 10),
                          width: 5.0.w,
                          child: Text(
                            "0.0",
                            style: TextStyle(
                                color: Colors.grey[800], fontSize: 10.0.sp),
                          ),
                        ),
                      ]),
                    ],
                  ),
                ),
              ],
              // crossAxisAlignment: CrossAxisAlignment.start,
            ),
          ),
        )
      ]),
    );
  }

  getThird() {
    _widget = Container(
      // padding: EdgeInsets.all(10),
      child: ListView(children: [
        SizedBox(
          height: 20,
        ),
        Card(
            child: Container(
          height: 150,
          child: Row(
            children: [
              SizedBox(
                width: 6.0.w,
              ),
              Center(
                child: CircularPercentIndicator(
                  radius: 25.0.w,
                  lineWidth: 4.0,
                  percent: 0.00,
                  center: Text("0.0 %",
                      style: TextStyle(
                        fontSize: 13.0.sp,
                      )),
                  progressColor: Colors.green,
                ),
              ),
              SizedBox(
                width: 8.0.w,
              ),
              Container(
                height: 70,
                width: 100,
                child: Padding(
                  padding: const EdgeInsets.only(left: 10, top: 10, bottom: 10),
                  child: Text(
                    "data",
                    style: TextStyle(color: Colors.black54, fontSize: 15.0.sp),
                  ),
                ),
                decoration: BoxDecoration(
                  border: Border(
                    left: BorderSide(width: 5, color: Colors.black54),
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                height: 70,
                width: 100,
                child: Padding(
                  padding: const EdgeInsets.only(left: 10, top: 10, bottom: 10),
                  child: Text(
                    "data",
                    style: TextStyle(
                        color: Colors.lightGreen[400], fontSize: 15.0.sp),
                  ),
                ),
                decoration: BoxDecoration(
                  border: Border(
                    left: BorderSide(width: 5, color: Colors.lightGreen[400]),
                  ),
                ),
              )
            ],
          ),
        ))
      ]),
    );
  }
}

exitAlert() async {
  var alert =
      AlertDialog(title: Text('Are you sure you want to exit'), actions: [
    TextButton(
        onPressed: (() {
          exit(0);
        }),
        child: Text('Yes')),
    TextButton(
        onPressed: (() {
          BuildContext contextt;
          Navigator.pop(contextt);
        }),
        child: Text('No'))
  ]);
  var contextt;
  showDialog(context: contextt, builder: (context) => alert);
}

getEmployeeName() async {
  HomeActivityState.title = 'Welcome ' +
      (await User().fromMap(HomeActivity_VM().employee_name())).employeeName;
}

//bottom buttons showing my plans and activity
class BottomButtons extends StatelessWidget {
  WillPopScope onWillPop;
  BuildContext contextt;
  @override
  Widget build(BuildContext context) {
    contextt = context;
    return Row(children: [
      buttons('My Plan', 'assets/ic_my_plan_48.png', Colors.green[300]),
      buttons('Activities', 'assets/ic_activity_white.png', Colors.blue[400])
    ]);
  }

  Widget buttons(String text, String image, Color color) {
    return Expanded(
        child: Container(
            color: color,
            padding: EdgeInsets.only(left: 2.0.h),
            child: FlatButton(
                onPressed: (() {
//logs
                  FLog.info(
                      className: runtimeType.toString(),
                      methodName: "buttons press $text",
                      text: "onPress line 870");
                     
                  Navigator.push(
                      contextt,
                      MaterialPageRoute(
                          builder: (context) => TabMyPlanActivityFragment()));
                }),
                child: Row(children: [
                  Image(height: 6.0.h, width: 6.0.w, image: AssetImage(image)),
                  SizedBox(width: 7),
                  Text(text,
                      style: TextStyle(color: Colors.white, fontSize: 15.0.sp))
                ]))));
  }
}

//page to show when my plan or activities clicked
class TabMyPlanActivityFragment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TabMyPlanActivityFragmentState();
  }
}

class TabMyPlanActivityFragmentState extends State<TabMyPlanActivityFragment>
    with TickerProviderStateMixin {
  static BuildContext contextt;
  TabController cn;
  String tab;
  Widget screen;
  Widget activityScreen_Widget = null;
  @override
  void initState() {
    cn = TabController(length: 2, vsync: this);
    if (HomeActivityState.title == 'My Plan') {
      changeTab(0);
    } else {
      changeTab(1);
    }
    getActivitys();
  }

  Widget myPlanScreen() {
    return Container(color: Colors.red);
  }

  Widget activityScreen() {
    return Container(
        padding: EdgeInsets.all(0.5.h),
        child: activityScreen_Widget != null
            ? activityScreen_Widget
            : Center(
                child: CircularProgressIndicator(
                color: Colors.orange,
              )));
  }

  getActivitys() async {
    await Future.delayed(Duration(seconds: 2));
    var items = await TabMyPlanActivityFragmentState_VM().getActivities();
    activityScreen_Widget = ListView(shrinkWrap: true, children: items);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    contextt = context;
    return WillPopScope(
        onWillPop: (() {
          onBackPressed();
        }),
        child: Material(
            child: Scaffold(
                appBar: AppBar(
                    title: Row(children: [
                  Image(
                      height: 15.0.h,
                      width: 12.0.w,
                      image: AssetImage('assets/salesnrichWhite.png')),
                  Text(tab)
                ])),
                drawer: MenuItems_Widget(),
                body: Container(
                    color: Colors.grey[300],
                    padding: EdgeInsets.all(0.5.h),
                    child: Column(children: [
                      TabBar(
                          controller: cn,
                          indicatorColor: Colors.orange,
                          onTap: (value) => print(value),
                          tabs: [
                            Container(
                                color: Colors.white,
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image(
                                          image: AssetImage(
                                              'assets/myAppOrange.png'),
                                          height: 5.0.h,
                                          width: 7.0.w),
                                      Text('My Plan',
                                          style: TextStyle(
                                              fontSize: 15.0.sp,
                                              color: Colors.orange))
                                    ])),
                            Container(
                                color: Colors.white,
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image(
                                          image: AssetImage(
                                              'assets/activities.png'),
                                          height: 5.0.h,
                                          width: 7.0.w),
                                      Text('Activities',
                                          style: TextStyle(
                                              fontSize: 15.0.sp,
                                              color: Colors.orange))
                                    ]))
                          ]),
                      Expanded(
                        child: TabBarView(
                            controller: cn,
                            children: [myPlanScreen(), activityScreen()]),
                      )
                    ])))));
  }

  Future<bool> onBackPressed() async {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => HomeActivity()));
  }

  changeTab(int value) {
    if (value == 0) {
      screen = myPlanScreen();
      tab = 'MyPlan';
    } else {
      screen = activityScreen();
      tab = 'Activity';
    }
    cn.animateTo(value);
    setState(() {});
  }
}

//side menus widget
class MenuItems_Widget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MenuItems_WidgetState();
  }
}

//http://www.salesnrich.com/api/company-image
class MenuItems_WidgetState extends State<MenuItems_Widget> {
  TextStyle textstyle = TextStyle(color: Colors.white, fontSize: 15.0.sp);
  Column menuItems = Column();
  String name = '', email = '';
  @override
  void initState() {
    returnMenus();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Drawer(
            child: Container(
                color: Colors.white,
                child: ListView(children: [
                  Image.network('http://www.salesnrich.com/api/company-image',
                      headers: {
                        'Authorization': 'Bearer ${StaticFields.token}'
                      }),
                  Container(
                      child: Stack(children: [
                    Image(image: AssetImage('assets/splash.png')),
                    Container(
                        margin: EdgeInsets.only(left: 20, top: 10),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('$name', style: textstyle),
                              SizedBox(height: 2),
                              Text('$email', style: textstyle)
                            ]))
                  ])),
                  Expanded(child: menuItems),
                  Divider(thickness: 1),
                  Padding(
                      padding: EdgeInsets.only(left: 10),
                      child: Text('Communication')),
                  ListTile(
                      title: Text('About'),
                      leading: Image(
                          height: 24,
                          width: 24,
                          image: AssetImage(
                              'assets/Menui_tem_icons/ic_about.png')),
                      onTap: (() {}))
                ]))));
  }

  returnMenus() async {
    var user = await User().fromMap(MobileMenuItems_VM().getNameEmail());
    name = user.firstName;
    email = user.email;
    MobileMenuItems_VM().getNameEmail();
    menuItems = Column(children: await MobileMenuItems_VM().getMenus());
    setState(() {});
  }
}

//class for if activities is clicked
class AccountProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AccountProfileScreen_State();
  }
}

class AccountProfileScreen_State extends State<AccountProfileScreen> {
  static List<Widget> items = [];
  static List<Widget> searchResults = [];
  static List<Widget> show = [];
  static BuildContext contextt;
  FocusNode node = FocusNode();
  static String territory = "All Territory";
  var searchController = TextEditingController();
  static var excutiveActivity = ExecutiveActivity();
  static var pid;
  @override
  void initState() {
    insertdata();
  }

  @override
  Widget build(BuildContext context) {
    contextt = context;
    return WillPopScope(
        onWillPop: (() {
          onBackPressed();
        }),
        child: Scaffold(
            appBar: StaticFields.getAppbar(tittle: 'Account Profile'),
            drawer: MenuItems_Widget(),
            body: Container(
                color: Colors.grey[300],
                padding: EdgeInsets.all(10),
                child: Column(children: [
                  Container(
                    padding: EdgeInsets.only(left: 2.0.w),
                    color: Colors.white,
                    child: Row(children: [
                      Expanded(child: Text('$territory')),
                      Container(
                          color: Colors.blue,
                          child: IconButton(
                              icon: Image(
                                  image: AssetImage(
                                      'assets/ic_filter_outline_white_24dp.png')),
                              onPressed: (() {
                                showTerritory();
                              })))
                    ]),
                  ),
                  SizedBox(height: 2.0.h),
                  Container(
                      color: Colors.white,
                      padding: EdgeInsets.only(
                          left: 1.0.w, right: 1.0.w, top: 0.1.h, bottom: 1.0.h),
                      child: Stack(
                          alignment: const Alignment(1.0, 1.0),
                          children: <Widget>[
                            TextField(
                              controller: searchController,
                              focusNode: node,
                              onChanged: ((value) {
                                AccountProfile_VM().search(value);
                              }),
                            ),
                            IconButton(
                                onPressed: () {
                                  searchController.clear();
                                  AccountProfile_VM().search("");
                                  node.unfocus();
                                },
                                icon: Icon(Icons.clear))
                          ])),
                  SizedBox(height: 2.0.h),
                  Expanded(
                      child: show.length == 0
                          ? Center(
                              child: CircularProgressIndicator(
                                  color: Colors.orange))
                          : ListView.builder(
                              itemCount: show.length,
                              itemBuilder: (context, index) {
                                return show[index];
                              },
                            ))
                ])),
            floatingActionButton: FloatingActionButton(
                onPressed: (() {
                  clear();
                }),
                child: Icon(Icons.add))));
  }

  Future<bool> onBackPressed() async {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => HomeActivity()));
  }

  clear() {
    searchController.clear();
    setState(() {});
  }

  insertdata() async {
    await AccountProfile_VM().getAccounts(pid);
  }

  showTerritory() {
    showDialog(
        context: context,
        builder: (context) {
          return Material(child: TerritoryScreen(), color: Colors.transparent);
        });
  }
}

//class for setting territorys
class TerritoryScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TerritoryState();
  }
}

class TerritoryState extends State<TerritoryScreen> {
  Widget show;
  static BuildContext contextt;
  static List<Territory> userTerritory = [];
  static List<Territory> spinner = [];
  @override
  void initState() {
    Territory_VM().getTerritory();
    set_Selectterritory();
  }

  @override
  Widget build(BuildContext context) {
    contextt = context;
    return GestureDetector(
        child: Container(
            padding: EdgeInsets.only(
                bottom: 10.0.h, top: 10.0.h, right: 5.0.w, left: 5.0.w),
            child: Container(
              child: show,
              color: Colors.green,
            )),
        onTap: () {
          //Navigator.pop(context);
        });
  }

  set_Selectterritory() async {
    show = Container(
        color: Colors.white,
        child: Column(children: <Widget>[
          Container(
              color: Colors.grey[300],
              child: Row(children: [
                Expanded(
                    child: Container(
                        alignment: Alignment.center,
                        child: Text('Select territory',
                            style: TextStyle(fontSize: 3.0.h)))),
                FlatButton(
                    onPressed: (() {
                      set_Addterritory();
                    }),
                    child: Icon(Icons.add),
                    color: Colors.orange)
              ])),
          SizedBox(height: 4.0.h),
          Container(
              padding: EdgeInsets.only(right: 2.0.w, left: 2.0.w),
              child: Material(
                  child: TextField(
                      decoration: InputDecoration(
                          hintText: 'search territory',
                          fillColor: Colors.white,
                          enabledBorder: UnderlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.orange[700])))))),
          SizedBox(height: 5.0.h),
          Expanded(
              child: ListView.builder(
                  itemCount: spinner.length,
                  itemBuilder: (context, index) {
                    return Material(
                        child: ListTile(title: Text(spinner[index].name)));
                  }))
        ]));
    setState(() {});
  }

  set_Addterritory() {
    show = Container(
      color: Colors.white,
      child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
        Container(
            color: Colors.blue,
            alignment: Alignment.center,
            child: Text('Create New territory')),
        Container(
            padding: EdgeInsets.only(right: 2.0.w, left: 2.0.w),
            child: Material(
                child: TextField(
                    decoration: InputDecoration(
                        hintText: 'search territory',
                        fillColor: Colors.white,
                        enabledBorder: UnderlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.orange[700])))))),
        SizedBox(height: 1.50.h),
        Container(
            child: Text('Select Your Parent Territory'),
            margin: EdgeInsets.all(2.0.w)),
        SizedBox(height: 1.50.h),
        Material(
            child: DropdownButton(
                items: spinner
                    .map((e) => DropdownMenuItem(value: e, child: Text(e.name)))
                    .toList())),
        Container(
            child: FlatButton(
                onPressed: (() {}), child: Text('ADD'), color: Colors.orange),
            margin: EdgeInsets.all(2.0.w))
      ]),
    );
    setState(() {});
  }
}
