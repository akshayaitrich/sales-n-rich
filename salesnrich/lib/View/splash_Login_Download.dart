import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:lottie/lottie.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:salesnrich/Controller/FetchMaster.dart';
import 'package:salesnrich/Controller/keygenerator.dart';
import 'package:salesnrich/Controller/staticFields.dart';
import 'package:salesnrich/Model/user.dart';
import 'package:salesnrich/ViewModel/LoginVM.dart';
import 'package:sizer/sizer.dart';
import 'home_Activity.dart';

class FirstPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FirstPageState();
  }
}

class FirstPageState extends State<FirstPage> {
  static Widget wi = SplashScreen();
  static BuildContext contextt;

  @override
  void initState() {
    change();
  }

  @override
  Widget build(BuildContext context) {
    contextt = context;
    return wi;
  }

  change() async {
    await Future.delayed(Duration(seconds: 2));
    wi = LoginPage();
    setState(() {});
  }
}

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/splash.png'), fit: BoxFit.fitHeight)),
        child: Stack(children: [
          Container(
              child: Center(
                  child: Image(image: AssetImage('assets/splash_logo.png'))))
        ]));
  }
}

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginPageState();
  }
}

class LoginPageState extends State<LoginPage> {
  static BuildContext contextt;
  static TextEditingController username = TextEditingController();
  static TextEditingController password = TextEditingController();
  String userError, passwordError;
  bool showPassword = false;
  bool usernull = false, passnull = false, credentialerror = false;

  @override
  void initState() {
    LoginVM().getuser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    contextt = context;
    return Scaffold(
        backgroundColor: Colors.grey[300],
        appBar: AppBar(),
        body: Container(
          margin: EdgeInsets.all(10),
          color: Colors.white,
          child: Container(
              padding: EdgeInsets.only(left: 40, right: 40),
              child: returnCloum()),
        ));
  }

  Widget returnCloum() {
    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      textfield(labelText: 'Username', field: username, error: this.userError),
      textfield(
          labelText: 'Password', field: password, error: this.passwordError),
      SizedBox(height: 20),
      Visibility(
          visible: credentialerror,
          child: Text(
            "BAD DREDENTIALS",
            style: TextStyle(color: Colors.red, fontSize: 15.0.sp),
          )),
      SizedBox(height: 20),
      Row(children: [
        Expanded(
            child: FlatButton(
                child: Text('Login',
                    style: TextStyle(color: Colors.white, fontSize: 12.0.sp)),
                height: 5.0.h,
                onPressed: validate,
                color: Colors.orange))
      ]),
    ]);
  }

  Widget textfield(
      {String labelText, TextEditingController field, String error}) {
    var iconbutton = labelText == 'Username'
        ? usernull
            ? IconButton(
                iconSize: 4.0.h,
                onPressed: () {
                  if (username.text == null || username.text == '') {
                    userError = 'Field is empty';
                  }
                  setState(() {});
                },
                icon: Icon(
                  Icons.report,
                  color: Colors.red,
                ))
            : null
        : IconButton(
            iconSize: 4.0.h,
            icon: Icon(Icons.remove_red_eye,
                color: showPassword == true ? Colors.blue : Colors.black),
            onPressed: (() {
              showPassword == true ? showPassword = false : showPassword = true;
              setState(() {});
            }));

    return TextField(
      style: TextStyle(fontSize: 14.0.sp),
      obscureText: labelText == 'Password'
          ? showPassword == true
              ? false
              : true
          : false,
      controller: field,
      decoration: InputDecoration(
          errorStyle: TextStyle(fontSize: 10.0.sp),
          errorText: error,
          labelText: labelText,
          suffixIcon: passnull
              ? IconButton(
                  iconSize: 4.0.h,
                  onPressed: () {
                    passwordError = 'Field is empty';
                    setState(() {});
                  },
                  icon: Icon(
                    Icons.report,
                    color: Colors.red,
                  ))
              : iconbutton),
    );
  }

  validate() async {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (username.text == null || username.text == '') {
      usernull = true;
    } else {
      usernull = false;
      userError = null;
    }
    if (password.text == null || password.text == '') {
      passnull = true;
    } else {
      passnull = false;
      passwordError = null;
    }

    if (username.text != null &&
        username.text != '' &&
        password.text != null &&
        password.text != '') {
      bool isThereaUser =
          await LoginVM(username: username.text, password: password.text)
              .credientials();
      if (!isThereaUser) {
        credentialerror = true;
      } else {
        changetoDownload();
      }
    }
    setState(() {});
  }

  changetoDownload() async {
    if (StaticFields.userexists || StaticFields.downLoadPercentage == 100) {
      FirstPageState.wi = HomeActivity();
      (FirstPageState.contextt as StatefulElement).state.setState(() {});
    } else {
      bool b = await askPermission();
      if (b) {
        UserModel user = UserModel(
            tokenId: StaticFields.token,
            username: username.text,
            userPassword: password.text);
        UserDatabase().insertUser(user);
        FirstPageState.wi = DownloadPage();
        (FirstPageState.contextt as StatefulElement).state.setState(() {});
      } else {
        var status = await Permission.phone.status;
        var location = await Permission.location.status;
        var storage = await Permission.storage.status;
        var camera = await Permission.camera.status;
        if (!status.isGranted ||
            !location.isGranted ||
            !storage.isGranted ||
            !camera.isGranted) {
          Fluttertoast.showToast(msg: 'Some Permmissions are denied');
        }
      }
    }
  }
}

class DownloadPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return DownloadPageState();
  }
}

class DownloadPageState extends State<DownloadPage> {
  static BuildContext contextt;
  static String total_Downloads = '0/53';
  static double current_Download_Percentage = 0;
  static double download_Proggressbar_length = current_Download_Percentage * 3;
  static String item_Started_to_download = '';
  static String sub_items_downloaded_inThat = '0 items downloaded';

  static Widget button;

  @override
  void initState() {
    button = FlatButton(
        onPressed: (() {
          startDownload();
        }),
        color: Colors.orange,
        child: Text(
          'Download',
          style: TextStyle(fontSize: 14.0.sp),
        ));
  }

  @override
  Widget build(BuildContext context) {
    contextt = context;

    return Scaffold(
        body: Container(
      padding: EdgeInsets.all(10),
      child: Column(
        children: [
          Expanded(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                ourProgressBar(),
                SizedBox(height: 25),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(children: [
                    Text(
                      '$item_Started_to_download',
                      style: TextStyle(color: Colors.blue, fontSize: 13.0.sp),
                    ),
                    Lottie.asset('assets/50829-dots-loader.json',
                        height: 10.0.h),
                  ]),
                ),
                SizedBox(height: 25),
                Text('$sub_items_downloaded_inThat')
              ])),
          button
        ],
      ),
    ));
  }

  Widget ourProgressBar() {
    Widget progress() {
      return Row(
        children: [
          Text(
            '${current_Download_Percentage.toInt()}%',
            style: TextStyle(fontSize: 15.0.sp),
          ),
          Stack(children: [
            Container(width: 75.0.w, height: 0.5.h, color: Colors.grey),
            Container(
                width: download_Proggressbar_length,
                height: 0.5.h,
                color: Colors.orange)
          ]),
        ],
      );
    }

    return Row(
      children: [
        Container(child: progress()),
        Text(
          '$total_Downloads',
          style: TextStyle(fontSize: 14.0.sp),
        )
      ],
    );
  }

  startDownload() {
    Fetchmaster();
  }
}
