// ignore_for_file: unused_local_variable, unnecessary_cast

import 'dart:math';
import 'package:airplane_mode_checker/airplane_mode_checker.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:salesnrich/Controller/preferenceOperation.dart';
import 'package:salesnrich/Controller/staticFields.dart';
import 'package:salesnrich/Model/Tables.dart';
import 'package:salesnrich/Model/database.dart';
import 'package:salesnrich/View/document.dart';
import 'package:salesnrich/WebServer.dart';
import 'package:sim_info/sim_info.dart';
import 'package:sizer/sizer.dart';
import 'package:geocoding/geocoding.dart';
// import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;

class Document_VM {
  static var clientPrimaryKey = " ";
  static var taskExecution;
  static Document document;
  static String date1;
  static String invoiceNo;
  getDocumetsByActivityPid(var pid) async {
    // await OurDatabase(
    //         table: InventoryVoucherDetails().table,
    //         query: InventoryVoucherDetails().query)
    //     .deleteAll(); //006
    DocumentPageState.activityDocuments = [];
    var activityDocument = [];
    var alldocument = await getDocument() as List;
    var allactivityDocument = await getActivityDocument(pid) as List;
    for (var i = 0; i < alldocument.length; i++) {
      var doc = Document().fromMap(alldocument[i]);
      for (var j = 0; j < allactivityDocument.length; j++) {
        //   print('this the all activitydocument[j] ${allactivityDocument[j]}');
        var act = ActivityDocument().fromMap(allactivityDocument[j]);
        // print('this is the act.pid${act.pid}');
        // print('this is the doc.pid${doc.pid}');

        if (doc.pid == act.pid) {
          //   print("this is the act${act}");

          activityDocument.add(allactivityDocument[j]);
          // print(" the aaqcticity${activityDocument}");

          break;
        }
      }
    }
    // print("${activityDocument[0]}");
    activityDocument.sort((a, b) =>
        (a['activityDocSortOrder']).compareTo(b['activityDocSortOrder']));
    activityDocument.forEach((element) {
      var mp = element;
      DocumentPageState.activityDocuments.add(getSingleDocumen(mp));
      (DocumentPageState.contextt as StatefulElement).state.setState(() {});
    });
  }

  getActivityDocument(var pid) async {
    // print('this is the pid $pid');
    String table = ActivityDocument().table;
    List filterlist = [];
    var list = await OurDatabase(table: table).getTables() as List;
    list.forEach((element) {
      var mp = element;
      //    print('this is datA1 ${mp.id}');
      if (pid == mp['activityPid']) {
        filterlist.add(mp);
      }
    });
    return filterlist;
  }

  getDocument() async {
    String table = Document().table;
    var document = await OurDatabase(table: table).getTables() as List;
    return document;
  }

  getSingleDocumen(var mp) {
    var totalwithnoofitems;

    if (mp['name'] == 'Van Sales') {
      if (InventoryVoucherFormScreenState.carttotal == null ||
          InventoryVoucherFormScreenState.carttotal == 0.0) {
        totalwithnoofitems = '';
      } else {
        totalwithnoofitems =
            '${InventoryVoucherFormScreenState.carttotal} (${InventoryVoucherFormScreenState.noofitemsincart})';
      }
    } else {
      totalwithnoofitems = '';
    }
    Widget getWidget() {
      return Container(
          padding: EdgeInsets.all(2.0.h),
          child: Container(
              padding: EdgeInsets.only(left: 2.0.h),
              height: 6.0.h,
              alignment: Alignment.centerLeft,
              child: Row(
                children: [
                  Text(mp['name']),
                  SizedBox(
                    width: 35.0.w,
                  ),
                  Text(totalwithnoofitems,
                      style:
                          TextStyle(color: Colors.black54, fontSize: 10.0.sp))
                ],
              ),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: 2))));
    }

    return GestureDetector(
        onTap: () {
          onDocumentPress(Document().fromMap(mp));
        },
        child: getWidget());
  }

  onDocumentPress(mp) async {
    var profile = DocumentPageState.accountProfile;
    var activity = DocumentPageState.excutiveActivity;
    var table = MobileSettingsConfiguration().table;
    MobileSettingsConfiguration mobileConfig;
    var list = await OurDatabase(table: table).getTables() as List;
    list.forEach((element) {
      mobileConfig = MobileSettingsConfiguration().fromMap(element);
    });

    generateTaskExecution(mp, false);

    if (mp.documentType == 'INVENTORY_VOUCHER') {
      if (mobileConfig.inventoryVoucherUIType == 'TYPE_1') {
        print('TYPE_1');
        document = mp;
        // print((taskExecution as TaskExecution).accountProfileAddress);
        InventoryVoucherFormScreenState.accountPid =
            (taskExecution as TaskExecution).accountProfilePid;
        InventoryVoucherFormScreenState.document = document;
        InventoryVoucherFormScreenState.taskExecution = taskExecution;
        Navigator.of(DocumentPageState.contextt).push(MaterialPageRoute(
            builder: (context) => InventoryVoucherFormScreen()));
      } else {
        print('Dynamic type');
      }
    } else if (mp.documentType == 'ACCOUNTING_VOUCHER') {
      print('accounting');
      taskExecution as TaskExecution;
      AccountingVoucherFormScreenState.accountPid =
          (taskExecution as TaskExecution).accountProfilePid;
      AccountingVoucherFormScreenState.document = document;
      Navigator.of(DocumentPageState.contextt).push(MaterialPageRoute(
          builder: (context) => AccountingVoucherFormScreen()));
    } else if (mp.documentType == 'DYNAMIC_DOCUMENT') {
      print('dynamic');
    }
  }

  generateTaskExecution(var document, bool isAdvanceSearcch) async {
    var profile = DocumentPageState.accountProfile;
    var activity = DocumentPageState.excutiveActivity;

    taskExecution = null;
    if (taskExecution == null) {
      taskExecution = new TaskExecution();

      taskExecution.accountProfileId = profile.id;
      taskExecution.accountProfilePid = profile.pid;
      taskExecution.accountProfileName = profile.name;
      taskExecution.accountProfileAddress = profile.address;
      taskExecution.accountProfilePhNo = profile.phone1;

      taskExecution.executiveActivityId = activity.id;
      taskExecution.executiveActivityPid = activity.pid;
      taskExecution.executiveActivityName = activity.name;
      activity.interimSave == 0 ? taskExecution.interimSave = true : false;

      taskExecution.activityGroupId = '';
      taskExecution.activityGroupName = '';
      taskExecution.activityGroupPid = '';

      taskExecution.outstandingAmount = DocumentPageState.outstanding;

      var date = new DateTime.now();
      DocumentPageState.mcc = await SimInfo.getMobileCountryCode;
      DocumentPageState.mnc = await SimInfo.getMobileNetworkCode;
      var status = await AirplaneModeChecker.checkAirplaneMode();
      var hour = "${date.hour}".length == 1 ? "0${date.hour}" : "${date.hour}";
      var month =
          "${date.month}".length == 1 ? "0${date.month}" : "${date.month}";
      var day = "${date.day}".length == 1 ? "0${date.day}" : "${date.day}";
      var minute =
          "${date.minute}".length == 1 ? "0${date.minute}" : "${date.minute}";
      var second =
          "${date.second}".length == 1 ? "0${date.second}" : "${date.second}";
      date1 =
          "${date.year}-${month}-${day}T${hour}:${minute}:${second}.${date.millisecond}${date.microsecond}";
      taskExecution.sendDate = date1;
      var time = '${TimeOfDay.now().hour}:${TimeOfDay.now().minute}';
      taskExecution.sendTime = time.toString();

      taskExecution.successSendDate = null;

      taskExecution.remarks = getDocumentRemarks();
      taskExecution.accountTypePid = profile.accountTypePid;
      taskExecution.accountTypeName = profile.accountTypeName;
      taskExecution.sentStatus = 1;
      taskExecution.isSelected = false;
      taskExecution.latitude = 0.0;
      taskExecution.longitude = 0.0;
      taskExecution.isLocationStatus = true;
      taskExecution.isTowerLocationStatus = false;
      taskExecution.mcc = DocumentPageState.mcc;
      taskExecution.mnc = DocumentPageState.mnc;
      taskExecution.isFlightModeStatus =
          status == AirplaneModeStatus.off ? false : true;
      taskExecution.cellId = "2";
      // DocumentPageState.cellsResponse1.primaryCellList.length.toString();
      if (isAdvanceSearcch) {
        if (document.documentType == 'DYNAMIC_DOCUMENT') {
          taskExecution.isDynamicUpdateSend = true;
          taskExecution.isInventoryUpdateSend = false;
          taskExecution.isAccountingUpdateSend = false;
        } else if (document.documentType == 'INVENTORY_VOUCHER') {
          taskExecution.isInventoryUpdateSend = true;
          taskExecution.isDynamicUpdateSend = false;
          taskExecution.isAccountingUpdateSend = false;
        } else {
          taskExecution.isAccountingUpdateSend = true;
          taskExecution.isInventoryUpdateSend = false;
          taskExecution.isDynamicUpdateSend = false;
        }
        taskExecution.updatedDocumentPid = document['pid'];
      } else {
        taskExecution.isAccountingUpdateSend = false;
        taskExecution.isInventoryUpdateSend = false;
        taskExecution.isDynamicUpdateSend = false;
        taskExecution.updatedDocumentPid = null;
      }
      //TODO
      //   activity['today'] = 0.0;
      taskExecution.accountProfileToServerReason = null;
      taskExecution.isStartTimeLocationStatus = null;

      //taskExecutionId if the id return by insert the taskExectution object to sql
      var taskExecutionId = DateTime.now().millisecond + Random().nextInt(99);
      taskExecution.id = int.parse(taskExecutionId.toString());

      invoiceNo = StaticFields.user.username + "_$taskExecutionId";

      taskExecution.clientPrimaryKeyId = generateClientTransactionKey();
      // taskExecution.tabUniqueId = invoiceNo;
      //TODO
      taskExecution.isGpsOff =
          DocumentPageState.position != null ? false : true;
      taskExecution.isMobileDataOff = null;
      taskExecution.executiveTaskPlanPid = null;
      var id = OurDatabase(
              table: TaskExecution().table, query: TaskExecution().query)
          .insertTable(TaskExecution().objectToMap(taskExecution));
      InventoryVoucherFormScreenState.taskExecutionHeaderId = id;
      //update taskExecution
      //update ExecutiveActivity
    } else {
      if (isAdvanceSearcch) {
        if (document.documentType == 'DYNAMIC_DOCUMENT') {
          taskExecution.isDynamicUpdateSend = true;
        } else if (document.documentType == 'INVENTORY_VOUCHER') {
          taskExecution.isInventoryUpdateSend = true;
        } else {
          taskExecution.isAccountingUpdateSend = true;
        }
        taskExecution.updatedDocumentPid = document.pid;
      }
      taskExecution.remarks = getDocumentRemarks();
      //TODO
      taskExecution.isGpsOff =
          DocumentPageState.position != null ? false : true;
      taskExecution.isMobileDataOff = null;
      //update taskExecution
    }
  }

  generateClientTransactionKey() {
    clientPrimaryKey = " ";
    if (clientPrimaryKey.length < 3) {
      var timeMilliSecond = new DateTime.now().toString();
      // print(StaticFields.user.username);
      String username = StaticFields.user.username;
      String random = new Random().nextInt(10).toString();
      clientPrimaryKey = timeMilliSecond + "_" + username + "_" + random;
    }
    return clientPrimaryKey;
  }

  getDocumentRemarks() {
    return DocumentPageState.remarks.text;
  }
}

class InventoryVoucherForm_VM {
  static int a;
  List<ActivityAccountType> activityAccountTypes = [];
  List<AccountProfile> accountProfilesList = [];
  static List<DocumentAccountType> receiverAccountTypeList = [];
  static List<DocumentAccountType> supplierAccountTypeList = [];
  static List<AccountProfile> receiverAccountProfileList = [];
  static List<AccountProfile> supplierAccountProfileList = [];
  static List<InventoryVoucherDetails> inventoryVoucherDetailsList = [];
  PriceLevel selectedPriceLevel;

  static var list2 = [];
  ProductCategory productCategory = null;
  ProductGroup productGroup = null;
  var preference;
  GuidedSellingConfiguration guidedSellingConfiguration;

  getGuidedSellingConfiguration() async {
    var abc = await OurDatabase(table: GuidedSellingConfiguration().table)
        .databaseExists();
    if (abc) {
      var li = await OurDatabase(table: GuidedSellingConfiguration().table)
          .getTables() as List;
      guidedSellingConfiguration =
          GuidedSellingConfiguration().fromMap(li.first);
    }
  }

  getProductCategory_n_group(ProductProfile product) async {
    // print(product);

    var defaultProductLoadingFrom =
        await Preference().getDefaultProductLoadingFrom();

    if (defaultProductLoadingFrom == "Product Category") {
      productCategory = await OurDatabase(table: ProductCategory().table)
          .getTable(where: 'pid', whereArgs: product.productCategoryPid);
    } else {
      var productToProductGroup = null;
      if (await OurDatabase(table: ProductToProductGroup().table)
          .databaseExists()) {
        productToProductGroup =
            await OurDatabase(table: ProductToProductGroup().table).getTable(
                where: 'pid', whereArgs: product.pid) as Map<String, dynamic>;
      }
      if (productToProductGroup != null) {
        productGroup = await OurDatabase(table: ProductGroup().table).getTable(
            where: 'pid', whereArgs: productToProductGroup.productGroupPid);
      } else {
        var productProfile = await OurDatabase(table: ProductProfile().table)
            .getTable(where: 'pid', whereArgs: product.pid);
        if (productProfile != null ||
            guidedSellingConfiguration.favouriteProductGroupPid != null) {
          var pid = product.pid;
          var productGroup = await OurDatabase(table: ProductGroup().table)
              .getTable(where: 'pid', whereArgs: pid);
          print('thios is ${productGroup}');
        }
      }
    }
  }

  getProductProfile() async {
    // await OurDatabase(table: InventoryVoucherHeader().table).deleteAll();
    list2.clear();
    InventoryVoucherFormScreenState.productsWidget = [];
    // print(InventoryVoucherFormScreenState.selectedndex.toString());
    var table = ProductProfile().table;
    var list = await OurDatabase(table: table).getTables() as List;
    // list2 = [];
    list.forEach((element) {
      list2.add(element);
    });
    list2.sort((a, b) {
      var mp1 = a as Map<String, dynamic>;
      var mp2 = b as Map<String, dynamic>;
      return mp1['name'].toString().compareTo(mp2['name'].toString());
    });
    list2.forEach((element) {
      // print(element);
      InventoryVoucherFormScreenState.productsWidget
          .add(getProductWidget(ProductProfile().fromMap(element)));
      (InventoryVoucherFormScreenState.contextt as StatefulElement)
          .state
          .setState(() {});
    });
  }

  Widget getProductWidget(ProductProfile mp) {
    var status;
    list2.forEach((element) {
      ProductProfile profile = ProductProfile().fromMap(element);
      if (profile.pid == mp.pid) {
        status = list2.indexOf(element);
      }
    });
    return ListTile(
        dense: true,
        tileColor: InventoryVoucherFormScreenState.selectedndex.contains(status)
            ? Colors.lightBlueAccent[700]
            : Colors.white,
        title: Text(
          mp.name,
          style: TextStyle(
              color: mp.stockQty == 0 ? Colors.red : Colors.black,
              fontSize: 12.0.sp),
        ),
        onTap: (() {
          a = status;
          InventoryVoucherFormScreenState.selectedndex.add(status);
          Inventory.selectedPrice = selectedPriceLevel;
          Inventory().show(mp);
        }));
  }

  getActivityAccountTypes() async {
    var list = await OurDatabase(table: ActivityAccountType().table).getTables()
        as List;
    print("ActivityAccountTypes");
    list.forEach((element) {
      activityAccountTypes.add(element as ActivityAccountType);
    });
    getReceiverSupplierAccountTypes();
  }

  search(String search) {
    var namesList1 = [];
    list2.forEach((element) {
      if (element['name']
          .toString()
          .toLowerCase()
          .contains(search.toLowerCase())) {
        namesList1.add(element);
      }
    });
    namesList1.sort((a, b) {
      var mp1 = a as Map<String, dynamic>;
      var mp2 = b as Map<String, dynamic>;
      return mp1['name'].toString().compareTo(mp2['name'].toString());
    });
    InventoryVoucherFormScreenState.productsWidget.clear();
    namesList1.forEach((element) {
      var mp = element as Map<String, dynamic>;
      InventoryVoucherFormScreenState.productsWidget
          .add(getProductWidget(ProductProfile().fromMap(mp)));
      (InventoryVoucherFormScreenState.contextt as StatefulElement)
          .state
          .setState(() {});
    });
  }

  getAccountProfiles() async {
    var list =
        await OurDatabase(table: AccountProfile().table).getTables() as List;
    list.forEach((element) {
      accountProfilesList.add(AccountProfile().fromMap(element));
    });
    filterAccountType();
  }

  getReceiverSupplierAccountTypes() async {
    var list = await OurDatabase(table: DocumentAccountType().table).getTable(
        where: "documentPid", whereArgs: Document_VM.document.pid) as List;

    list.forEach((element) {
      var mp = DocumentAccountType().fromMap(element);
      if (mp.accountTypeColumn == "Receiver") {
        receiverAccountTypeList.add(mp);
      } else if (mp.accountTypeColumn == "Supplier") {
        supplierAccountTypeList.add(mp);
      }
    });
    getAccountProfiles();
  }

  filterAccountType() {
    InventoryVoucherFormScreenState.receiver.clear();
    InventoryVoucherFormScreenState.supplier.clear();

    findSupplierAccountProfiles();
    findReceiverAccountProfiles();
    String activityAccountType = Document_VM.document.activityAccount;
    if (activityAccountType == "Receiver") {
      bool hasfound = false;
      for (int i = 0; i < receiverAccountTypeList.length; i++) {
        for (int j = 0; j < activityAccountTypes.length; j++) {
          var docAccTpe = receiverAccountTypeList[i];
          var accType = activityAccountTypes[j];
          if (docAccTpe.accountTypePid == accType.pid) {
            hasfound = true;
          }
          if (!hasfound) {
            receiverAccountTypeList.removeAt(i);
            break;
          }
        }
      }
    } else if (activityAccountType == "Supplier") {
      bool hasfound = false;
      for (int i = 0; i < supplierAccountTypeList.length; i++) {
        for (int j = 0; j < activityAccountTypes.length; j++) {
          var docAccTpe = supplierAccountTypeList[i];
          var accType = activityAccountTypes[j];
          if (docAccTpe.accountTypePid == accType.pid) {
            hasfound = true;
          }
          if (!hasfound) {
            supplierAccountTypeList.removeAt(i);
            break;
          }
        }
      }
    }
    InventoryVoucherFormScreenState.receiver.clear();
    InventoryVoucherFormScreenState.supplier.clear();
    findReceiverAccountProfiles();
    findSupplierAccountProfiles();
  }

  findReceiverAccountProfiles() {
    for (int j = 0; j < accountProfilesList.length; j++) {
      for (int i = 0; i < receiverAccountTypeList.length; i++) {
        var docAccType = receiverAccountTypeList[i];
        var accountProfile = accountProfilesList[j];
        if (docAccType.accountTypePid == accountProfile.accountTypePid) {
          receiverAccountProfileList.add(accountProfile);
          break;
        }
      }
    }

    receiverAccountProfileList.forEach((element) {
      InventoryVoucherFormScreenState.receiver.add(element.name);
      (InventoryVoucherFormScreenState.contextt as StatefulElement)
          .state
          .setState(() {});
    });
    if (receiverAccountProfileList.length > 0) {
      String activityAccounts = Document_VM.document.activityAccount;
      if (activityAccounts.isNotEmpty) {
        if (Document_VM.document.activityAccount == "Receiver") {
          InventoryVoucherFormScreenState.receiver = [];
          bool status = false;
          for (int i = 0; i < receiverAccountProfileList.length; i++) {
            var docAccType = receiverAccountProfileList[i];
            if (docAccType.pid.toString() ==
                DocumentPageState.accountProfile.pid) {
              InventoryVoucherFormScreenState.receiver.add(docAccType.name);
              (InventoryVoucherFormScreenState.contextt as StatefulElement)
                  .state
                  .setState(() {});
              break;
            }
          }
        }
      }
    }
  }

  findSupplierAccountProfiles() {
    supplierAccountProfileList.clear();
    for (int j = 0; j < accountProfilesList.length; j++) {
      for (int i = 0; i < supplierAccountTypeList.length; i++) {
        var docAccType = supplierAccountTypeList[i];
        var accountProfile = accountProfilesList[j];
        if (docAccType.accountTypePid == accountProfile.accountTypePid) {
          supplierAccountProfileList.add(accountProfile);
          break;
        }
      }
    }
    supplierAccountProfileList.forEach((element) {
      print('this is thename of ${element.name} ');
      InventoryVoucherFormScreenState.supplier.add(element.name);
      (InventoryVoucherFormScreenState.contextt as StatefulElement)
          .state
          .setState(() {});
    });
    if (supplierAccountProfileList.length > 0) {
      String activityAccounts = Document_VM.document.activityAccount;
      if (activityAccounts.isNotEmpty) {
        if (Document_VM.document.activityAccount == "Supplier") {
          InventoryVoucherFormScreenState.supplier = [];
          bool status = false;
          for (int i = 0; i < supplierAccountProfileList.length; i++) {
            var docAccType = supplierAccountProfileList[i];
            if (docAccType.pid.toString() ==
                DocumentPageState.accountProfile.pid) {
              InventoryVoucherFormScreenState.supplier.add(docAccType.name);
              (InventoryVoucherFormScreenState.contextt as StatefulElement)
                  .state
                  .setState(() {});
              break;
            }
          }
        }
      }
    }
    loadDocumentPriceLevels();
  }

  loadDocumentPriceLevels() async {
    var list = await OurDatabase(table: AccountProfile().table).getTable(
        where: "pid",
        whereArgs: InventoryVoucherFormScreenState.accountPid) as List;
    // print('list ______ $list');
    if (list != null) {
      var priceLevelList =
          await OurDatabase(table: PriceLevel().table).getTables() as List;
      // print(priceLevelList);

      var accountProfile = AccountProfile().fromMap(list[0]);
      if (accountProfile.defaultPriceLevelName != null) {
        var map = await OurDatabase(table: PriceLevel().table).getTable(
            where: 'pid',
            whereArgs: accountProfile.defaultPriceLevelName) as List;
        if (map != null) {
          selectedPriceLevel = PriceLevel().fromMap(map[0]);
          if (selectedPriceLevel.name != null) {
            //TODO
          }
        }
      } else {
        //TODO
      }
    }
  }

  getDynamicFormulaByDocumentPid() async {
    InventoryVoucherFormScreenState.dynamicFormulalist = [];
    var map = await OurDatabase(table: DynamicFormula().table).getTable(
        where: "documentPid",
        whereArgs: InventoryVoucherFormScreenState.document.pid) as List;

    map.forEach((element) {
      InventoryVoucherFormScreenState.dynamicFormulalist
          .add(DynamicFormula().fromMap(element));
      (InventoryVoucherFormScreenState.contextt as StatefulElement)
          .state
          .setState(() {});
    });
  }

  onAdd(
    InventoryVoucherDetails inventoryVoucherDetails,
    ProductProfile productProfile,
  ) {
    if (inventoryVoucherDetailsList.length > a) {
      inventoryVoucherDetailsList[a] = inventoryVoucherDetails;
      // inventoryVoucherDetailsList.insert(a, inventoryVoucherDetails);
    } else {
      inventoryVoucherDetailsList.length = a;
      inventoryVoucherDetailsList.insert(a, inventoryVoucherDetails);
    }
  }
}

class Inventory {
  static PriceLevel selectedPrice;
  var price_with = "rateWithTax";
  var rate;
  var dropdownValue = "One";
  List vouchers;
  String total = "0.0";
  String Wgt_Pcs = "Wgt/Pcs";
  static List<Widget> items;

  static List totalpriceslist = [];
  List<String> headers = [];
  var quantity, taxrate, mrp, totalrate;
  ProductProfile products;
  InventoryVoucherDetails inventoryVoucher;
  InventoryVoucherDetails inventoryVoucherforcart = InventoryVoucherDetails();

  ProductCategory productCategory;
  ProductGroup productgroup;
  var mobile_configurations = [];
  List<ProductPriceList> productPriceLists = [];
  var selectedPriceLevel;
  bool enableDynamicUnit,
      rateWithoutCalc,
      preventNegativeStock,
      belowPriceLevel,
      isOrderTakingDialogShow,
      orderUpdation,
      isPcsToMT,
      isMT,
      isLoadNewProduct = false;

  bool isBatchExist = false;

  bool realTimeProductPriceEnabled = false;

  TextEditingController stock_controller = TextEditingController();
  TextEditingController price_w_tax_controller = TextEditingController();
  TextEditingController size_controller = TextEditingController();
  TextEditingController unit_quantity_controller = TextEditingController();
  TextEditingController quantity_controller = TextEditingController();
  TextEditingController selling_rate_controller = TextEditingController();
  TextEditingController free_quantity_controller = TextEditingController();
  TextEditingController dis_amount_controller = TextEditingController();
  TextEditingController dis_per_controller = TextEditingController();
  TextEditingController mrp_controller = TextEditingController();
  TextEditingController tax_amount_controller = TextEditingController();
  TextEditingController offer_string_controller = TextEditingController();
  TextEditingController remark_controller = TextEditingController();
  TextEditingController total_amount_controller = TextEditingController();
  TextEditingController unit_type_controller = TextEditingController();
  TextEditingController psc_calc_controller = TextEditingController();
  TextEditingController mt_calc_controller = TextEditingController();

  final keyfordiscountpercentvalidate = GlobalKey<FormState>();
  var discountpercentvalidate;
  final keyfordiscountamountvalidate = GlobalKey<FormState>();
  var discountamountvalidate;
  final keyforoffersvalidate = GlobalKey<FormState>();
  var offresvalidate;
  final keyformrpvalidate = GlobalKey<FormState>();
  var mrpvalidate;
  final keyfortaxvalidate = GlobalKey<FormState>();
  var taxvalidate;

  show(ProductProfile products) async {
    bool stockVisibility = false,
        ratewitaxVisibility = false,
        sizeVisibility = false,
        unitquantityVisibility = false,
        quantityVisibility = false,
        rateVisibility = false,
        freequantityVisibility = false,
        discountRSVisibility = false,
        discountPerVisibility = false,
        mrpVisibility = false,
        taxVisibility = false,
        remarkVisibility = false,
        offerVisibility = false,
        unitTypeVisibility = false,
        calcVisibility = false;

    bool addBtnVisibility = true,
        cancelBtnVisibility = true,
        removeBtnVisibility = false,
        updateBtnVisibility = false;

    bool sellingRateEnable = false,
        quantityEnable = false,
        mrpEnable = false,
        freeQuantityEnable = false,
        discountPerEnable = false,
        discountAmtEnable = false,
        remarkEnable = false;

    this.products = products as ProductProfile;
    // products.forEach((key, value) {
    //   print("$key :$value");
    // });

    var productCategory = null;
    var productGroup = null;

    // var favouriteProductGroupPid;

    // var data =
    //     await OurDatabase(table: SharedPrefernce().table).getTables() as List;
    // var preference = data.first as Map<String, dynamic>;

    // var defaultLoadingFrom = preference[PREF_KEY_DEFAULT_PRODUCT_LOADING_FROM];

    InventoryVoucherForm_VM().getProductCategory_n_group(products);
    // print('selectedPrice ______ $selectedPrice');
    //================================================================================================================
    //mobile configurations
    var mobileConfig =
        await OurDatabase(table: MobileSettingsConfiguration().table)
            .getTables() as List;
    mobileConfig.forEach((element) {
      mobile_configurations.add(MobileSettingsConfiguration().fromMap(element));
    });

    print('configs ______ ${mobile_configurations[0]}');

    enableDynamicUnit = mobile_configurations.first.enableDynamicUnit;
    rateWithoutCalc = mobile_configurations.first.rateWithoutCalculation;
    preventNegativeStock = mobile_configurations.first.preventNegativeStock;
    belowPriceLevel = mobile_configurations.first.belowPriceLevel;

    //================================================================================================================

    //   if (defaultLoadingFrom.toString() == "Product Category") {
    //     productCategory = await OurDatabase(table: ProductCategory().table)
    //         .getTable(where: 'pid', whereArgs: products['productCategoryPid']);
    //  } else {
    //     var productToProductGroup ={};
    //         // await OurDatabase(table: ProductToProductGroup().table)
    //         //     .getTable(where: 'pid', whereArgs: products['pid']);

    //     if (productToProductGroup != null) {
    //       productGroup = await OurDatabase(table: ProductGroup().table).getTable(
    //           where: 'pid', whereArgs: productToProductGroup['productGroupPid']);
    //     } else {
    //       bool isFavGroup = checkProductProfileExist(products['pid']);
    //       if (isFavGroup) {
    //         productGroup = await OurDatabase(table: ProductGroup().table)
    //             .getTable(where: 'pid', whereArgs: favouriteProductGroupPid);
    //       }
    //     }
    //   }
    //================================================================================================================
    //initial all by mobile configurations
    if (enableDynamicUnit == 1) {
      unitTypeVisibility = true;
      ratewitaxVisibility = true;
      calcVisibility = true;
      price_with = 'RatePerPcs';

      if (products.unitQty != null) {
        Wgt_Pcs = "Wgt/Pcs : ${products.unitQty}";
      }

      if (inventoryVoucher == null) {
        cancelBtnVisibility = true;
        addBtnVisibility = true;
        removeBtnVisibility = false;
        updateBtnVisibility = false;
      } else {
        removeBtnVisibility = true;
        updateBtnVisibility = true;
        cancelBtnVisibility = false;
        addBtnVisibility = false;
      }
    }
    addHeadersToSpinner();

    if (inventoryVoucher != null) {
      orderUpdation = true;

      if (inventoryVoucher.itemtype == "PcsToMT") {
        isPcsToMT = true;
      } else if (inventoryVoucher.itemtype == "MT") {
        isMT = true;
      }

      setResultValues(false, 0);

      if (inventoryVoucher.batchNumber != null) {
        price_w_tax_controller.text = inventoryVoucher.batchNumber.toString();
      } else {
        price_w_tax_controller.text = "0";
      }
    }

    //================================================================================================================
    //creating columns
    vouchers = await OurDatabase(table: VoucherColoum().table).getTable(
        where: "documentName", whereArgs: (Document_VM.document).name) as List;
    vouchers.forEach((element) async {
      switch (element['inventoryVoucherColumn']) {
        case 'STOCK':
          stock_controller.text = "0.0";
          stockVisibility = true;
          //   getLiveStock();
          break;
        case 'PRICE_WITH_TAX':
          var taxRate = products.taxRate;
          rate = selling_rate_controller.text != ""
              ? double.parse(selling_rate_controller.text)
              : 0.0;
          var taxprice = rate != 0.0 ? (rate * taxRate) / 100 : 0;
          double priceWtax = rate + taxprice;
          price_w_tax_controller.text = priceWtax.toStringAsFixed(2);
          mrp_controller.text = (products.mrp).toString();
          ratewitaxVisibility = true;
          break;
        case 'SIZE':
          size_controller.text = products.size.toString();
          sizeVisibility = true;
          break;
        case 'UNIT_QUANTITY':
          unit_quantity_controller.text = products.unitQty.toString();
          unitquantityVisibility = true;
          break;
        case 'QUANTITY':
          if (element["enabled"] == 1) {
            if (isBatchExist) {
              quantityEnable = false;
            } else
              quantityEnable = true;
          }
          if (inventoryVoucher != null) {
            double quantity;
            if (enableDynamicUnit) {
              if (unit_type_controller.text == "PcsToMT") {
                try {
                  quantity =
                      (inventoryVoucher.quantity * 1000) / products.stockQty;
                  quantity = double.parse(quantity.toStringAsFixed(2));
                } catch (e) {
                  quantity = 0;
                }
              } else {
                quantity = inventoryVoucher.quantity;
              }
            } else {
              quantity = inventoryVoucher.quantity;
            }

            quantity_controller.text = inventoryVoucher.quantity.toString();
          }
          quantityVisibility = true;
          break;
        case 'SELLING_RATE':
          if (element["enabled"] == 1) {
            if (isBatchExist) {
              sellingRateEnable = false;
            } else
              sellingRateEnable = true;
          }
          if (realTimeProductPriceEnabled) {
            if (inventoryVoucher == null) {
              getLivePrice();
            } else {
              selling_rate_controller.text =
                  inventoryVoucher.sellingRate.toString();
            }
          } else {
            if (selectedPriceLevel != null) {
              productPriceLists =
                  await OurDatabase(table: ProductPriceList().table).getTable(
                      where: "ProductProfilePid",
                      whereArgs: products.pid) as List;
              // print(
              //     "this is............................................. $productPriceLists");
            }

            if (productPriceLists.length > 0) {
              if (inventoryVoucher != null) {
                selling_rate_controller.text =
                    inventoryVoucher.sellingRate.toString();
              } else {
                if (selectedPriceLevel != null) {
                  var sellingrate =
                      double.parse(productPriceLists.first.price.toString());
                  selling_rate_controller.text = sellingrate.toStringAsFixed(2);
                } else {
                  var sellingrate = double.parse(products.price.toString());
                  selling_rate_controller.text = sellingrate.toStringAsFixed(2);
                }
              }
            } else {
              if (inventoryVoucher != null) {
                selling_rate_controller.text =
                    inventoryVoucher.sellingRate.toString();
              } else {
                if (selectedPriceLevel != null) {
                  if (productPriceLists.length > 0) {
                    var sellingrate =
                        double.parse(productPriceLists.first.price.toString());
                    selling_rate_controller.text =
                        sellingrate.toStringAsFixed(2);
                  }
                  selling_rate_controller.text = products.price.toString();
                } else {
                  var sellingrate = double.parse(products.price.toString());
                  selling_rate_controller.text = sellingrate.toStringAsFixed(2);
                }
              }
            }
          }
          rateVisibility = true;
          break;
        case 'FREE_QUANTITY':
          if (element["enabled"] == 1) {
            if (isBatchExist) {
              freeQuantityEnable = false;
            } else
              freeQuantityEnable = true;
          }
          if (inventoryVoucher != null) {
            free_quantity_controller.text = inventoryVoucher.freeQty.toString();
          }
          freequantityVisibility = true;
          break;
        case 'DISCOUNT_PERCENTAGE':
          if (element["enabled"] == 1) {
            if (isBatchExist) {
              discountPerEnable = false;
            } else
              discountPerEnable = true;
          }
          if (inventoryVoucher != null) {
            dis_per_controller.text =
                inventoryVoucher.discountInPercentage.toString();
          }
          discountPerVisibility = true;
          break;
        case 'DISCOUNT_AMOUNT':
          if (element["enabled"] == 1) {
            if (isBatchExist) {
              discountAmtEnable = false;
            } else
              discountAmtEnable = true;
          }
          if (inventoryVoucher != null) {
            dis_amount_controller.text =
                inventoryVoucher.discountInRupees.toString();
          }
          discountRSVisibility = true;
          break;
        case 'MRP':
          double mrp;
          if (!realTimeProductPriceEnabled) {
            mrp = products.mrp;
          }
          if (inventoryVoucher != null) {
            mrp = inventoryVoucher.mrp;
          }
          if (mrp == 0) {
            mrp_controller.text = "";
          } else {
            mrp_controller.text = mrp.toString();
          }
          if (element["enabled"] == 1) {
            if (isBatchExist) {
              mrpEnable = false;
            } else
              mrpEnable = true;
          }
          mrpVisibility = true;
          break;
        case 'TAX_PERCENTAGE':
          tax_amount_controller.text = products.taxRate.toString();
          taxVisibility = true;
          break;
        case 'OFFER_STRING':
          offerVisibility = true;
          break;
        case 'REMARKS':
          if (element["enabled"] == 1) {
            if (isBatchExist) {
              remarkEnable = false;
            } else
              remarkEnable = true;
          }
          if (inventoryVoucher != null) {
            remark_controller.text = inventoryVoucher.remarks;
          }
          remarkVisibility = true;
          break;
        case "TOTAL":
          if (inventoryVoucher != null) {
            total_amount_controller.text =
                inventoryVoucher.totalAmount.toString();
          }
          break;
        default:
          break;
      }
    });

    Widget cartitems() {
      var b;
      InventoryVoucherFormScreenState.selectedndex.forEach((element) {
        b = element;
      });
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
              'Name: ${InventoryVoucherForm_VM.inventoryVoucherDetailsList[b].itemName}'),
          Text(
              '${InventoryVoucherForm_VM.inventoryVoucherDetailsList[b].refQuantity}(Qty)X${InventoryVoucherForm_VM.inventoryVoucherDetailsList[b].sellingRate}(TaxRate)=${InventoryVoucherForm_VM.inventoryVoucherDetailsList[b].totalAmount}'),
          Text(
              'Free Qty : ${InventoryVoucherForm_VM.inventoryVoucherDetailsList[b].freeQty}')
        ],
      );
    }

    addtoinventoryvoucherdetails() {
      inventoryVoucherforcart.itemName = products.name;
      inventoryVoucherforcart.itemPid = products.pid;
      inventoryVoucherforcart.sku = products.sku;
      inventoryVoucherforcart.unitQty = products.unitQty;
      inventoryVoucherforcart.productCategoryPid = products.productCategoryPid;
      inventoryVoucherforcart.productCategoryName =
          products.productCategoryName;
      inventoryVoucherforcart.inventoryVoucherHeaderDate = Document_VM.date1;
      inventoryVoucherforcart.inventoryVoucherHeaderId =
          Document_VM.taskExecution.id;
      inventoryVoucherforcart.rateOfVat = products.taxRate;
      inventoryVoucherforcart.description = products.description;
      inventoryVoucherforcart.isShowDetails = false;

      addToInvetoryDetails(inventoryVoucherforcart); //007
    }

    Widget cancelBtn = Visibility(
        visible: cancelBtnVisibility,
        child: GestureDetector(
            onTap: (() {
              Navigator.pop(InventoryVoucherFormScreenState.contextt);
            }),
            child: Container(
                alignment: Alignment.center,
                height: 10.0.w,
                color: Colors.blueGrey[200],
                child: Text('CANCEL'))));
    Widget addBtn = Visibility(
        visible: addBtnVisibility,
        child: GestureDetector(
            onTap: (() async {
              FocusScopeNode currentFocus =
                  FocusScope.of(InventoryVoucherFormScreenState.contextt);

              addtoinventoryvoucherdetails(); //003

              if (quantity_controller.text == '') {
                quantity_controller.text = "0.0";
              }
              if (free_quantity_controller.text == '') {
                free_quantity_controller.text = '0.0';
              }
              if (totalrate != 0.00 && totalrate != null) {
                totalpriceslist.add(double.parse(totalrate.toStringAsFixed(2)));
              }

              InventoryVoucherFormScreenState.cartlist.add(cartitems()); //002
              Navigator.pop(InventoryVoucherFormScreenState.contextt);
              InventoryVoucherForm_VM().getProductProfile();
            }),
            child: Container(
                alignment: Alignment.center,
                height: 10.0.w,
                color: Colors.blue,
                child: Text('ADD'))));
    Widget removeBtn = Visibility(
        visible: removeBtnVisibility,
        child: GestureDetector(
            onTap: (() {}),
            child: Container(
                alignment: Alignment.center,
                height: 10.0.w,
                color: Colors.blueGrey[200],
                child: Text('REMOVE'))));
    Widget updateBtn = Visibility(
        visible: updateBtnVisibility,
        child: GestureDetector(
            onTap: (() {}),
            child: Container(
                alignment: Alignment.center,
                height: 10.0.w,
                color: Colors.blueGrey[200],
                child: Text('UPDATE'))));

    List<Widget> getItems() {
      bool stock = true;

      if (stock_controller.text == "0.0") {
        stock = true;
      } else if (stock_controller.text != "0.0") {
        stock = false;
      }

      return [
        Visibility(
            visible: stockVisibility,
            child: Container(
                color: Colors.orange[700],
                padding: EdgeInsets.all(1.0.h),
                width: double.infinity,
                child: Row(children: [
                  Expanded(
                      child:
                          Text('stock', style: TextStyle(color: Colors.white))),
                  Expanded(
                      child: TextField(
                          keyboardType: TextInputType.number,
                          controller: stock_controller,
                          enabled: false,
                          style: TextStyle(
                              color: stock ? Colors.red : Colors.white)))
                ]))),
        Visibility(
            visible: ratewitaxVisibility,
            child: Container(
                color: Colors.orange[700],
                padding: EdgeInsets.all(1.0.h),
                width: double.infinity,
                child: Row(children: [
                  Expanded(
                      child: Text(price_with,
                          style: TextStyle(color: Colors.white))),
                  Expanded(
                      child: TextField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(border: InputBorder.none),
                    style: TextStyle(color: Colors.white),
                    controller: price_w_tax_controller,
                    enabled: false,
                  ))
                ]))),
        Visibility(
            visible: sizeVisibility,
            child: Container(
                color: Colors.orange[700],
                padding: EdgeInsets.all(1.0.h),
                width: double.infinity,
                child: Row(children: [
                  Expanded(
                      child:
                          Text('size', style: TextStyle(color: Colors.white))),
                  Expanded(
                      child: Text('${products.size}',
                          style: TextStyle(color: Colors.white)))
                ]))),
        Visibility(
            visible: unitquantityVisibility,
            child: Container(
                color: Colors.orange[700],
                padding: EdgeInsets.all(1.0.h),
                width: double.infinity,
                child: Row(children: [
                  Expanded(
                      child: Text('unitQuatity',
                          style: TextStyle(color: Colors.white))),
                  Expanded(
                      child: Text('${products.unitQty}',
                          style: TextStyle(color: Colors.white)))
                ]))),
        Visibility(
            visible: true,
            child: Container(
                color: Colors.orange[700],
                padding: EdgeInsets.all(1.0.h),
                width: double.infinity,
                child: Row(children: [
                  Expanded(
                      child: Text('$Wgt_Pcs:',
                          style: TextStyle(color: Colors.white))),
                  Expanded(
                      child: Text('Unit Type',
                          style: TextStyle(color: Colors.white))),
                  Expanded(
                      child: TextField(
                          keyboardType: TextInputType.number,
                          enabled: false,
                          controller: unit_type_controller,
                          style: TextStyle(color: Colors.white))),
                  PopupMenuButton(
                    icon: Icon(Icons.keyboard_arrow_down_rounded,
                        color: Colors.white),
                    padding: EdgeInsets.zero,
                    onSelected: ((value) {
                      unit_type_controller.text = value;
                      onUnitTypeSelected();
                    }),
                    itemBuilder: (BuildContext context) {
                      return headers.map((String choice) {
                        return PopupMenuItem<String>(
                          value: choice,
                          child: Text(choice),
                        );
                      }).toList();
                    },
                  )
                ]))),
        Visibility(
            visible: quantityVisibility,
            child: Container(
                padding: EdgeInsets.only(left: 1.0.w, right: 1.0.w),
                child: TextField(
                  keyboardType: TextInputType.number,
                  onChanged: (value) {
                    if (value.isNotEmpty) {
                      quantity = double.parse(value);

                      var taxRate = products.taxRate;
                      rate = double.parse(selling_rate_controller.text);
                      var taxprice = (rate * taxRate) / 100;
                      double priceWtax = rate + taxprice;
                      price_w_tax_controller.text =
                          priceWtax.toStringAsFixed(2);
                      taxrate = priceWtax;
                      inventoryVoucherforcart.sellingRate = taxrate;
                      totalrate = quantity * taxrate;
                      inventoryVoucherforcart.totalAmount = totalrate;

                      total_amount_controller.text =
                          totalrate.toStringAsFixed(2);
                    } else {
                      total_amount_controller.text = '0.0';
                    }
                  },
                  controller: quantity_controller,
                  decoration: InputDecoration(
                      labelText: 'Quantity',
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey)),
                      labelStyle: TextStyle(color: Colors.orange)),
                ))),
        Visibility(
            visible: rateVisibility,
            child: Container(
                padding: EdgeInsets.only(left: 1.0.w, right: 1.0.w),
                child: TextField(
                  keyboardType: TextInputType.number,
                  onChanged: (value) {
                    var taxRate = products.taxRate;
                    rate = double.parse(selling_rate_controller.text);

                    var taxprice = (rate * taxRate) / 100;
                    double priceWtax = rate + taxprice;
                    price_w_tax_controller.text = priceWtax.toStringAsFixed(2);
                    taxrate = priceWtax;

                    if (value.isNotEmpty) {
                      total_amount_controller.text =
                          (double.parse(price_w_tax_controller.text) *
                                  double.parse(quantity_controller.text))
                              .toStringAsFixed(2);
                      return taxrate;
                    } else {
                      total_amount_controller.text = '0.0';
                    }
                  },
                  controller: selling_rate_controller,
                  decoration: InputDecoration(
                      labelText: 'Rate',
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.orange)),
                      labelStyle: TextStyle(color: Colors.orange)),
                ))),
        Visibility(
            visible: freequantityVisibility,
            child: Container(
                padding: EdgeInsets.only(left: 1.0.w, right: 1.0.w),
                child: TextField(
                  onChanged: (value) {
                    if (value.isNotEmpty) {
                      inventoryVoucherforcart.freeQty = double.parse(value);
                    } else {
                      inventoryVoucherforcart.freeQty = 0.0;
                    }
                  },
                  keyboardType: TextInputType.number,
                  controller: free_quantity_controller,
                  decoration: InputDecoration(
                      labelText: 'Free Quantity',
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.orange)),
                      labelStyle: TextStyle(color: Colors.orange)),
                ))),
        Visibility(
            visible: discountPerVisibility,
            child: Container(
                padding: EdgeInsets.only(left: 1.0.w, right: 1.0.w),
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: dis_per_controller,
                  decoration: InputDecoration(
                      labelText: 'Discount(%)',
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.orange)),
                      labelStyle: TextStyle(color: Colors.orange)),
                ))),
        Visibility(
            visible: discountRSVisibility,
            child: Container(
                padding: EdgeInsets.only(left: 1.0.w, right: 1.0.w),
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: dis_amount_controller,
                  decoration: InputDecoration(
                      labelText: 'Discount(RS)',
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.orange)),
                      labelStyle: TextStyle(color: Colors.orange)),
                ))),
        Visibility(
            visible: offerVisibility,
            child: Container(
                padding: EdgeInsets.only(left: 1.0.w, right: 1.0.w),
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: offer_string_controller,
                  decoration: InputDecoration(
                      labelText: 'Offers',
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.orange)),
                      labelStyle: TextStyle(color: Colors.orange)),
                ))),
        Visibility(
            visible: mrpVisibility,
            child: Container(
                padding: EdgeInsets.only(left: 1.0.w, right: 1.0.w),
                child: TextField(
                  keyboardType: TextInputType.number,
                  onChanged: (value) {
                    if (value.isNotEmpty) {
                      inventoryVoucherforcart.mrp = double.parse(value);
                    } else {
                      inventoryVoucherforcart.mrp = 0.0;
                    }
                  },
                  controller: mrp_controller,
                  decoration: InputDecoration(
                      labelText: 'MRP',
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.orange)),
                      labelStyle: TextStyle(color: Colors.orange)),
                ))),
        Visibility(
            visible: taxVisibility,
            child: Container(
                padding: EdgeInsets.only(left: 1.0.w, right: 1.0.w),
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: tax_amount_controller,
                  decoration: InputDecoration(
                      labelText: 'TAX %',
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.orange)),
                      labelStyle: TextStyle(color: Colors.orange)),
                ))),
        Visibility(
            visible: remarkVisibility,
            child: Container(
                padding: EdgeInsets.only(left: 1.0.w, right: 1.0.w),
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: remark_controller,
                  decoration: InputDecoration(
                      labelText: 'Remarks',
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.orange)),
                      labelStyle: TextStyle(color: Colors.orange)),
                ))),
      ];
    }

    //================================================================================================================

    List<Widget> widget = getItems();
    widget.add(SizedBox(height: 5.0.h));

    showDialog(
      context: InventoryVoucherFormScreenState.contextt,
      builder: (BuildContext buildContext) {
        getLiveStock();
        return Scaffold(
            backgroundColor: Colors.transparent,
            body: Container(
                color: Colors.white,
                margin: EdgeInsets.all(1.0.h),
                child: Column(children: [
                  Container(
                      color: Colors.orange,
                      child: Column(children: [
                        ListTile(
                            title: Text(products.name,
                                style: TextStyle(color: Colors.white)),
                            tileColor: Colors.orange)
                      ])),
                  Expanded(
                      child: new ListView(shrinkWrap: true, children: widget)),
                  Visibility(
                      visible: calcVisibility,
                      child: ListTile(
                          tileColor: Colors.grey[200],
                          title: Container(
                              child: Row(children: [
                            Expanded(
                                child: Text('MT',
                                    style: TextStyle(color: Colors.grey))),
                            Expanded(
                                child: TextField(
                                    controller: mt_calc_controller,
                                    enabled: false,
                                    style: TextStyle(color: Colors.grey))),
                            Expanded(
                                child: Text('Pcs',
                                    style: TextStyle(color: Colors.grey))),
                            Expanded(
                                child: TextField(
                                    controller: psc_calc_controller,
                                    enabled: false,
                                    style: TextStyle(color: Colors.grey))),
                          ])))),
                  ListTile(
                      title: Container(
                          child: Row(children: [
                        Expanded(
                            child: Text('Total ',
                                style: TextStyle(color: Colors.grey))),
                        Expanded(
                            child: TextField(
                          controller: total_amount_controller,
                          enabled: false,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              labelText: total_amount_controller.text == '0.0'
                                  ? '0.0'
                                  : null),
                        ))
                      ])),
                      tileColor: Colors.grey[200]),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Expanded(
                            child: inventoryVoucher == null
                                ? cancelBtn
                                : removeBtn),
                        Expanded(
                            child:
                                inventoryVoucher == null ? addBtn : updateBtn),
                      ])
                ])));
      },
      barrierDismissible: true,
      barrierLabel:
          MaterialLocalizations.of(InventoryVoucherFormScreenState.contextt)
              .modalBarrierDismissLabel,
      barrierColor: Colors.black,
    );
  }

  //================================================================================================================
  bool checkProductProfileExist(var pid) {
    return false;
  }

  double getStock() {
    double stock = 0;
    try {
      stock = double.parse(selling_rate_controller.text);
    } catch (e) {
      stock = 0;
    }
    return stock;
  }

  addHeadersToSpinner() {
    if (products.sku == 'MT') {
      headers.add(' MT ');
      headers.add(' Pcs ');
      headers.add(' PcsToMT ');
    } else {
      headers.add(' Pcs ');
      headers.add(' MT ');
      headers.add(' PcsToMT ');
    }
    unit_type_controller.text = headers.first;
  }

  getLiveStock() async {
    print('*****');
    var dynamicFormula = InventoryVoucherFormScreenState.dynamicFormulalist[0];

    var mp = {
      'documentPid': dynamicFormula.documentPid,
      'productPid': products.pid
    };

    var response = await WebServer().get_with_query(url: 'stock', data: mp)
        as http.Response;
    if (response.statusCode == 200) {
      stock_controller.text = response.body;
    } else {
      Fluttertoast.showToast(msg: " no internet connection ");
    }
  }

  getLivePrice() async {
    print('*****');
    var mp = {
      'productPid': products.pid,
      'accountPid': DocumentPageState.accountProfile.pid
    };
    // print(mp);
    var response = await WebServer()
        .get_with_query(url: 'product/get-price', data: mp) as http.Response;

    if (response.statusCode == 200) {
      // print(response.statusCode);
    }
  }

  addToInvetoryDetails(InventoryVoucherDetails inventoryVoucherDet) async {
    bool belowPrice = getPriceStatus();
    bool status = getNegativeStockStatus();
    if (InventoryVoucherFormScreenState.document.name == "Vansales return") {
      status = false;
    }
    if (status == true) {
      Fluttertoast.showToast(msg: "not enough stock!!");
    } else if (belowPrice) {
      Fluttertoast.showToast(
          msg: "Lower price not accepted. Enter a valid price");
    } else {
      if (getQuantity() > 0 || getFreeQuantity() > 0) {
        // if (!isOrderTakingDialogShow) {
        //   dismiss();
        // }

        // var productCategory1 = await OurDatabase(table: ProductCategory().table)
        //     .getTable(where: 'pid', whereArgs: products.productCategoryPid);
        // print(productCategory1);
        // var productgroup1 = await OurDatabase(table: ProductGroup().table)
        //     .getTable(where: 'pid', whereArgs: products.name);

        // productCategory = ProductCategory().fromMap(productCategory1);
        InventoryVoucherForm_VM().onAdd(
          generateInventoryVoucherDetails(inventoryVoucherDet),
          products,
        );
      } else {
        Fluttertoast.showToast(msg: "please enter a valid quantity");
      }
    }
  }

  // validateQuantity() {
  //   if (quantity_controller.text == null || quantity_controller.text == '') {
  //     showDialog(
  //         context: contextt,
  //         builder: (context) {
  //           return AlertDialog(
  //             title: Text('Invalid Data'),
  //             content: Text('Quantity must not be null'),
  //             actions: [
  //               FlatButton(
  //                 child: Text('OK'),
  //                 onPressed: () {
  //                   Navigator.of(contextt).pop();
  //                 },
  //               )
  //             ],
  //           );
  //         });
  //   } else {
  //     (InventoryVoucherFormScreenState.contextt as StatefulElement)
  //         .state
  //         .setState(() {});
  //     Navigator.pop(contextt);
  //     print('added');
  //   }
  // }

  bool getPriceStatus() {
    if (belowPriceLevel) {
      double sellingRate = getSellingRate();
      double priceRange = 0;

      if (productPriceLists.isNotEmpty) {
        priceRange = productPriceLists[0].price;
      }

      if (sellingRate < priceRange) {
        return true;
      }
    }
    return false;
  }

  double getSellingRate() {
    double sellingRate = 0;
    double tax = 0;
    double disInRs = 0;
    try {
      if (rateWithoutCalc) {
        try {
          tax = products.taxRate;
        } catch (e) {
          tax = 0;
        }

        try {
          disInRs = getDiscountInRupees();
        } catch (e) {
          tax = 0;
        }
        double rateWithTax = double.parse(selling_rate_controller.text);
        rateWithTax = rateWithTax - disInRs;
        sellingRate = (rateWithTax * 100) / (100 + tax);
        sellingRate = double.parse(sellingRate.toStringAsFixed(2));
      } else {
        sellingRate = double.parse(selling_rate_controller.text);
      }
    } catch (e) {
      sellingRate = 0;
    }
    return sellingRate;
  }

  double getPcsCalc() {
    double pcs;
    try {
      pcs = double.parse(psc_calc_controller.text);
    } catch (e) {
      pcs = 0;
    }
    return pcs;
  }

  setResultValues(bool isNewProduct, double value) {
    String type;
    double calc;
    double val;

    if (isNewProduct) {
      val = value;
      type = unit_type_controller.text;
    } else {
      val = inventoryVoucher.quantity;
      type = inventoryVoucher.itemtype;
    }

    if (type == "PcsToMT") {
      if (isNewProduct) {
        try {
          calc = (val * products.unitQty) / 1000;
        } catch (e) {
          calc = 0;
        }

        setUnitValues(val, calc);
      } else {
//                onUpdate, qty is MT.so we change that to pcs
        try {
          calc = (val * 1000) / products.unitQty;
        } catch (e) {
          calc = 0;
        }
        setUnitValues(calc, val);
      }
    } else if (type == "Pcs") {
      try {
        calc = (val * products.unitQty) / 1000;
      } catch (e) {
        calc = 0;
      }
      setUnitValues(val, calc);
    } else {
      try {
        calc = (val * 1000) / products.unitQty;
      } catch (e) {
        calc = 0;
      }
      setUnitValues(calc, val);
    }
  }

  setUnitValues(double val, double calc) {
    try {
      psc_calc_controller.text = val.toStringAsFixed(3);
    } catch (e) {
      psc_calc_controller.text = "0";
    }

    try {
      mt_calc_controller.text = val.toStringAsFixed(3);
    } catch (e) {
      mt_calc_controller.text = "0";
    }
  }

  onUnitTypeSelected() {
    double pcs, mt;

    if (!orderUpdation) {
      if (unit_type_controller.text == "Pcs") {
//                 sp_unit_type.setSelected(true);

        if (isPcsToMT) {
          setTotalCalc(getPcsCalc());
        } else {
          try {
            pcs = double.parse(mt_calc_controller.text);
          } catch (e) {
            pcs = 0;
          }

          try {
            mt = (getQuantity() * products.unitQty) / 1000;
          } catch (e) {
            mt = 0;
          }

          setUnitValues(pcs, mt);
        }
        isPcsToMT = false;
        isMT = false;
      } else if (unit_type_controller.text == "MT") {
//                 sp_unit_type.setSelected(true);

        if (isPcsToMT) {
          try {
            mt = double.parse(psc_calc_controller.text);
          } catch (e) {
            mt = 0;
          }

          try {
            pcs = (getQuantity() * 1000) / products.unitQty;
          } catch (e) {
            pcs = 0;
          }

          setUnitValues(pcs, mt);
          setTotalCalc(mt);
        } else {
          try {
            mt = double.parse(psc_calc_controller.text);
          } catch (e) {
            mt = 0;
          }

          try {
            pcs = (getQuantity() * 1000) / products.unitQty;
          } catch (e) {
            pcs = 0;
          }
          setUnitValues(pcs, mt);
        }
        isPcsToMT = false;
        isMT = true;
      } else if (unit_type_controller.text == "pcsToMT") {
//                 sp_unit_type.setSelected(true);

        try {
          pcs = double.parse(mt_calc_controller.text);
        } catch (e) {
          pcs = 0;
        }

//                pcsToMT qty is in MT.so change to pcs
        try {
          mt = (pcs * products.unitQty) / 1000;
        } catch (e) {
          mt = 0;
        }

        if (isMT) {
          setUnitValues(pcs, mt);
          setTotalCalc(mt);
        } else {
          setTotalCalc(pcs);
        }

        isPcsToMT = true;
        isMT = false;
      } else {
        if (isLoadNewProduct) {
//                     dismiss();

//                     if (defaultLoadingFrom.equalsIgnoreCase("Product Group")) {
//                         ProductGroup productGroup = allProductGroupList.get(pos);
//                         listener.onItemSelection(null, productGroup);

//                     } else {

//                         ProductCategory productCategory = allProductCategoryList.get(pos);
//                         listener.onItemSelection(productCategory, null);
//                     }
        } else {
          isLoadNewProduct = true;
        }
      }

      setRatePerPcs();
    }
    orderUpdation = false;
  }

  setRatePerPcs() {
    if (enableDynamicUnit) {
      String type = unit_type_controller.text;

      if (type == "Pcs") {
        price_w_tax_controller.text = getSellingRate().toString();
      } else {
        double ratePerPcs = 0;
        double tot = 0;

        if (isPcsToMT) {
          tot = getQuantity() * getSellingRate();
        } else {
          tot = getTotal();
        }

        try {
          if (getPcsCalc() > 0) {
            ratePerPcs = (tot / getPcsCalc());
          }
        } catch (e) {
          ratePerPcs = 0;
        }
        price_w_tax_controller.text = ratePerPcs.toStringAsFixed(3);
      }
    }
  }

  setTotalCalc(double val) {
    double total = val * getSellingRate();
    total_amount_controller.text = total.toStringAsFixed(2);
  }

  double getDiscountInRupees() {
    double discountInRupees = 0;
    try {
      discountInRupees = double.parse(dis_amount_controller.text);
    } catch (e) {
      discountInRupees = 0;
    }
    return discountInRupees;
  }

  getNegativeStockStatus() {
    if (preventNegativeStock == 1) {
      double stockqty = getStock();
      double qty = getQuantity();
      if (qty > stockqty) {
        return true;
      }
    }
    return false;
  }

  getQuantity() {
    if (InventoryVoucherFormScreenState.document.name == "Vansales return") {
      if (double.parse(quantity_controller.text) <= inventoryVoucher.quantity) {
        quantity = double.parse(quantity_controller.text);
      } else {
        Fluttertoast.showToast(
            msg:
                'Please input quantity Between 1 &" ${inventoryVoucher.quantity} "only');
        quantity_controller.text = inventoryVoucher.quantity.toString();
        quantity = inventoryVoucher.quantity;
      }
    } else {
      return quantity = double.parse(quantity_controller.text);
    }

    if (enableDynamicUnit) {
      quantity = getQtyByType(quantity);
    }
  }

  double getQtyByType(double quantity) {
    String type = unit_type_controller.text;
    if (type == "PcsToMT") {
      try {
        quantity = (quantity * products.unitQty) / 1000;
      } catch (e) {
        quantity = 0;
      }
    }
    return double.parse(quantity.toStringAsFixed(3));
  }

  getFreeQuantity() {
    double freeQty = 0;
    try {
      freeQty = double.parse(free_quantity_controller.text);
    } catch (e) {
      freeQty = 0;
    }
    return freeQty;
  }

  getDiscountpercentage() {
    double discountInPercentage = 0;
    try {
      discountInPercentage = double.parse(dis_per_controller.text);
    } catch (e) {
      discountInPercentage = 0;
    }
    return discountInPercentage;
  }

  getOfferString() {
    return offer_string_controller.text;
  }

  getRemarks() {
    return remark_controller.text;
  }

  getTotal() {
    double total = 0;
    try {
      total = double.parse(total_amount_controller.text);
    } catch (e) {
      total = 0;
    }
    return total;
  }

  double getMrp() {
    double sellingRate = 0;
    try {
      sellingRate = double.parse(mrp_controller.text);
    } catch (e) {
      sellingRate = 0;
    }

    return sellingRate;
  }

  InventoryVoucherDetails generateInventoryVoucherDetails(
      InventoryVoucherDetails inventoryVoucherDet) {
    InventoryVoucherDetails inventoryVoucherDetails = InventoryVoucherDetails();

    if (inventoryVoucherDet == null) {
      inventoryVoucherDetails.itemName = products.name;
      inventoryVoucherDetails.itemPid = products.pid;
      inventoryVoucherDetails.sku = products.sku;
      inventoryVoucherDetails.unitQty = products.unitQty;
      inventoryVoucherDetails.productCategoryPid = products.productCategoryPid;
      inventoryVoucherDetails.productCategoryName =
          products.productCategoryName;

      inventoryVoucherDetails.rateOfVat = products.taxRate;
      inventoryVoucherDetails.description = products.description;
      inventoryVoucherDetails.isShowDetails = false;
      inventoryVoucherDetails.inventoryVoucherHeaderDate = Document_VM.date1;

      //TODO
      inventoryVoucherDetails.stockLocationName = "";

      if (products.sku != null) {
        inventoryVoucherDetails.sku = products.sku;
      }
      return inventoryVoucherDetails;
    } else {
      inventoryVoucherDetails.itemName = inventoryVoucherDet.itemName;
      inventoryVoucherDetails.itemPid = inventoryVoucherDet.itemPid;
      inventoryVoucherDetails.inventoryVoucherHeaderId =
          inventoryVoucherDet.inventoryVoucherHeaderId;
      if (inventoryVoucherDet.sku != null) {
        inventoryVoucherDetails.sku = inventoryVoucherDet.sku;
      }
      inventoryVoucherDetails.productCategoryPid =
          inventoryVoucherDet.productCategoryPid;
      inventoryVoucherDetails.productCategoryName =
          inventoryVoucherDet.productCategoryName;
      inventoryVoucherDetails.rateOfVat = inventoryVoucherDet.rateOfVat;
      inventoryVoucherDetails.isShowDetails = inventoryVoucherDet.isShowDetails;

      if (inventoryVoucherDet.pidFromServer != null) {
        inventoryVoucherDetails.pidFromServer =
            inventoryVoucherDet.pidFromServer;
      }
      if (inventoryVoucherDet.isInventorySearch != null) {
        inventoryVoucherDetails.isInventorySearch =
            inventoryVoucherDet.isInventorySearch;
      }
      if (inventoryVoucherDet.referenceInventoryVoucherHeaderPid != null) {
        inventoryVoucherDetails.referenceInventoryVoucherHeaderPid =
            inventoryVoucherDet.referenceInventoryVoucherHeaderPid;
      }
      if (inventoryVoucherDet.referenceInventoryVoucherDetailId != null) {
        inventoryVoucherDetails.referenceInventoryVoucherDetailId =
            inventoryVoucherDet.referenceInventoryVoucherDetailId;
      }
      if (inventoryVoucherDet.isBatchAllocated != null) {
        inventoryVoucherDetails.isBatchAllocated =
            inventoryVoucherDet.isBatchAllocated;
      }
      if (inventoryVoucherDet.inventoryVoucherHeaderDate != null) {
        inventoryVoucherDetails.inventoryVoucherHeaderDate =
            inventoryVoucherDet.inventoryVoucherHeaderDate;
      } else {
        // var date = new DateTime.now();
        // var date1 =
        //     "${date.year}-0${date.month}-0${date.day}T${date.hour}:${date.minute}:${date.second}.${date.millisecond}${date.microsecond}";
        inventoryVoucherDetails.inventoryVoucherHeaderDate = Document_VM.date1;
      }
    }
    if (enableDynamicUnit) {
      String type = unit_type_controller.text;
      inventoryVoucherDetails.itemtype = type;
      inventoryVoucherDetails.pieces = getPcsCalc();
      /** Here We are using BatchNumber to save RatePerPcs.*/
      if (price_w_tax_controller.text != null) {
        inventoryVoucherDetails.batchNumber =
            double.parse(price_w_tax_controller.text);
      }
      if (type == "Pcs") {
        inventoryVoucherDetails.ratePerPiece =
            double.parse(getSellingRate().toStringAsFixed(2));
      } else {
        double ratePerPcs = 0;

        try {
          if (getPcsCalc() > 0) {
            ratePerPcs = (getTotal() / getPcsCalc());
          }
        } catch (e) {
          ratePerPcs = 0;
        }
        inventoryVoucherDetails.ratePerPiece =
            double.parse(ratePerPcs.toStringAsFixed(3));
      }
    }
    inventoryVoucherDetails.totalAmount = getTotal();

    vouchers.forEach((element) {
      switch (element['inventoryVoucherColumn']) {
        case 'STOCK':
          inventoryVoucherDetails.stock = getStock();
          break;
        case 'PRICE_WITH_TAX':
          break;
        case 'SIZE':
          inventoryVoucherDetails.size = products.size;
          break;
        case 'UNIT_QUANTITY':
          inventoryVoucherDetails.unitQty = products.unitQty;
          break;
        case 'QUANTITY':
          inventoryVoucherDetails.quantity = getQuantity();
          inventoryVoucherDetails.refQuantity =
              inventoryVoucherDetails.quantity;
          break;
        case 'SELLING_RATE':
          inventoryVoucherDetails.sellingRate = getSellingRate();
          break;
        case 'FREE_QUANTITY':
          inventoryVoucherDetails.freeQty = getFreeQuantity();
          break;
        case 'DISCOUNT_PERCENTAGE':
          inventoryVoucherDetails.discountInPercentage =
              getDiscountpercentage();
          break;
        case 'DISCOUNT_AMOUNT':
          inventoryVoucherDetails.discountInRupees = getDiscountInRupees();
          break;
        case 'MRP':
          inventoryVoucherDetails.mrp = getMrp();
          break;
        case 'TAX_PERCENTAGE':
          inventoryVoucherDetails.rateOfVat = products.taxRate;
          break;
        case 'OFFER_STRING':
          inventoryVoucherDetails.offer = getOfferString();
          break;
        case 'REMARKS':
          inventoryVoucherDetails.remarks = getRemarks();
          break;
        case "TOTAL":
          break;
        default:
          break;
      }
    });
    // print(inventoryVoucherDetails);
    return inventoryVoucherDetails;
  }
}

class AccountingvoucherForm_VM {
  List<String> li = [];
  initializeUi() {
    try {
      if (Document_VM.document.mode == 'ALL') {
        String accountingVoucherMode = '';
        if (accountingVoucherMode == 'Cash') {
          li.add('Cash');
          li.add('Cheque');
          li.add('RTGS');
        } else if (accountingVoucherMode == 'Cheque') {
          li.add('Cheque');
          li.add('RTGS');
          li.add('Cash');
        } else {
          li.add('RTGS');
          li.add('Cheque');
          li.add('Cash');
        }
      } else {
        li.add(Document_VM.document.mode);
      }
    } catch (e) {
      String accountingVoucherMode = '';
      if (accountingVoucherMode == 'Cash') {
        var list = ['Cash', 'Cheque', 'RTGS'];
        li.add('Cash');
        li.add('Cheque');
        li.add('RTGS');
      } else if (accountingVoucherMode == 'Cheque') {
        li.add('Cheque');
        li.add('RTGS');
        li.add('Cash');
      } else {
        li.add('RTGS');
        li.add('Cheque');
        li.add('Cash');
      }
    }
    AccountingVoucherFormScreenState.modeList = li;
    (AccountingVoucherFormScreenState.contextt as StatefulElement)
        .state
        .setState(() {});
  }

  setDrawrHeader() {
    if (AccountingVoucherFormScreenState.document.activityAccount == "By") {
      AccountingVoucherFormScreenState.header = "Order";
      (AccountingVoucherFormScreenState.contextt as StatefulElement)
          .state
          .setState(() {});
    } else
      AccountingVoucherFormScreenState.header = "Payment";
    (AccountingVoucherFormScreenState.contextt as StatefulElement)
        .state
        .setState(() {});
  }
}
