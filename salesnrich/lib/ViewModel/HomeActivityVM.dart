// ignore_for_file: deprecated_member_use

import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:salesnrich/Controller/preferenceOperation.dart';
import 'package:salesnrich/Model/Tables.dart';
import 'package:salesnrich/Model/database.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:salesnrich/View/document.dart';
import 'package:salesnrich/View/home_Activity.dart';
import 'package:salesnrich/main.dart';
import 'package:sizer/sizer.dart';

//class homeactivity
class HomeActivity_VM {
  dynamic employee_name() async {
    var table = User().table;
    var list = await OurDatabase(table: User().table).getTables() as List;
    var mp = list[0] as Map<String, dynamic>;
    return {'EmployeeName': mp['employeeName']};
  }
}

//class of my plan and activities page
class TabMyPlanActivityFragmentState_VM {
  dynamic getActivities() async {
    var table = ExecutiveActivity().table;
    var list =
        await OurDatabase(table: ExecutiveActivity().table).getTables() as List;
    print('this is the list excecutivity activty $list');
    List<Widget> retuningWidgets = List<Widget>();
    list.forEach((element) {
      var mp = element;
      print('this is the list emp $mp');

      retuningWidgets.add(getsingleActivity(ExecutiveActivity().fromMap(mp)));
    });
    return retuningWidgets;
  }

  Widget getsingleActivity(mp) {
    print('this is the list getsingleactivity data $mp');

    Widget gettext() {
      return Expanded(
          child: Container(
              padding: EdgeInsets.only(left: 20, right: 30),
              child: Column(children: [
                Text(mp.name),
                Divider(thickness: 2),
                Row(children: [
                  Text('Target'),
                  Expanded(child: Spacer()),
                  Text('${mp.target}')
                ]),
                Row(children: [
                  Text('Acheived'),
                  Expanded(child: Spacer()),
                  Text('${mp.achieved}')
                ]),
                Row(children: [
                  Text('Today'),
                  Expanded(child: Spacer()),
                  Text('${mp.today}')
                ]),
              ])));
    }

    return GestureDetector(
        onTap: () {
          DocumentPageState.excutiveActivity = mp;
          AccountProfileScreen_State.excutiveActivity = mp;
          activityClicked(mp.pid);
        },
        child: Container(
            color: Colors.white,
            margin: EdgeInsets.only(bottom: 5),
            padding: EdgeInsets.all(10),
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              CircularPercentIndicator(
                  radius: 80.0,
                  lineWidth: 6.0,
                  animation: true,
                  percent: 0.0,
                  progressColor: Colors.orange,
                  center: new Text("0.0%")),
              gettext(),
              FloatingActionButton(
                  onPressed: (() {
                    activityClicked(mp.pid);
                  }),
                  backgroundColor: Colors.orange,
                  child: Icon(Icons.add))
            ])));
  }

  void activityClicked(var pid) {
    AccountProfileScreen_State.pid = pid;
    Navigator.push(TabMyPlanActivityFragmentState.contextt,
        MaterialPageRoute(builder: (context) => AccountProfileScreen()));
  }
}

class AccountProfile_VM {
  static List items = [];
  int i = 0;

  List territories = [];

  getAccounts(var pid) async {
    var table = ActivityAccountType().table;
    var list = await OurDatabase(table: table).getTables() as List;
    var li = await OurDatabase(table: ActivityAccountType().table)
        .getTable(where: "ActivityPid", whereArgs: pid) as List;
    print(
        'this is the activityaccounttype pid data ${(ActivityAccountType().fromMap(li[0])).pid}');

    getAccountProfile((ActivityAccountType().fromMap(li[0])).pid);
  }

  getAccountProfile(var pid) async {
    items = [];
    AccountProfileScreen_State.items = [];
    print('Our AccountProfile Pid : $pid');
    var li = await OurDatabase(table: AccountProfile().table)
        .getTable(where: "accountTypePid", whereArgs: pid) as List;
    li.forEach((element) {
      items.add(element as Map<String, dynamic>);
    });

    if (AccountProfileScreen_State.excutiveActivity.hasDefaultAccount == 1) {
      onAccountProfileClicked(items[1]);
    } else {
      items.sort((a, b) {
        var mp1 = a as Map<String, dynamic>;
        var mp2 = b as Map<String, dynamic>;
        return mp1['name'].toString().compareTo(mp2['name'].toString());
      });
      items.forEach((element) {
        var mp = element;
        // print('map mp $mp');
        AccountProfileScreen_State.items.add(
            getWidget(AccountProfile().fromMap(element, donotSort: false)));
        // print('this is the item list $items');
        AccountProfileScreen_State.show = AccountProfileScreen_State.items;
        (AccountProfileScreen_State.contextt as StatefulElement)
            .state
            .setState(() {});
      });
    }
  }

  getWidget(mp) {
    String name;
    if (mp.name.length > 22) {
      name = mp.name.substring(0, 19) + ' ...';
    } else {
      name = mp.name;
    }
    return Container(
        decoration: BoxDecoration(color: Colors.white),
        child: Row(children: [
          Expanded(
              child: ListTile(
                  onTap: () {
                    onAccountProfileClicked(mp);
                  },
                  title: Text(name),
                  subtitle: Text('${mp.address}${mp.phone1}'))),
          Row(children: [
            IconButton(
                icon:
                    Image(image: AssetImage('assets/ic_phone_black_24dp.png')),
                onPressed: (() {})),
            IconButton(
                icon: Image(image: AssetImage('assets/location_indicator.png')),
                onPressed: (() {}))
          ])
        ]));
  }

  onAccountProfileClicked(AccountProfile mp) {
    DocumentPageState.accountProfile = mp;
    Navigator.push(AccountProfileScreen_State.contextt,
        MaterialPageRoute(builder: (context) => DocumentPage()));
  }

  search(String search) {
    var namesList = [];
    items.forEach((element) {
      if (element['name']
          .toString()
          .toLowerCase()
          .contains(search.toLowerCase())) {
        namesList.add(element as Map<String, dynamic>);
      }
    });
    AccountProfileScreen_State.searchResults.clear();
    namesList.forEach((element) {
      var mp = element as Map<String, dynamic>;
      AccountProfileScreen_State.searchResults.add(getWidget(AccountProfile().fromMap(mp)));
      AccountProfileScreen_State.show =
          AccountProfileScreen_State.searchResults;
      (AccountProfileScreen_State.contextt as StatefulElement)
          .state
          .setState(() {});
    });
  }
}

//class menu icoms comes and its navigations
class MobileMenuItems_VM {
  List menuItemsOrder = [
    'Home',
    'Attendance',
    'Punch Out',
    'Sent Items',
    'Dashboard',
    'Sent Items View',
    'Outbox',
    'Draft',
    'Supplier Profiles',
    'Choose Stock location',
    'Unsend Image',
    'Geo Tag',
    'Outstandings',
    'Stock Details',
    'Notification',
    'Update',
    'Clear',
    'Settings',
    'Signout',
    'Exit'
  ];

  dynamic getNameEmail() async {
    var table = User().table;
    var list = await OurDatabase(table: table).getTables() as List;
    var mp = list[0] as Map<String, dynamic>;
    return {
      'Name': '${mp['firstName']} ${mp['lastName']}',
      'Email': mp['email'],
      'EmployeeName': mp['employeeName']
    };
  }

  Future<List<Widget>> getMenus() async {
    List<Widget> li = List<Widget>();
    var menuItems =
        await OurDatabase(table: MobileMenuItems().table).getTables() as List;
    menuItemsOrder.forEach((value) {
      menuItems.forEach((element) {
        var mp = element as Map<String, dynamic>;
        if (value == mp['label']) {
          li.add(getSingle_Menus(mp['label']));
        }
      });
    });

    return li;
  }

  Widget getSingle_Menus(String value) {
    switch (value) {
      case 'Home':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage('assets/Menui_tem_icons/ic_home.png')),
            onTap: (() {}));
        break;
      case 'Attendance':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage('assets/Menui_tem_icons/ic_attendance.png')),
            onTap: (() {}));
        break;
      case 'Day Plan':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage('assets/Menui_tem_icons/ic_day_plan.png')),
            onTap: (() {}));
        break;
      case 'Sent Items':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage(
                    'assets/Menui_tem_icons/ic_sent_activities.png')),
            onTap: (() {}));
        break;
      case 'Sent Items Details':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage(
                    'assets/Menui_tem_icons/ic_sent_activities.png')),
            onTap: (() {}));
        break;
      case 'Outbox':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage('assets/Menui_tem_icons/ic_outbox.png')),
            onTap: (() {}));
        break;
      case 'Draft':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage('assets/Menui_tem_icons/ic_draft.png')),
            onTap: (() {}));
        break;
      case 'Knowledge Base':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image:
                    AssetImage('assets/Menui_tem_icons/ic_knowledge_base.png')),
            onTap: (() {}));
        break;
      case 'Comp Activity':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image:
                    AssetImage('assets/Menui_tem_icons/ic_comp_activity.png')),
            onTap: (() {}));
        break;
      case 'Comp Activity View':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image:
                    AssetImage('assets/Menui_tem_icons/ic_comp_activity.png')),
            onTap: (() {}));
        break;
      case 'Outstandings':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image:
                    AssetImage('assets/Menui_tem_icons/ic_outstandings.png')),
            onTap: (() {}));
        break;
      case 'Notification':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image:
                    AssetImage('assets/Menui_tem_icons/ic_outstandings.png')),
            onTap: (() {}));
        break;
      case 'Document Approval':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image:
                    AssetImage('assets/Menui_tem_icons/ic_doc_approval.png')),
            onTap: (() {}));
        break;
      case 'Ledger Report':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image:
                    AssetImage('assets/Menui_tem_icons/ic_ledger_report.png')),
            onTap: (() {}));
        break;
      case 'Update':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage('assets/Menui_tem_icons/ic_update.png')),
            onTap: (() {}));
        break;
      case 'Clear':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage('assets/Menui_tem_icons/ic_clear.png')),
            onTap: (() {}));
        break;
      case 'Settings':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage('assets/Menui_tem_icons/ic_settings.png')),
            onTap: (() {}));
        break;
      case 'Exit':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage('assets/Menui_tem_icons/ic_exit.png')),
            onTap: (() {}));
        break;
      case 'Web View':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage('assets/Menui_tem_icons/ic_outstanding.png')),
            onTap: (() {}));
        break;
      case 'Geo Tag':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image:
                    AssetImage('assets/Menui_tem_icons/ic_comp_activity.png')),
            onTap: (() {}));
        break;
      case 'Approvals':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image:
                    AssetImage('assets/Menui_tem_icons/ic_comp_activity.png')),
            onTap: (() {}));
        break;
      case 'Signout':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage('assets/Menui_tem_icons/ic_sign_out.png')),
            onTap: (() {}));
        break;
      case 'Price List':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage('assets/Menui_tem_icons/ic_home.png')),
            onTap: (() {}));
        break;
      case 'Punch Out':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage('assets/Menui_tem_icons/ic_attendance.png')),
            onTap: (() {}));
        break;
      case 'Picking Packing':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image:
                    AssetImage('assets/Menui_tem_icons/ic_knowledge_base.png')),
            onTap: (() {}));
        break;
      case 'Final Balance':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage('assets/Menui_tem_icons/ic_outstanding.png')),
            onTap: (() {}));
        break;
      case 'Stock Details':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage('assets/Menui_tem_icons/ic_outstanding.png')),
            onTap: (() {}));
        break;
      case 'Unsend Image':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage('assets/Menui_tem_icons/ic_draft.png')),
            onTap: (() {}));
        break;
      case 'Sent Items View':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image:
                    AssetImage('assets/Menui_tem_icons/ic_comp_activity.png')),
            onTap: (() {}));
        break;
      case 'Supplier Profiles':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage(
                    'assets/Menui_tem_icons/ic_sent_activities.png')),
            onTap: (() {}));
        break;
      case 'Dashboard':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage(
                    'assets/Menui_tem_icons/ic_sent_activities.png')),
            onTap: (() {}));
        break;
      case 'Choose Stock location':
        return ListTile(
            title: Text(value),
            leading: Image(
                height: 24,
                width: 24,
                image: AssetImage('assets/Menui_tem_icons/ic_outstanding.png')),
            onTap: (() {}));
        break;
    }
  }
}

class Territory_VM {
  Map<String, dynamic> selectedTerritory;
  List territories = [];

  getTerritory() async {
    territories = await OurDatabase(table: Territory().table)
        .getTable(where: "isUserTerritory", whereArgs: 1) as List;

    if (territories.isNotEmpty) {
      String lastSelectedTerritoryName =
          await Preference().getLastSelectedTerritoryName();
      if (lastSelectedTerritoryName != "") {
        if (lastSelectedTerritoryName.toLowerCase() !=
            "All Territory".toLowerCase()) {
          addAllTerritory(true);
        } else {
          selectedTerritory = await OurDatabase(table: Territory().table)
                  .getTable(where: 'name', whereArgs: lastSelectedTerritoryName)
              as Map<String, dynamic>;
          if (selectedTerritory.isNotEmpty) {
            AccountProfileScreen_State.territory = selectedTerritory['name'];
            (AccountProfileScreen_State.contextt as StatefulElement)
                .state
                .setState(() {});
            addAllTerritory(false);
          } else {
            addAllTerritory(true);
          }
        }
      } else {
        addAllTerritory(true);
      }
    }
    TerritoryState.spinner = territories;
    (TerritoryState.contextt as StatefulElement).state.setState(() {});
  }

  addAllTerritory(bool addAllTerritoryToFirst) {
    if (addAllTerritoryToFirst) {
      var territory = {};
      territory['name '] = "All Territory";
      territories[0] = territory;
      selectedTerritory = territory;
    } else {
      var territory = {};
      territory['name '] = "All Territory";
      territories[0] = territory;
    }
  }
}
