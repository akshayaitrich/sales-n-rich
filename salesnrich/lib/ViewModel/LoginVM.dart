import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:salesnrich/Controller/APIs.dart';
import 'package:salesnrich/Controller/staticFields.dart';
import 'package:salesnrich/Model/user.dart';
import 'package:salesnrich/View/splash_Login_Download.dart';
import 'package:salesnrich/WebServer.dart';

class LoginVM {
  String username;
  String password;

  LoginVM({String username, String password}) {
    if (username != null && password != null) {
      this.username = username;
      this.password = password;
    }
  }

  getuser() async {
    if (await UserDatabase().userExists()) {
      UserModel user = await UserDatabase().getUser();
      LoginPageState.username.text = user.username;
      LoginPageState.password.text = user.userPassword;
    }
  }

  Future<bool> credientials() async {
    var user = {'username': '$username', 'password': '$password'};
    var response = await http.post(
      ServiceGenerator. LOGIN,
      body: json.encode(user),
      headers: {'Content-Type': 'application/json'},
      //encoding: Encoding.getByName('gzip')
    );
    if (response.statusCode == 200) {
      userLogin(response);
      print(response.body);
      return true;
    } else {
      return false;
    }
  }

  userLogin(http.Response response) async {
    if (await UserDatabase().userExists()) {
      UserModel user = await UserDatabase().getUser();
      StaticFields.token = user.tokenId;
      UserDatabase().update(user);
    } else {
      Map mp = json.decode(response.body);
      StaticFields.token = mp['id_token'];
    }
  }
}
