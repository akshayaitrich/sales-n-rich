import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:salesnrich/Controller/staticFields.dart';
import 'package:salesnrich/Model/Tables.dart';
import 'Controller/APIs.dart';

class WebServer {
  Future<dynamic> post({String url, Map data}) async {
    var response = await http.post(ServiceGenerator.POST_DEVICE_KEY,
        body: json.encode(data),
        headers: {
          'Authorization': 'Bearer ${StaticFields.token}',
          'Content-Type': 'application/json'
        });
    return response;
  }

  Future<dynamic> postSavedTaskExecution({String url, data}) async {
    var body = TaskExecutionSubmissionJson().toJson(data);
    print("{$body}");
    print(body['inventoryVouchers']);
    var a = jsonEncode(body);
    var response = await http.post(url, body: a  , headers: {
      'Authorization': 'Bearer ${StaticFields.token}',
      'Content-Type': 'application/json'
    });
    return response; 
  }

  Future<dynamic> gett({String url}) async {
    var response = await http.get(url, headers: {
      'Authorization': 'Bearer ${StaticFields.token}',
      'Content-Type': 'application/json'
    });
    return response;
  }

  Future<dynamic> get_with_query({String url, Map data}) async {
    var uri = Uri.http(ServiceGenerator.URL, '/api/$url', data);
    var response = await http.get(uri, headers: {
      'Authorization': 'Bearer ${StaticFields.token}',
      'Content-Type': 'application/json'
    });
    return response;
  }
}
