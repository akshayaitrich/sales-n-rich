import 'package:f_logs/f_logs.dart';
import 'package:flutter/material.dart';
import 'package:salesnrich/Controller/staticFields.dart';
import 'package:salesnrich/Model/user.dart';
import 'package:salesnrich/View/splash_Login_Download.dart';
import 'package:flutter/services.dart';
import 'package:sizer/sizer.dart';

void main() {
  runApp(MyApp());
  getValuestoStaticFields();
}

getValuestoStaticFields() async {
  LogsConfig config = LogsConfig()
    ..isDebuggable = true
    ..customClosingDivider = "|"
    ..customOpeningDivider = "|"
    ..csvDelimiter = ", "
    ..encryptionEnabled = false
    ..encryptionKey = ""
    ..formatType = FormatType.FORMAT_CURLY
    ..logLevelsEnabled = [LogLevel.INFO, LogLevel.ERROR]
    ..dataLogTypes = [
      DataLogType.DEVICE.toString(),
      DataLogType.NETWORK.toString(),
      DataLogType.ERRORS.toString(),
      DataLogType.LOCATION.toString(),
      "Your own data type here"
    ]
    ..timestampFormat = TimestampFormat.TIME_FORMAT_READABLE;

  FLog.applyConfigurations(config);
  FLog.clearLogs();
  if (await UserDatabase().userExists()) {
    StaticFields.userexists = true;
    StaticFields.user = await UserDatabase().getUser();
    StaticFields.token = (StaticFields.user).tokenId;
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);

    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    return LayoutBuilder(builder: (context, constraints) {
      return OrientationBuilder(builder: (context, orientation) {
        SizerUtil().init(constraints, orientation);
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          home: FirstPage(),
        );
      });
    });
  }
}
